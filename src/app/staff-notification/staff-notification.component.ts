import { Component, OnInit } from '@angular/core';
import {NotificationService} from  '../shared/services/notification.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
    selector: 'app-staff-notification',
    templateUrl: './staff-notification.component.html',
    styleUrls: ['./staff-notification.component.css']
})
export class StaffNotificationComponent implements OnInit {
    private items: any[] = [];
    private isLoading: boolean =  false;
    private id:number;
    constructor(
        private service: NotificationService
    ) { }

    ngOnInit() {
        var user = JSON.parse(localStorage.getItem('user'));
        this.id = user.id;
        this.loadItems();
    }
    loadItems(){
		this.isLoading = true;
		this.service.staffNotifications(this.id).subscribe(res => {
			this.items = res;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
		});

	}
}
