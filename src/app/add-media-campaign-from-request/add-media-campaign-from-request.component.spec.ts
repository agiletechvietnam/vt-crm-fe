/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddMediaCampaignFromRequestComponent } from './add-media-campaign-from-request.component';

describe('AddMediaCampaignFromRequestComponent', () => {
  let component: AddMediaCampaignFromRequestComponent;
  let fixture: ComponentFixture<AddMediaCampaignFromRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMediaCampaignFromRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMediaCampaignFromRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
