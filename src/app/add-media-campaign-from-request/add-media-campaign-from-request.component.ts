import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import { Router, ActivatedRoute } from '@angular/router';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Subscription } from 'rxjs';
import {AreaService} from '../shared/services/area.service';
@Component({
  selector: 'app-add-media-campaign-from-request',
  templateUrl: './add-media-campaign-from-request.component.html',
  styleUrls: ['./add-media-campaign-from-request.component.css']
})
export class AddMediaCampaignFromRequestComponent implements OnInit {

  private isLoading: boolean = false;
    public subscription: Subscription;
    public program: any = {};
    private mes: Message[];
    private doanhthuNhom;
    private doanhthuSp;
    private diaban;
    private areas: any[] = [];
    private delItem: any;
    private form: FormGroup;
    private displayDel: boolean = false;
    private formCreate: boolean;
	private title: string;
    private displayForm: boolean = false;
    private channels: any[] = [];

    private shopId;
    constructor(
        private service: MediaCampaignService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private areaService: AreaService
    ) { }
    ngOnInit() {
        var user = JSON.parse(localStorage.getItem("user"));
        this.shopId = user.shopId;
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.program.id = params['id'];
            let that = this;

            this.loadProgram().then(function(){
                that.loadDoanhthuNhom();
                that.loadDoanhthuSp();
                that.loadDiaban();
            });
            this.loadSelect();
        });

        this.form = this.fb.group({
			'id': new FormControl(),
			'areaShopId': new FormControl('', Validators.compose([Validators.required])),
			'startDate': new FormControl('', Validators.compose([Validators.required])),
			'endDate': new FormControl('', Validators.compose([Validators.required])),
			'amount': new FormControl(),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
        
    }

    loadProgram(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.findMediaCampaignById(that.program.id).subscribe(
                res => {
                    that.program = res;
                    that.isLoading = false;
                    resolve();
                }, 
                error => {
                    error > console.log(error);
                    reject();
                }
            );
        })
        
    }

    loadDoanhthuNhom(){
        let query = {
            "sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = 1 and CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" and SHOP_ID = "+this.program.shopId
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuNhom = res;
        }, error => {

        });
    }
    
    loadDoanhthuSp(){
        let query = {
            "sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = 0 and CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" and SHOP_ID = "+this.program.shopId
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuSp = res;
        }, error => {

        });
    }
    
    loadDiaban(){
		this.service.findAreaByMediaId(this.program.id).subscribe(
			res => {
				this.diaban = res;
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    loadChannelsOfCampaign(){
		this.isLoading = true;
		this.service.findChannelsOfCampgin(this.shopId, this.program.campaignRequestId).subscribe(res=> {
			this.channels = res;
			this.isLoading = false;
			
		}, error=>{

		})
	}
    loadSelect(){
		
		if (this.areas.length == 0){
			this.isLoading = true;
			this.areas.push({label:'Địa bàn', value:null});
			this.areaService.findAreasByShopId(this.shopId).subscribe(res => {	
				for(let i=0; i<res.length; i++){
					this.areas.push({label: res[i].address, value: res[i].id, ob: res[i]});
				}
				this.isLoading = false;
			}, error => {});
		}
	}
    suane(){
		let total = 0;
		for (let i=0; i<this.channels.length; i++){
			this.channels[i].amount = this.channels[i].quantity * this.channels[i].price;
			total += this.channels[i].amount;
		}
		this.form.controls['amount'].setValue(total, { onlySelf: false });
		 
	}
    create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
		if (this.channels.length == 0){
			this.loadChannelsOfCampaign();
		}
		for (let i =0; i<this.channels.length; i++)
			this.channels[i].quantity = 0;
	}
    onSubmit(value:any){

		if (this.formCreate){
			if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
				this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
				return;
			}
			if (this.form.controls['startDate'].value < this.program.startDate){
				this.showMes('error', 'Ngày bắt đầu của địa bàn phải lớn hơn hoặc bằng ngày bắt đầu của chiến dịch');
				return;
			}
			if (this.form.controls['endDate'].value > this.program.endDate){
				this.showMes('error', 'Ngày kết thúc của địa bàn phải nhỏ hơn hoặc bằng ngày kết thúc của chiến dịch');
				return;
			}
		}
		
		 
		 
		let data = {
			id: this.program.id,
			campaignDetail:{
                areaShopId : value.areaShopId,
                startDate: value.startDate,
                endDate: value.endDate,
                description : value.description
        	},
			prices: [],
		};

		for (let i=0;i<this.channels.length; i++){
			if (this.channels[i].quantity != 0 && this.channels[i].quantity != '' && this.channels[i].quantity != null){
				data.prices.push({
					mediaId: this.channels[i].mediaId,
					price: this.channels[i].price,
					quantity : this.channels[i].quantity,
					amount : this.channels[i].amount
				});
			}
		}
	

		if (data.prices.length == 0) {
			this.showMes('error', 'Bạn điền số lượng cho ít nhất 1 hình thức truyền thông');
			return;
		}
		if (this.formCreate == true){
			
			this.isLoading = true;
			this.service.addMediaCampaignDetail(data).subscribe(res => {
				this.isLoading = false;
				this.displayForm = false;
				this.loadProgram();
                this.loadDiaban();
				this.showMes('success', 'Thêm thành công');
			}, error => {
				this.isLoading = false;
				this.showMes('error', 'Thêm thất bại');
			});
		}else{
			this.isLoading = true;
			delete data.campaignDetail.endDate;
			delete data.campaignDetail.startDate;
			// console.log('sua');
			this.service.updateMediaCampaignDetail(data).subscribe(res => {
				this.loadProgram();
				 this.loadDiaban();
				this.displayForm = false;
			}, error => {

			});
		}

	}
    gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
    update(item){
		console.log(item.id);
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: false });
		this.form.controls['amount'].setValue(item.amount, { onlySelf: false });
		this.form.controls['description'].setValue(item.description, { onlySelf: false });
		this.form.controls['areaShopId'].setValue(item.areaShopId, { onlySelf: false });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: false });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: false });

		this.channels = [{mediaId: item.mediaId, name: item.mediaName, quantity: item.quantity, amount: item.amount, price: item.price}];
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.service.delAreaInMedia(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadProgram();
                this.loadDiaban();
				this.showMes('warn', 'Xóa thành công');
			},
			error => {}
		);
	}
	guiDuyet(){
		if (this.program.totalCost > this.program.budget || ((this.program.totalCost/this.program.budget) * 100) <80){
			this.showMes('error', 'Tổng tiền phải lớn hơn 80% ngân sách và nhỏ hơn ngân sách');
			 return;
		}
		this.isLoading = true;
		this.service.updateMediaCampaign({status: 2}, this.program.id).subscribe(
			res => {
				this.displayForm = false;
				this.loadProgram();
				this.isLoading = false;
				this.showMes('success', 'Gửi duyệt thành công');
			},
			error => {}
		)
	}
	
	
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 5000);
	}

}
