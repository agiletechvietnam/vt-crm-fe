import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service';
import { TargetModel } from '../shared/models/target.model';
@Component({
    selector: 'app-target-staff',
    templateUrl: './target-staff.component.html',
    styleUrls: ['./target-staff.component.css']
})
export class TargetStaffComponent implements OnInit {

    private items: any[] = [];
    private isLoading: boolean =  false;
    private user;
    constructor(
        private service: TargetService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.loadItems();
    }
    loadItems(){
		this.isLoading = true;
		this.service.findTargetStaff(this.user.id).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			},
			error => {
				error => console.log(error);
			}
		);
	}
}
