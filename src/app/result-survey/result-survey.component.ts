import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from "../shared/services/survey.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
  selector: 'app-result-survey',
  templateUrl: './result-survey.component.html',
  styleUrls: ['./result-survey.component.css']
})
export class ResultSurveyComponent implements OnInit {

  private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    private campaignCusId;
    private survey: any = {};
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: SurveyService
    ) { }

    ngOnInit() {
        
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignCusId = params['campaignCusId'];
            this.loadItems();
        });
    }
    
    loadItems() {
        this.isLoading = true;
        this.service.resultSurvey(this.campaignCusId).subscribe(
            res => {
                console.log(res);
                this.survey = res;
                
                for (let i=0; i<this.survey.questions.length; i++){
                    if (this.survey.questions[i].questionType == 3 || this.survey.questions[i].questionType == 2){
                        this.survey.questions[i].realAnswers = [];
                       
                            
                        for (let j=0; j< this.survey.questions[i].cusAnswers.length; j++){
                            this.survey.questions[i].realAnswers.push(this.survey.questions[i].cusAnswers[j].id);
                            if (this.survey.questions[i].questionType == 3 )
                                this.survey.questions[i].note = this.survey.questions[i].cusAnswers[j].note;
                        }
                            
                    }
                        
                }
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    Done(){
        console.log(this.survey);
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
 
