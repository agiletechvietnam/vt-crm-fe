import { Component, OnInit } from '@angular/core';
import {SurveyService} from  '../shared/services/survey.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {Message} from 'primeng/primeng';
@Component({
    selector: 'app-answer',
    templateUrl: './answer.component.html',
    styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
    public subscription: Subscription;
    private items: any[] = [];
	private mes: Message[]; 
	private form: FormGroup;
	private formAnswer: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
    private questionId;
    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
		private service: SurveyService
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'responseText': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(200), this.NoWhitespaceValidator])),
		});

        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.questionId = params['id'];
            this.loadItems();
        });
    }
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	
    loadItems(){
		this.isLoading = true;
		this.service.findAnswersByQuestionId(this.questionId).subscribe(res => {
			this.items = res;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
		});

	}
	create(){
		this.form.reset();
		this.title = "Thêm trả lời";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['responseText'].setValue(item.responseText, { onlySelf: true });
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delAnswer(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			value.questionId = this.questionId;
			this.service.addAnswer(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			this.service.updateAnswer(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
