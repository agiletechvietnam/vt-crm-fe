import { Component, OnInit } from '@angular/core';
import {SurveyService} from  '../shared/services/survey.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
	selector: 'app-question',
	templateUrl: './question.component.html',
	styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

	private items: any[] = [];
	private mes: Message[];

	private form: FormGroup;
	private formAnswer: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
	private types: any[] = [];

	private titleAnswer: string;
	private formCreateAnswer: boolean;
	private displayFormAnswer: boolean = false;
	private displayDelAnswer: boolean = false;
	private delItemAnswer: any;
	private questionId;
	private user;
	constructor(
		private fb: FormBuilder,
		private service: SurveyService
	) { }

	ngOnInit() {
		//form question
		this.user  = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'description':  new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(200)])),
			'questionText': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(200)])),
			'questionType': new FormControl('', Validators.compose([Validators.required]))
		});
		//form answer
		this.formAnswer = this.fb.group({
			'id': new FormControl(),
			'responseText': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(200)])),
		});

		this.types.push({label: 'Chọn kiểu', value: null}, {label: 'Tự trả lời', value: 1},  {label: 'Chọn 1', value: 2}, {label: 'Chọn nhiều', value: 3});
		this.loadItems();
	}
	loadItems(){
		this.isLoading = true;
		this.service.findQuestionsAnswers().subscribe(res => {
			this.items = res;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
		});

	}
	create(){
		this.form.reset();
		this.title = "Thêm câu hỏi";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa câu hỏi";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['questionText'].setValue(item.questionText, { onlySelf: true });
		this.form.controls['questionType'].setValue(item.questionType, { onlySelf: true });
		this.form.controls['description'].setValue(item.description, { onlySelf: true });
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delQuestion(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			this.service.addQuestion(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			this.service.updateQuestion(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}

	//answer

	createAnswer(id){
		this.formAnswer.reset();
		this.titleAnswer = "Thêm trả lời";
		this.formCreateAnswer = true;
		this.displayFormAnswer = true;
		this.questionId = id;
	}
	updateAnswer(item){
		this.formAnswer.reset();
		this.titleAnswer = "Sửa";
		this.formCreateAnswer = false;
		this.displayFormAnswer = true;
		this.formAnswer.controls['id'].setValue(item.id, { onlySelf: true });
		this.formAnswer.controls['responseText'].setValue(item.responseText, { onlySelf: true });
	}
	deleteAnswer(item){
		this.delItemAnswer = item;
		this.displayDelAnswer = true;
	}
	acceptDelAnswer(){
		this.isLoading = true;
		this.service.delAnswer(this.delItemAnswer).subscribe(
			res => {
				this.isLoading = false;
				this.delItemAnswer = null;
				this.displayDelAnswer = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmitAnswer(value:any){
		this.isLoading = true;
		if (this.formCreateAnswer){
			value.questionId = this.questionId;
			this.service.addAnswer(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayFormAnswer = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			this.service.updateAnswer(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayFormAnswer = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}

	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
