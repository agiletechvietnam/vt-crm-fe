import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class SurveyService {

    constructor(private http: HttpService) {
        
    }
    findSurveys(): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SURVEYS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    delSurvey(c: any){
        return this.http.delete(APIConstant.DELETE_SURVEY+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addSurvey(c: any){
        return this.http.post(APIConstant.ADD_SURVEY, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateSurvey(c: any, id){
        return this.http.post(APIConstant.UPDATE_SURVEY+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findSurveyById(id): Observable<any>{
        return this.http.get(APIConstant.FIND_SURVEY_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findQuestions(): Observable<any[]> {
        return this.http.get(APIConstant.FIND_QUESTIONS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findQuestionsAnswers(): Observable<any[]> {
        return this.http.get(APIConstant.FIND_QUESTIONS_ANSWERS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delQuestion(c: any){
        return this.http.delete(APIConstant.DELETE_QUESTION+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addQuestion(c: any){
        return this.http.post(APIConstant.ADD_QUESTION, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateQuestion(c: any, id){
        return this.http.post(APIConstant.UPDATE_QUESTION+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findQuestionById(id): Observable<any>{
        return this.http.get(APIConstant.FIND_QUESTION_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findAnswersByQuestionId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_ANSWERS_BY_QUESION_ID+'?questionId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    delAnswer(c: any){
        return this.http.delete(APIConstant.DELETE_ANSWER+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addAnswer(c: any){
        return this.http.post(APIConstant.ADD_ANSWER, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateAnswer(c: any, id){
        return this.http.post(APIConstant.UPDATE_ANSWER+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findQuestionsBySurveyId(id){
        return this.http.get(APIConstant.FIND_QUESTIONS_BY_SURVEY_ID+'?surveyId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    delQuestionInSurvey(id){
        return this.http.delete(APIConstant.DELETE_QUESTION_IN_SURVEY+id,null)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addQuestionToSurvey(c: any){
        return this.http.post(APIConstant.ADD_QUESTION_TO_SURVEY, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    fillSurvey(id){
        return this.http.get(APIConstant.FILL_SURVEY+'?surveyId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    allSurvey(){
        return this.http.get(APIConstant.ALL_SURVEY)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    resultSurvey(id){
        return this.http.get(APIConstant.RESULT_SURVEY+'?campaignCusId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    upSurvey(c: any){
        return this.http.post(APIConstant.UP_SURVEY, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}