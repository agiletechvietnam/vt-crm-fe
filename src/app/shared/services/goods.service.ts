import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {CustomerSearchModel} from "../models/customer.search";
import {Observable} from "rxjs";
import {CustomerModel} from "../models/customer.model";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class GoodsService {

    constructor(private http: HttpService) {
        
    }

    //nganh hang
    findProductType(){
         return this.http.get(APIConstant.FIND_PRODUCT_TYPE)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
   
    //nhom hang
    findGoodsgroupByProductType(type){
         return this.http.get(APIConstant.FIND_GOODSGROUP_BY_PRODUCT_TYPE+'?productInType='+type)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    //mat hang boi nhom hang
    findGoodByGroupId(id){
         return this.http.get(APIConstant.FIND_GOODS_BY_GROUP_ID+'?groupIdIn='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
     //mat hang
    findGoods(){
         return this.http.get(APIConstant.FIND_GOODS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findGoodsByKey(key){
        return this.http.get(APIConstant.FIND_GOODS_BY_KEY+'?searchText='+key)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findGoodsInRequest(key){
        return this.http.get(APIConstant.FIND_GOODS_IN_REQUEST+'?id='+key)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addGoodToCampaign(any){
        return this.http.post(APIConstant.ADD_GOODS_TO_CAMPAIGN, any)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    deleteGoodToCampaign(id){
        return this.http.delete(APIConstant.DELETE_GOODS_TO_CAMPAIGN+id, null)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}
