import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";
import { Observable } from "rxjs";
import { CampaignModel } from "../models/campaign.model";
import { APIConstant } from "../api.constant";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class CampaignService {

    constructor(private http: HttpService) {

    }
    findCampaigns(): Observable<any> {
        return this.http.get(APIConstant.FIND_CAMPAIGNS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCampaignsByStatus(): Observable<CampaignModel[]> {
        return this.http.get(APIConstant.FIND_CAMPAIGNS_BY_STATUS+'?statusIn=2,3,4,5,6')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delCampaign(c: CampaignModel) {
        return this.http.delete(APIConstant.DELETE_CAMPAIGN + c.id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addCampaign(c: CampaignModel) {
        return this.http.post(APIConstant.ADD_CAMPAIGN, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    updateCampaign(c: any, id) {
        return this.http.post(APIConstant.UPDATE_CAMPAIGN + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateCampaignCustomer(c: any, id) {
        return this.http.post(APIConstant.UPDATE_CAMPAIGNCUSTOMER + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    updateCampaignShop(c: any, id) {
        return this.http.post(APIConstant.UPDATE_CAMPAIGNSHOP + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCampaignShopById(id): Observable<any> {
        return this.http.get(APIConstant.FIND_CAMPAIGNSHOP+  id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    findCampaignById(id): Observable<any> {
        return this.http.get(APIConstant.FIND_CAMPAIGN_BY_ID+'?id=' + id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCampaignByShopId(id): Observable<any> {
        return this.http.get(APIConstant.FIND_CUSTOMER_CAMPAIGN_BY_SHOP_ID + '?shopId=' + id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCustomersByCampaignId(id): Observable<any> {
        return this.http.get(APIConstant.FIND_CUSTOMERS_BY_CAMPAIGN_ID + '?campaignId=' + id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    runSql(id): Observable<any> {
        return this.http.get(APIConstant.RUNSQL + '?campaignId=' + id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findCampaignShopByShopId(id): Observable<any>{

        return this.http.get(APIConstant.FIND_CAMPAIGNSHOP_BY_SHOP_ID + '?shopId=' + id +'&statusIn=3,5,6')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));

    }
    assignAuto(c){
        return this.http.post(APIConstant.ASSIGN_STAFF_AUTO, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    assignManualAgain(c){
        return this.http.post(APIConstant.ASSIGN_STAFF_MANUAL_AGIAN, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    assignManual(c){
        return this.http.post(APIConstant.ASSIGN_STAFF_MANUAL, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findStaffsCampaignByShopId(shopId, campaignId): Observable<any>{
        return this.http.get(APIConstant.FIND_STAFFS_CAMPAIGN_BY_SHOP_ID + '?shopId=' + shopId+'&campaignId='+campaignId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findResultStaffsCampaignByShopId(shopId, campaignId): Observable<any>{
        return this.http.get(APIConstant.FIND_RESULT_STAFFS_CAMPAIGN_BY_SHOP_ID + '?shopId=' + shopId+'&campaignId='+campaignId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findResultCustomersCampaignByStaffId(staffId, campaignId): Observable<any>{
        return this.http.get(APIConstant.FIND_RESULT_CUSTOMERS_CAMPAIGN_BY_SHOP_ID + '?staffId=' + staffId+'&campaignId='+campaignId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findCampaignByStaffId(staffId): Observable<any>{
        return this.http.get(APIConstant.FIND_CAMPAIGN_BY_STAFF_ID + '?staffId=' +staffId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findShopCampaignByCampaignId(campaignId): Observable<any>{
        return this.http.get(APIConstant.FIND_CAMPAIGNSHOP_BY_CAMPAIGN_ID + '?campaignId='+campaignId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCampaignCustomerByCampaignShopId(campaignId, shopId): Observable<any>{
        return this.http.get(APIConstant.FIND_CAMPAIGN_CUSTOMERS_BY_CAMPAIGN_SHOP_ID + '?campaignId='+campaignId+'&shopId='+shopId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    assignShop(c) {
        return this.http.post(APIConstant.CAMPAIGN_ASSIGN_SHOP, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    verifyCampaign(c, id){
        return this.http.post(APIConstant.VERIFY_CAMPAIGN+'?campaignId='+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
       
    }
    
}
