import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {CustomerSearchModel} from "../models/customer.search";
import {Observable} from "rxjs";
import {CustomerModel} from "../models/customer.model";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {

    constructor(private http: HttpService) {
        
    }
    findCampaign(any){
        return this.http.post(APIConstant.DOMAIN_RUNSQL, any)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}
