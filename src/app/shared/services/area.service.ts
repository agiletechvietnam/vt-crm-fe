import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";
import { Observable } from "rxjs";
import { APIConstant } from "../api.constant";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class AreaService {
    constructor(private http: HttpService) {
    }
    findAreasByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_AREAS_BY_SHOP_ID+'?shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    addAreaShop(c){
        return this.http.post(APIConstant.ADD_AREA_SHOP, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateAreaShop(c, id){
        return this.http.post(APIConstant.UPDATE_AREA_SHOP+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delAreaShop(c){
        return this.http.delete(APIConstant.DELETE_AREA_SHOP+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    
    addArea(c){
        return this.http.post(APIConstant.ADD_AREA, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateArea(c, id){
        return this.http.post(APIConstant.UPDATE_AREA+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delArea(c){
        return this.http.delete(APIConstant.DELETE_AREA+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findAreaById(id): Observable<any> {
        return this.http.get(APIConstant.FIND_AREA+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    //competitor
    findCompetitorByAreaCode(areaCode): Observable<any[]> {
        return this.http.get(APIConstant.FIND_COMPETITOR_BY_AREA_CODE+'?payAreaCode='+areaCode)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCompetitorByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_COMPETITOR_BY_SHOP_ID+'?shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    addCompetitor(c){
        return this.http.post(APIConstant.ADD_COMPETITOR, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateCompetitor(c, id){
        return this.http.post(APIConstant.UPDATE_COMPETITOR+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delCompetitor(c){
        return this.http.delete(APIConstant.DELETE_COMPETITOR+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    //company
    findCompanysByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_COMPANYS_BY_SHOP_ID+'shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }


    findPopulationGroups(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_GROUP_BY_AREA_ID+'?id='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    findMarketsByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_MARKETS_BY_SHOP_ID+'shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findDvByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_DV_BY_SHOP_ID+'shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findSchoolsByShopId(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SCHOOLS_BY_SHOP_ID+'shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
     
    // findPopulation(): Observable<any[]> {
    //     return this.http.get(APIConstant.FIND_POPULATION)
    //         .map(res => res.json())
    //         .catch((error: any) => Observable.throw(error));
    // }
    findPopulation(key: any, page, size): Observable<any> {
        if (key == "" || key == null)
            return this.http.get(APIConstant.FIND_POPULATION+'?page='+page+'&size='+size)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
        return this.http.get(APIConstant.FIND_POPULATION+'?page='+page+'&size='+size+'&searchText='+key)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
    }

    updatePopulation(c): Observable<any[]> {
        return this.http.post(APIConstant.UPDATE_POPULATION+c.id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    //tổ dan phố
    findPopgroupByAreaCode(code): Observable<any[]> {
        return this.http.get(APIConstant.FIND_POPGGROUP_BY_AREA_CODE+'?payAreaCode='+code)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addPopgroup(c){
        return this.http.post(APIConstant.ADD_POPGGROUP, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updatePopgroup(c, id){
        return this.http.post(APIConstant.UPDATE_POPGGROUP+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delPopgroup(c){
        return this.http.delete(APIConstant.DELETE_POPGGROUP_BY_AREA_CODE+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    findAreas(){
        return this.http.get(APIConstant.FIND_AREAS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCityByCenterCodeIn(code){
        return this.http.get(APIConstant.FIND_CITY_BY_CENTER_CODE_IN+'?centerCodeIn='+code)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findShopByProvinceIn(proviceIn){
        return this.http.get(APIConstant.FIND_SHOPS_BY_PROVINCE_IN+'?provinceIn='+proviceIn)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
     
    }

    
} 