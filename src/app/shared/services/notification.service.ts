import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class NotificationService {

    constructor(private http: HttpService) {
        
    }
    staffNotifications(id): Observable<any[]> {
        return this.http.get(APIConstant.STAFF_NOTIFICATION+'?staffId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    
}