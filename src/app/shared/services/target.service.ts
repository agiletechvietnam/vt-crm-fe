import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {TargetModel} from "../models/target.model";
import {APIConstant} from "../api.constant";
import {Http, Response, RequestOptionsArgs, Headers} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class TargetService {
    constructor(private http: HttpService) {
        
    }
    findTargets(): Observable<TargetModel[]> {
        return this.http.get(APIConstant.FIND_TARGETS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }   
    findTargetsByStatus(status): Observable<any> {
        return this.http.get(APIConstant.FIND_TARGETS_BY_STATUS+'?statusIn='+status)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findTargetsByShopId(shop_id): Observable<any[]>{
        return this.http.get(APIConstant.FIND_TARGETS_BY_SHOP_ID+'?shopId='+shop_id+'&statusIn=3,5')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findShopTargetByShopId(shop_id): Observable<any[]>{
        return this.http.get(APIConstant.FIND_TARGETS_BY_SHOP_ID+'?shopId='+shop_id+'&statusIn=2,3,4,5,6')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }


    
    delTarget(c: TargetModel){
        return this.http.delete(APIConstant.DELETE_TARGET+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addTarget(c: TargetModel){
        return this.http.post(APIConstant.ADD_TARGET, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateTarget(c: any, id){
        return this.http.post(APIConstant.UPDATE_TARGET+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findTargetById(id): Observable<TargetModel>{
        return this.http.get(APIConstant.FIND_TARGET_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    // tra ve danh sach sieu thi thuoc id_target
    findShopTargetByTarget(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SHOP_TARGET_BY_TARGET+'?targetId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    // tra ve tat ca cac chi tieu cua sieu thi
    findShopTarget(): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SHOP_TARGET)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    // tra ve shop chi tieu co id
    findShopTargetById(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SHOP_TARGET_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    addShopTarget(data: any){
        return this.http.post(APIConstant.ADD_SHOP_TARGET, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateShopTarget(data: any){
        return this.http.post(APIConstant.UPDATE_SHOP_TARGET+data.id, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    deleteShopTarget(id){
        return this.http.delete(APIConstant.DELETE_SHOP_TARGET+id, null)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    //http://localhost:3000/#/phancongnhanvien
    findShopAreaTarget(id): Observable<any[]> {
        return this.http.get(APIConstant.SHOP_AREA_TARGET+'?shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findTargetStaff(id): Observable<any[]> {
        return this.http.get(APIConstant.FIND_TARGET_STAFF+'?staffId='+id+'&statusIn=2,3,4,5,6')
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findShopAreaTargetByShopTargetId(bdkhTargetId, shopId): Observable<any[]> {
        return this.http.get(APIConstant.FIND_TARGET_VIEW_SHOP+'?shopId='+shopId+'&bdkhTargetId='+bdkhTargetId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateSaffTarget(data: any){
        return this.http.post(APIConstant.UPDATE_TARGET_STAFF, data)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
 
} 