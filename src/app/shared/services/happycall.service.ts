import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';
@Injectable()
export class HappyCallService {

    constructor(private http: HttpService) {
        
    }

    findAllHappys(): Observable<any[]> {
        return this.http.get(APIConstant.FIND_SHOPS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    delHappy(id, c: any){
        return this.http.delete(APIConstant.DELETE_SHOP+id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addHappy(c: any){
        return this.http.post(APIConstant.ADD_SHOP, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateHappy(c: any, id){
        return this.http.post(APIConstant.UPDATE_SHOP+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findHappyById(id): Observable<any>{
        return this.http.get(APIConstant.FIND_SHOP_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}