import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {CustomerSearchModel} from "../models/customer.search";
import {Observable} from "rxjs";
import {CustomerModel} from "../models/customer.model";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable() 
export class CustomerService {

    constructor(private http: HttpService) {
        
    }

    findCustomers(key: any, page, size): Observable<any> {
        if (key == "" || key == null)
            return this.http.get(APIConstant.FIND_CUSTOMERS+'?page='+page+'&size='+size)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
        return this.http.get(APIConstant.FIND_CUSTOMERS+'?page='+page+'&size='+size+'&searchText='+key)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
    }
  
    findCustomersByShopId(key, id, page, size): Observable<any> {
        if (key == "" || key == null)
            return this.http.get(APIConstant.FIND_CUSTOMERS_BY_SHOP_ID+'?shopId='+id+'&page='+page+'&size='+size)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
       return this.http.get(APIConstant.FIND_CUSTOMERS_BY_SHOP_ID+'?shopId='+id+'&page='+page+'&size='+size+'&searchText='+key)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
    }

    delCustomer(id, c: CustomerModel){
        return this.http.delete(APIConstant.DELETE_CUSTOMER+id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addCustomer(c: CustomerModel){
        return this.http.post(APIConstant.ADD_CUSTOMER, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateCustomer(c: CustomerModel, id){
        return this.http.post(APIConstant.UPDATE_CUSTOMER+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findCustomerById(id): Observable<any>{
        return this.http.get(APIConstant.FIND_CUSTOMER_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    //lich su giao dich
    findTransactions(phone): Observable<any[]>{
        return this.http.get(APIConstant.FIND_TRANSACTIONS+'?mobile='+phone)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findTransactionDetail(id): Observable<any[]>{
        return this.http.get(APIConstant.FIND_TRANSACTION_DETAIL+'?transId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    // lich su bao hanh
    findWarrantys(phone): Observable<any[]>{
        return this.http.get(APIConstant.FIND_WARRANTY+'?phone='+phone)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findCustomerByPhone(phone): Observable<any>{
        return this.http.get(APIConstant.FIND_CUSTOMER_BY_PHONE+'?mobile='+phone)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}