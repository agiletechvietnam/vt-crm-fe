import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {ChannelModel} from "../models/channel.model";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class ChannelService {

    constructor(private http: HttpService) {
        
    }
    findChannels(): Observable<ChannelModel[]> {
        return this.http.get(APIConstant.FIND_CHANNELS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findRequest(id){
        return this.http.get(APIConstant.FIND_REQUEST+id )
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delChannel(c: ChannelModel){
        return this.http.delete(APIConstant.DELETE_CHANNEL+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addChannel(c: ChannelModel){
        return this.http.post(APIConstant.ADD_CHANNEL, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateChannel(c: ChannelModel, id){
        return this.http.post(APIConstant.UPDATE_CHANNEL+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateMediaCampaign(c: any, id) {
        return this.http.post(APIConstant.UPDATE_MEDIA_CAMPAIGN + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findChannelById(id): Observable<ChannelModel>{
        return this.http.get(APIConstant.FIND_CHANNEL_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    channelShopPrices(id){
        return this.http.get(APIConstant.CHANNEL_SHOP_PRICES+'?mediaId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateShopPrice(id, c){
        return this.http.post(APIConstant.UPDATE_SHOP_PRICE+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addShopPrice(c){
        return this.http.post(APIConstant.ADD_SHOP_PRICE, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    deleteShopPrice(c){
        return this.http.delete(APIConstant.DELETE_SHOP_PRICE+c.id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addChannelToRequest(c){
        return this.http.post(APIConstant.ADD_CHANNEL_TO_REQUEST, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    deleteChannelInRequest(id){
        return this.http.delete(APIConstant.DELETE_CHANNEL_IN_REQUEST + id, {})
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    deleteChannelInRequestFromShop(requestId, mediaId){
        return this.http.post(APIConstant.DEL_CHANNEL_FROM_SHOP + '?campaignRequestId='+requestId+'&mediaId='+mediaId, {})
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findChannelsInRequest(id){
        return this.http.get(APIConstant.FIND_CHANNEL_IN_REQUEST+'?campaignRequestId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addShopToRequest(c){
        return this.http.post(APIConstant.ADD_SHOP_TO_REQUEST, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findShopInRequest(id){
        return this.http.get(APIConstant.FIND_SHOP_IN_REQUEST+'?id='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findDoanhthu(id){
        return this.http.get(APIConstant.FIND_DOANHTHU+'?campaignRequestId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateDoanhthu(value){
        return this.http.post(APIConstant.UPDATE_DOANHTHU+value.id, value)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
}