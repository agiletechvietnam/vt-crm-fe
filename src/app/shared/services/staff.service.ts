import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import {StaffSearchModel} from "../models/staff.search";
import {StaffModel} from "../models/staff.model";
import 'rxjs/add/operator/map';

@Injectable()
export class StaffService {
    constructor(private http: HttpService) {
        
    }  

    // findStaffs(staffSearch: StaffSearchModel): Observable<StaffModel[]> {
    //     return this.http.get(APIConstant.FIND_STAFFS, staffSearch)
    //         .map(res => res.json())
    //         .catch((error: any) => Observable.throw(error));
    // }
    findStaffs(key: any, page, size): Observable<any> {
        if (key == "" || key == null)
            return this.http.get(APIConstant.FIND_STAFFS+'?page='+page+'&size='+size)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
        return this.http.get(APIConstant.FIND_STAFFS+'?page='+page+'&size='+size+'&textSearch='+key)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
    }
    findStaffsByShopId(key, id, page, size): Observable<any> {
        // return this.http.get(APIConstant.FIND_STAFFS_BY_SHOP_ID+'?shopId='+id)
        //     .map(res => res.json().content)
        //     .catch((error: any) => Observable.throw(error));

        if (key == "" || key == null)
        return this.http.get(APIConstant.FIND_STAFFS_BY_SHOP_ID+'?shopId='+id+'&page='+page+'&size='+size)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
       return this.http.get(APIConstant.FIND_STAFFS_BY_SHOP_ID+'?shopId='+id+'&page='+page+'&size='+size+'&searchText='+key)
                .map(res => res.json())
                .catch((error: any) => Observable.throw(error));
    }
    findStaffsByShopId2(id: number): Observable<any> {
        return this.http.get(APIConstant.FIND_STAFFS_BY_SHOP_ID2+'?shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delStaff(c: any){
        return this.http.delete(APIConstant.DELETE_STAFF+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addStaff(c: any){
        return this.http.post(APIConstant.ADD_STAFF, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateStaff(c: any, id){
        return this.http.post(APIConstant.UPDATE_STAFF+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateRole(c, id){
        return this.http.post(APIConstant.UPDATE_STAFF_ROLE+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}