import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';
import {StoreModel} from '../models/store.model';
@Injectable()
export class StoreService {

    constructor(private http: HttpService) {
        
    }

    findAllStores(): Observable<StoreModel[]> {
        return this.http.get(APIConstant.FIND_SHOPS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    delStore(id, c: StoreModel){
        return this.http.delete(APIConstant.DELETE_SHOP+id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addStore(c: StoreModel){
        return this.http.post(APIConstant.ADD_SHOP, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateStore(c: StoreModel, id){
        return this.http.post(APIConstant.UPDATE_SHOP+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findStoreById(id): Observable<any>{
        return this.http.get(APIConstant.FIND_SHOP_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
}