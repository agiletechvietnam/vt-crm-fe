import { Injectable } from "@angular/core";
import { HttpService } from "../http.service";
import { Observable } from "rxjs";
import { APIConstant } from "../api.constant";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class MediaCampaignService {

    constructor(private http: HttpService) {

    }
    findMediaCampaigns(statusIn): Observable<any[]> {
        return this.http.get(APIConstant.FIND_MEDIA_CAMPAIGNS+'get-by-status-in?statusIn='+statusIn)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findMediaCampaignsByShopId(id): Observable<any[]>{
        return this.http.get(APIConstant.FIND_MEDIA_CAMPAIGNS_BY_SHOP_ID+'?shopId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findMediaCampaignsByStaffId(id): Observable<any[]>{
        return this.http.get(APIConstant.FIND_MEDIA_CAMPAIGNS_BY_STAFF_ID_STATUS_IN+'?statusIn=3,5&staffId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delMediaCampaign(c: any) {
        return this.http.delete(APIConstant.DELETE_MEDIA_CAMPAIGN + c.id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addMediaCampaign(c: any) {
        return this.http.post(APIConstant.ADD_MEDIA_CAMPAIGN, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    updateMediaCampaign(c: any, id) {
        return this.http.post(APIConstant.UPDATE_MEDIA_CAMPAIGN + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    } 

    findMediaCampaignById(id): Observable<any> {
        return this.http.get(APIConstant.FIND_MEDIA_CAMPAIGN_BY_ID + id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    // are to media campaign
    addAreaToMedia(c: any): Observable<any> {
        return this.http.post(APIConstant.ADD_AREA_TO_MEDIA, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateAreaToMedia(c: any, id): Observable<any> {
        return this.http.post(APIConstant.UPDATE_AREA_TO_MEDIA + id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findAreaByMediaId(id): Observable<any> {
        return this.http.get(APIConstant.GET_AREA_BY_MEDIA_ID+'?mediaCampaignId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    delAreaInMedia(c: any) {
        return this.http.delete(APIConstant.DELETE_AREA_IN_MEDIA + c.id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    requestCampaign(c){
        return this.http.post(APIConstant.REQUEST_CAMPAIGN, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    // yeu cau tao chien dich
    requestCampaignDetail(id){
        return this.http.get(APIConstant.REQUEST_CAMPAIGN_DETAIL+ '?requestId='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findRequestCampaign(){
        return this.http.get(APIConstant.FIND_REQUEST_CAMPAIGN )
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findRequestByShopId(id){
        return this.http.get(APIConstant.FIND_REQUEST_BY_SHOP_ID+'?shopId='+id )
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    genShopArea(requestId, shopId){
        return this.http.post(APIConstant.GEN_SHOP_AREA+'?campaignRequestId='+requestId+'&shopId='+shopId, null)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    //new api

    findChannelsOfCampgin(shopId, requestId){
        return this.http.get(APIConstant.FIND_CHANNEL_OF_REQUEST+'?shopId='+shopId+'&campaignRequestId='+requestId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addMediaCampaignDetail(c){
         return this.http.post(APIConstant.ADD_CAMPAIGN_DETAIL, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    updateMediaCampaignDetail(c){
         return this.http.post(APIConstant.UPDATE_CAMPAIGN_DETAIL, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    findGoodsOfCampgin(shopId, requestId){
        return this.http.get(APIConstant.FIND_GOODS_CAMPAIGN+'?shopId='+shopId+'&campaignRequestId='+requestId)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    runSql(c){
        return this.http.post(APIConstant.DOMAIN_RUNSQL, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}


