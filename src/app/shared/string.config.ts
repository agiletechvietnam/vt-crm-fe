import {isUndefined} from "util";
export class StringUtils {
    public static isNullOrEmpty(str: string): boolean {
        return (!str || isUndefined(str) || str.length == 0);
    }

    public static isAnyNullOrEmpty(...arr: string[]): boolean {
        for (let i = 0; i < arr.length; i++) {
            if (this.isNullOrEmpty(arr[i]))
                return false;
        }
        return true;
    }

    public static isAnyNotNullOrNotEmpty(...arr: any[]): boolean {
        for (let i = 0; i < arr.length; i++) {
            if (!this.isNullOrEmpty(arr[i]))
                return true;
        }
        return false;
    }

    public static isEqualInArray(arr: string[], val: string): boolean {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                return true;
            }
        }
        return false;
    }

    public static isEqual(str1: string, str2: string): boolean {
        return str1 == str2;
    }
}