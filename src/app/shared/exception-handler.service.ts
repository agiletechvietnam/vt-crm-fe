import {Injectable} from "@angular/core";
import {ErrorModel} from "./error.model";

@Injectable()
export class ExceptionService {
    error: ErrorModel;

    constructor() {
    }


    handleException(err: any) {
        let errObj = err.json();

        this.error = this.toErrorModel(errObj);
        this.error.errorCode = errObj.errorCode;
        this.error.errorMsg = errObj.errorMsg;
    }

    private toErrorModel(errObj: any): any {

    }


}