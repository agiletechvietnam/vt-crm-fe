export class ErrorModel {
    private _errorCode : string;
    private _errorMsg : string;

    constructor() {
        this._errorCode = '10000';
        this._errorMsg = 'error message get from server';
    }

    get errorCode(): string {
        return this._errorCode;
    }

    set errorCode(value: string) {
        this._errorCode = value;
    }


    get errorMsg(): string {
        return this._errorMsg;
    }

    set errorMsg(value: string) {
        this._errorMsg = value;
    }
}
