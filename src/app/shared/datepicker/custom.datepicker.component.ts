import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';

@Component({
    selector: 'custom-datepicker',
    styleUrls: ['./custom.datepicker.component.css'],
    template: "<div style='position: absolute;z-index: 99;' class='{{cssClass}}' *ngIf='showDatePicker'>"
    + "             <datepicker [(ngModel)]='selectedDate' (selectionDone) = 'selectionDoneEvent($event)'></datepicker>"
    + "         </div>",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomDatepickerComponent {
    @Input() showDatePicker: boolean;
    @Input() cssClass: string;
    @Input() selectedDate: Date;
    @Output() onSelectionDoneEvent: EventEmitter<Date> = new EventEmitter<Date>();


    constructor() {
    }

    selectionDoneEvent(selDate: Date) {
        this.selectedDate = selDate;
        this.onSelectionDoneEvent.emit(selDate);
    }
}

