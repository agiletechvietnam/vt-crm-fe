import { Injectable } from "@angular/core";
import { Http, RequestOptions, Request, Response, RequestOptionsArgs, RequestMethod, Headers, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs";
import { AppSettings } from "../app.config";
import { HostConstant } from "../shared/host.constant";
@Injectable()

export class HttpService {
    private headers;
    constructor(private http: Http) {
        this.headers = new Headers();
        let user = JSON.parse(localStorage.getItem('user'));
        this.headers.append('Authorization', 'Bearer '+user.jwtToken);

    }

    /**
     * build request url
     *
     * @param resource
     * @returns {string}
     */
    buildUrl(resource: string): string {
        return AppSettings.API_ENDPOINT + resource;
    };

    /**
     * send a http request
     *
     * @param method
     * @param url
     * @param body
     * @param options
     * @returns {Observable<Response>}
     */
    request(method: RequestMethod, url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
       
        let requestOptions = new RequestOptions({
            method: method,
            url: this.buildUrl(url),
            body: body
        });//.merge(options);
        
        requestOptions.headers = this.headers;
        let request = new Request(requestOptions);
        let res = this.http.request(request).catch((err:any) => {
            console.log(err);

            
            // localStorage.removeItem('user');
            // window.location.href = HostConstant.HOST;
            return err;
        });
        console.log('du lieu');
        console.log(res);
        
        return res;
    }

    /**
     * send a http get request
     *
     * @param url
     * @param options
     * @returns {Observable<Response>}
     */
    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Get, url, null, options);
    }

    getWithBody(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Get, url, body, options);
    }

    getWithParam(url: string, param: URLSearchParams, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Get, url + "?" + param.toString(), null, options);
    }

    /**
     * send a http post request
     *
     * @param url
     * @param body
     * @param options
     * @returns {Observable<Response>}
     */
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Post, url, body, options);
    }

    /**
     * send a http post request
     *
     * @param url
     * @param body
     * @param options
     * @returns {Observable<Response>}
     */
    put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Put, url, body, options);
    }

    /**
     * send a http delete request
     *
     * @param url
     * @param body
     * @param options
     * @returns {Observable<Response>}
     */
    delete(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(RequestMethod.Delete, url, body, options);
    }

    makeParams(arr: any): string {
        let param: URLSearchParams = new URLSearchParams();
        for (let key in arr) {
            if (arr.hasOwnProperty(key)) {
                param.set(key, arr[key])
            }
        }
        return "?" + param.toString();
    }
}
