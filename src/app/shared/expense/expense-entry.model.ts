
export class ExpenseEntryModel {
    public expenseDate?: string;
    public category?: string;
    public miles?: number;
    public rate?: string;
    public description?: string;
    public incVAT?: string;
    public grossTotal?: string
    public net?: string;
    public refund?: boolean;
    public approver?: string;
    public attachment?: string
    public status?: string
}