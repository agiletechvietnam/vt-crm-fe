import {ProjectInfoModel} from '../../project/project.model'
export class ExpenseModel {
    public clientId?: number;
    public projectId?: number;
    public currencyCode?: string;
    public description?: string;
    public expenseEntries?: ExpenseEntryModel[];

    public currencies?: CurrencyModel[];
    public clients?: ClientModel[];
    public projects?: ProjectInfoModel[];
    public expenseCategories?: ExpenseCategoryModel[];
    public approvers?: ExpenseApproverModel[];

    constructor() {
    }
}

export class ExpenseEntryModel {
    public entryId?: number;
    //start: date picker for each row
    public invoiceDateDatePicker?: Date;
    public invoiceDateDatePickerShow?: boolean = false;
    //end: date picker
    public invoiceDate?: string;
    public categoryId?: number;
    public miles?: number;
    public rate?: string;
    public incVAT?: number;
    public grossTotal?: string;
    public net?: string;
    public refund?: number; //yes,no options
    public selectedApproverEmail?: string;
    public attachment?: string
    public status?: string

    constructor() {
    }
}

export class ExpenseCategoryModel {
    public categoryId?: number;
    public categoryName?: string;
    public categoryDesc?: string;
    constructor() {
    }
}

export class ClientModel {
    public clientId?: number;
    public clientName?: string;

    constructor() {
    }
}

export class CurrencyModel {
    public country?: string;
    public currencyCode?: string;
    public currencyName?: string;
    public symbol?: string;

    constructor() {
    }
}

export class ExpenseApproverModel {
    public email?: string
    public fullName?: string

    constructor() {
    }
}



