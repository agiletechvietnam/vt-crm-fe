export class BackendDate {
    constructor() {
    }

    public toBackendDateTimeString(date: Date): string {
        let year = date.getFullYear(),
            month = date.getMonth() + 1,
            day = date.getDate();
        return year + '-' + this.zeroFill(month, 2) + '-' + this.zeroFill(day, 2);
    }

    public toBackendLocalDateString(date: Date): string {
        let year = date.getFullYear(),
            month = date.getMonth() + 1,
            day = date.getDate();
        return year + '-' + this.zeroFill(month, 2) + '-' + this.zeroFill(day, 2) + 'T00:00:00.000Z';
    }
    public toBackendDate(date: any): Date {
        return new Date(date[0], date[1]-1, date[2]);
    }
    public toBackendDateFromObject(date: any): Date {
        let dateVal = new Date(date.year, date.monthOfYear-1, date.dayOfMonth);
        return dateVal;
    }
    private zeroFill(number, targetLength) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length;

        return Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }
}
