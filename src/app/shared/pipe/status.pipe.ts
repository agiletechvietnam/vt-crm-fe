import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'status' })
export class Status implements PipeTransform {
    transform(value: string, args: string[]): any {
        let status = {};
        status['gender'] = {
            1: 'Nam',
            0: 'Nữ',
            3: 'Khác',
            'M': 'Nam',
            'F': 'Nữ',
            null: 'Tất cả'
        };
        status['program'] = {
            1: '<div class="yellow">Mới tạo</div>',
            2: '<div class="blue">Chờ duyệt</div>',
            3: '<div class="green">Đã duyệt</div>',
            4: '<div class="red">Bị từ chối</div>',
            5: '<div class="xam">Kết thúc</div>'
            //6: '<div class="xam">Đã xóa</div>'
        };

        // this.lydos.push({label: "Tắt máy", value: 8});
		// this.lydos.push({label: "Bận không nghe máy", value: 9});
		// this.lydos.push({label: "Từ chối chăm sóc", value: 10});
		// this.lydos.push({label: "Khác", value: 7});


        status['campaign'] = {
            1: '<div class="yellow">Tạo lỗi</div>',
            2: '<div class="blue">Yêu cầu duyệt</div>',
            3: '<div class="green">Đã duyệt</div>',
            4: '<div class="red">Bị từ chối</div>',
            5: '<div class="orange">Đang chạy</div>',
            6: '<div class="xam">Kết thúc</div>',
            7: '<div class="xam">Khác</div>',
            8: '<div class="xam">Tắt máy</div>',
            9: '<div class="xam">Bận không nghe</div>',
            10: '<div class="xam">Từ chối chăm sóc</div>',
        };
        status['campaignstaff'] = {
            1: '<div class="yellow">Tạo lỗi</div>',
            2: '<div class="blue">Yêu cầu duyệt</div>',
            3: '<div class="green">Đã duyệt</div>',
            4: '<div class="red">Bị từ chối</div>',
            5: '<div class="orange">Đang chạy</div>',
            6: '<div class="green">Đã chăm sóc</div>',
            7: '<div class="xam">Khác</div>',
            8: '<div class="xam">Tắt máy</div>',
            9: '<div class="xam">Bận không nghe</div>',
            10: '<div class="xam">Từ chối chăm sóc</div>',
        };
        status['question'] = {
            1: 'Tự trả lời',
            2: 'Chọn một',
            3: 'Chọn nhiều'
        };
        status['careMethod'] = {
            1: 'Gọi điện',
            2: 'Nhắn tin',
            3: 'Trực tiếp',
            4: 'Qua facebook',
            5: 'Khác'
        };
        status['happycall'] = {
            1: '<div class="yellow">Mới tạo</div>',
            2: '<div class="blue">Yêu cầu duyệt</div>',
            3: '<div class="green">Đã duyệt</div>',
            4: '<div class="red">Bị từ chối</div>',
            5: '<div class="xam">Kết thúc</div>'
        };
        status['target'] = {
            1: '<div class="yellow">Mới tạo</div>',
            2: '<div class="blue">Đã giao</div>',
            3: '<div class="blue">Đã điều chỉnh</div>',
            // 4: '<div class="red">Bị từ chối</div>',
            5: '<div class="xam">Đang chạy</div>',
            6: '<div class="xam">Kết thúc</div>',
        };
        status['area'] = {
            1: 'Trọng điểm',
            2: 'Tiềm năng',
            3: 'Vãng lai'
        };
        status['sourceCustomer'] = {
            0: 'Từ file',
            1: 'Từ SM'
        },
        status['request'] = {
            0: 'Bên trên giao',
            1: 'Từ siêu thị'
        },
        status['survey'] = {
            1: 'Chưa sử dụng',
            2: 'Đã sử dụng'
        }
       
        return status[args.toString()][value];

    }
  
}