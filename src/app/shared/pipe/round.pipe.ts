import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'round' })
export class Round implements PipeTransform {
    transform(input) {
        if (isNaN(input)) return 0;
        return Math.floor(input);

    }

}