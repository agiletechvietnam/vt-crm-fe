import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'gender' })
export class Gender implements PipeTransform {
    transform(value: string, args: string[]): any {
        let genders = ['Nam', 'Nữ'];
        return genders[value];
    }

  
}