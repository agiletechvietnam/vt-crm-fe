import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';

@Component({
    selector: 'custom-pager',
    styleUrls: ['./pager.component.css'],
    template: "<div class='row'>" +
    "   <span class='pull-left' id='pager-info'>Show {{fromRow()}} to {{toRow()}} of {{totalPage}} entries(filtered from {{totalRow}} total entries)</span>" +
    "       <ul class='pagination pull-right' id='pager-link'>" +
    "           <li *ngIf='noOfPages.length > 0' class='previous {{isDisabled(1)}}' (click)='viewPrevPage()'><a>Previous</a></li>" +
    "           <li *ngFor='let no of noOfPages' (click)='viewPage(no)' class='{{isActivePage(no)}}'><a>{{no}}</a></li>" +
    "           <li *ngIf='noOfPages.length > 0' (click)='viewNextPage()' class='next {{isDisabled(totalPage)}}'><a>Next</a></li>" +
    "       </ul>" +
    "</div>",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PagerComponent implements  OnInit {
    @Input() totalRow: number;
    @Input() noOfRowInPage: number;
    private _totalPage: number;
    @Output() selectPage: EventEmitter<number> = new EventEmitter<number>();
    private noOfPages: string[] = new Array<string>();
    private limit: number = 5;
    newLimit: number;
    from: number = 1;
    currentPage: number = 1;
    private more = '...';

    constructor() {
    }

    ngOnInit() {
        this.onInitPager();
    }

    onInitPager(){
        this.from = this.currentPage = 1;
        this.newLimit = this.limit;
        let exceedLimit = this.totalPage > this.limit;
        this.initPager(exceedLimit, exceedLimit ? this.limit : this.totalPage);
    }

    @Input()
    set totalPage(totalPage: number) {
        this._totalPage = totalPage;
        this.onInitPager();
    }

    get totalPage(): number {
        return this._totalPage;
    }

    initPager(exceedLimit: boolean, noOfPageShown: number) {
        while (this.noOfPages.length > 0) {
            this.noOfPages.pop();
        }
        for (let i = this.from; i <= noOfPageShown; i++) {
            this.noOfPages.push(i + '');
        }
        if (exceedLimit) this.noOfPages.push(this.more);
    }

    fromRow() {
        if(this.noOfPages.length == 0) return 0;

        let val = this.noOfRowInPage * this.currentPage - this.noOfRowInPage;
        return val == 0 ? 1 : val;
    }

    toRow() {
        if (this.currentPage == this.totalPage) return this.totalRow;
        return this.noOfRowInPage * this.currentPage;
    }

    isActivePage(page: number): string {
        return page == this.currentPage ? 'active' : '';
    }

    isDisabled(page: number): string {
        return page == this.currentPage ? 'disabled' : '';
    }

    isMorePage(page: string): boolean {
        return page == this.more;
    }

    viewPage(page: string) {
        if (page == this.more) return;
        this.currentPage = Number.parseInt(page);
        this.selectPage.emit(this.currentPage);

        if (this.currentPage == this.newLimit) {
            this.from = this.newLimit;
            this.newLimit = this.currentPage + this.limit;
            let exceedLimit = this.totalPage > this.newLimit;
            this.initPager(exceedLimit, exceedLimit ? this.newLimit : this.totalPage);
        }
    }

    viewNextPage() {
        if (this.currentPage == this.totalPage) return;
        this.viewPage(++this.currentPage + '');
    }

    viewPrevPage() {
        if (this.currentPage == 1) return;

        this.currentPage--;
        this.viewPage(this.currentPage + '');

        if (this.currentPage < this.newLimit - this.limit) {
            this.from = this.newLimit - this.limit * 2;
            if (this.from == 0) this.from = 1;
            this.newLimit -= this.limit;
            let exceedLimit = this.totalPage > this.newLimit;
            this.initPager(exceedLimit, exceedLimit ? this.newLimit : this.totalPage);
        }
    }
}

