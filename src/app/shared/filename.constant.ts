export class FilenameConstant {
    public static customer = 'customer_import.xlsx';
    public static ngansach = 'ngan_sach.xlsx';
    public static doanhthu = 'doanh_thu.xlsx';
    public static giakenh = 'gia_kenh.xlsx';
    public static campaign = 'campaign_template.xlsx';
    public static target = 'target_template.xlsx';
    public static staff = 'import_staff.xlsx';
}
