import {Component} from '@angular/core';
import {AlertModel} from './alert.model';
import {AlertService} from './alert.service';

@Component({
    selector: 'custom-alert',
    template: "<div *ngFor='let alert of alerts'>" +
    "               <alert [type]='alert.type' dismissible='alert.dismissible' [dismissOnTimeout]='alert.timeout'>{{ alert.msg }}</alert>" +
    "          </div>"
})
export class AlertComponent {
    public alerts: AlertModel[] = AlertService.alerts;

    constructor() {
    }

    public add(alert: AlertModel): void {
        this.alerts.push(alert);
    }
}

