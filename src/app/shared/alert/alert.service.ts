import {Injectable} from '@angular/core';
import {AlertModel} from './alert.model';

@Injectable()
export class AlertService{
    public static alerts: AlertModel[] = new Array<AlertModel>();

    constructor() {
    }

    public static reset(): void {
        while(this.alerts.length > 0) {
            this.alerts.pop();
        }
    }
    public static add(alert: AlertModel): void {
        this.alerts.push(alert);
    }
}

