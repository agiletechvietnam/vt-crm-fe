export class AlertModel {
    public type: string = 'success';
    public msg: string;
    public dismissible: boolean = true;
    public timeout: number = 5000;
    const
    alertTypes = {
        success: 'success',
        info: 'info',
        warning: 'warning',
        danger: 'danger'
    };

    constructor(private _msg: string, private  _dismissible?: boolean, private  _timeout?: number) {
        this.msg = _msg;
        this.dismissible = _dismissible;
        if (_timeout) {
            this.timeout = _timeout;
        }
    }
}


