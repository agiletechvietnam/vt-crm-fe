import {HostConstant} from "./host.constant";
export class APIConstant {
    //private static ROOT = 'http://125.212.220.184:8083/';
    //private static ROOT = 'http://192.168.1.244:8083/';
    //private static ROOT = 'http://192.168.1.112:8083/';
    //private static ROOT = 'http://192.168.1.94:8083/';
    //private static ROOT = 'http://192.168.1.94:8083/';
    //private static ROOT = 'http://localhost:8083/';
    //customer
    private static ROOT = HostConstant.API;
    
    public static get FIND_CUSTOMERS(): string {
        return this.ROOT + 'customer/get-all';
        //return this.ROOT + 'customer/';
    }
    //localhost:8083/customer/get-all?page=0&size=50

    public static get FIND_CUSTOMERS_BY_SHOP_ID(): string {
        return this.ROOT + 'shop/get-customer-by-shop-id';
    }
    public static get ADD_CUSTOMER(): string {
        return this.ROOT + 'customer/create/';
    }
    public static get FIND_CUSTOMER_BY_ID(): string{
        return this.ROOT + 'customer/get/';
    }
    public static get UPDATE_CUSTOMER():string{
        return this.ROOT + 'customer/update/';
    }
    public static get DELETE_CUSTOMER():string{
        return this.ROOT + 'customer/delete/';
    }



    //get lich su giao dich, bao hanh
    public static get FIND_TRANSACTIONS():string{
        //return this.ROOT + 'transaction/get-by-phone';
        return this.ROOT + 'transactiondetail/get-by-mobile';
    }
    public static get TRANSACTION_DETAIL():string{
        return this.ROOT + 'transactiondetail/get-by-trans-id';
    }
    public static get FIND_WARRANTY():string{
        return this.ROOT + 'warranty/get-by-phone';
    }
    public static get FIND_TRANSACTION_DETAIL():string{
        return this.ROOT + 'transactiondetail/get-by-trans-id';
    }

    //shop
    public static get FIND_SHOPS(): string{
        return this.ROOT + 'shop/';
    }
    public static get ADD_SHOP(): string {
        return this.ROOT + 'shop/create/';
    }
    public static get FIND_SHOP_BY_ID(): string{
        return this.ROOT + 'shop/get/';
    }
    public static get UPDATE_SHOP():string{
        return this.ROOT + 'shop/update/';
    }
    public static get DELETE_SHOP():string{
        return this.ROOT + 'shop/delete/';
    }

    // target
    public static get FIND_TARGETS(): string{
        return this.ROOT + 'bdkhtarget/';
    }
    public static get FIND_TARGETS_BY_STATUS(): string{
        return this.ROOT + 'bdkhtarget/get-by-status-in';
    }
    public static get ADD_TARGET(): string {
        return this.ROOT + 'bdkhtarget/create/';
    }
    public static get FIND_TARGET_BY_ID(): string{
        return this.ROOT + 'bdkhtarget/get/';
    }
    public static get UPDATE_TARGET():string{
        return this.ROOT + 'bdkhtarget/update/';
    }
    public static get DELETE_TARGET():string{
        return this.ROOT + 'bdkhtarget/delete/';
    }

    public static get FIND_TARGET_VIEW_SHOP():string{
        return this.ROOT + 'bdkhshopareatarget/get-by-target-shop-id';
    }
    
    
    //shop target
    public static get FIND_SHOP_TARGET():string{
        return this.ROOT + 'bdkhshoptarget/';
    }
    public static get FIND_SHOP_TARGET_BY_TARGET():string{
        return this.ROOT + 'bdkhshoptarget/get-by-target-id';
    }
    public static get ADD_SHOP_TARGET():string{
        return this.ROOT + 'bdkhshoptarget/create/';
    }
    public static get UPDATE_SHOP_TARGET():string{
        return this.ROOT + 'bdkhshoptarget/update/';
    }
    public static get DELETE_SHOP_TARGET():string{
        return this.ROOT + 'bdkhshoptarget/delete/';
    }

    //man hinh target dia ban
    //http://localhost:3000/#/phancongnhanvien
    public static get SHOP_AREA_TARGET():string{
        return this.ROOT + 'bdkhshopareatarget/get-by-shop-id';
    }

    //man hinh target cua nhan vien
    public static get FIND_TARGET_STAFF():string{
        return this.ROOT + 'bdkhshopareatarget/get-by-staff-id';
    }

    public static get FIND_TARGETS_BY_SHOP_ID():string{
        return this.ROOT + 'bdkhshoptarget/get-target-by-shop-id';
    }

    public static get FIND_SHOP_TARGETS_BY_SHOP_ID():string{
        return this.ROOT + 'bdkhshoptarget/get-by-shop-id';
    }

    public static get FIND_SHOP_TARGET_BY_ID():string{
        return this.ROOT + 'bdkhshoptarget/get/';
    }
 

    //staff
    public static get FIND_STAFFS(): string {
        return this.ROOT + 'staff/get-staffs';
    }
    public static get FIND_STAFFS_BY_SHOP_ID(): string {
        return this.ROOT + 'staff/get-by-shop-id';
        //return this.ROOT + 'staff/get-staff-by-shop-id-to-assign';
    }
    public static get FIND_STAFFS_BY_SHOP_ID2(): string {
        return this.ROOT + 'staff/get-staff-by-shop-id-to-assign';
    }
    public static get ADD_STAFF(): string{
        return this.ROOT + 'staff/create/'
    }
    public static get UPDATE_STAFF(): string{
        return this.ROOT + 'staff/update/'
    }
    public static get DELETE_STAFF(): string{
        return this.ROOT + 'staff/delete/'
    }

    // channel
    public static get FIND_CHANNELS(): string{
        return this.ROOT + 'bdkhmediachannel/get-all'
    }
    public static get FIND_CHANNEL_BY_ID(): string{
        return this.ROOT + 'bdkhmediachannel/get/'
    }
    public static get ADD_CHANNEL(): string{
        return this.ROOT + 'bdkhmediachannel/create'
    }
    public static get DEL_CHANNEL_FROM_SHOP(): string{
        return this.ROOT + 'bdkhmediachannelrequest/delete-channel'
    }
    public static get UPDATE_CHANNEL(): string{
        return this.ROOT + 'bdkhmediachannel/update/'
    }
    public static get DELETE_CHANNEL(): string{
        return this.ROOT + 'bdkhmediachannel/delete/'
    }
    public static get CHANNEL_SHOP_PRICES():string{
        return this.ROOT + 'bdkhmediashopprice/get-by-media-id'
    }
    public static get UPDATE_SHOP_PRICE():string{
        return this.ROOT + 'bdkhmediashopprice/update/'
    }
    public static get ADD_SHOP_PRICE():string{
        return this.ROOT + 'bdkhmediashopprice/create/'
    }
    public static get DELETE_SHOP_PRICE():string{
        return this.ROOT + 'bdkhmediashopprice/delete/'
    }
    
    public static get ADD_CHANNEL_TO_REQUEST():string{
        return this.ROOT + 'bdkhmediachannelrequest/create/'
    }
    public static get DELETE_CHANNEL_IN_REQUEST():string{
        return this.ROOT + 'bdkhmediachannelrequest/delete/'
    }
    public static get FIND_CHANNEL_IN_REQUEST():string{
        return this.ROOT + 'bdkhmediachannelrequest/get-by-campaign-request-id'
    }

    // department
    public static get FIND_DEPARTMENTS(): string{
        return this.ROOT + 'department/';
    }
    public static get FIND_DEPARTMENT_BY_ID(): string{
        return this.ROOT + 'department/get/'
    }
    public static get ADD_DEPARTMENT(): string{
        return this.ROOT + 'department/create/'
    }
    public static get UPDATE_DEPARTMENT(): string{
        return this.ROOT + 'department/update/'
    }
    public static get DELETE_DEPARTMENT(): string{
        return this.ROOT + 'department/delete/'
    }
    // media campaign
    public static get FIND_MEDIA_CAMPAIGNS(): string{
        return this.ROOT + 'bdkhmediacampaign/';
    }
    public static get FIND_MEDIA_CAMPAIGNS_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhmediacampaign/get-by-shop-id';
    }
    public static get ADD_MEDIA_CAMPAIGN(): string {
        return this.ROOT + 'bdkhmediacampaign/create/';
    }
    public static get FIND_MEDIA_CAMPAIGN_BY_ID(): string{
        return this.ROOT + 'bdkhmediacampaign/get/';
    }
    public static get UPDATE_MEDIA_CAMPAIGN():string{
        return this.ROOT + 'bdkhmediacampaign/update/';
    }
    public static get DELETE_MEDIA_CAMPAIGN():string{
        return this.ROOT + 'bdkhmediacampaign/delete/';
    }
    public static get FIND_MEDIA_CAMPAIGNS_BY_STAFF_ID(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/get-by-staff-id';
    }
    public static get FIND_MEDIA_CAMPAIGNS_BY_STAFF_ID_STATUS_IN(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/get-by-staff-id-status-in';
    }
    //add area to campaign
    public static get  ADD_AREA_TO_MEDIA(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/create/';
    }
    public static get UPDATE_AREA_TO_MEDIA(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/update/';
    }
    public static get GET_AREA_BY_MEDIA_ID(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/get-by-media-campaign-id';
    }
    public static get DELETE_AREA_IN_MEDIA():string{
        return this.ROOT + 'bdkhmediacampaigndetail/delete/';
    }

    
    // campaign
    public static get FIND_CAMPAIGNS(): string{
        return this.ROOT + 'campaign/get-all';
    }
    public static get FIND_CAMPAIGNS_BY_STATUS(): string{
        return this.ROOT + 'campaign/get-by-status-in';
    }
    public static get ADD_CAMPAIGN(): string {
        return this.ROOT + 'campaign/create/';
    }
    // public static get FIND_CAMPAIGN_BY_ID(): string{
    //     return this.ROOT + 'campaign/get/';
    // }
    public static get FIND_CAMPAIGN_BY_ID(): string{
        return this.ROOT + 'campaign/get-by-id';
    }
    public static get UPDATE_CAMPAIGN():string{
        return this.ROOT + 'campaign/update/';
    }
    public static get DELETE_CAMPAIGN():string{
        return this.ROOT + 'campaign/delete/';
    }
    
    public static get FIND_CUSTOMER_CAMPAIGN_BY_SHOP_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-shop-id';
    }
    public static get FIND_CAMPAIGN_BY_SHOP_ID(): string{
        return this.ROOT + 'campaign/get-by-shop-id';
    }
    public static get CAMPAIGN_ASSIGN_SHOP(): string{
        return this.ROOT + 'campaign/assign-shop';
    }
    public static get FIND_CUSTOMER_BY_PHONE():string{
        //return this.ROOT + 'customer/get-by-mobile';
        return this.ROOT + 'campaigncustomer/get-by-mobile';
    }

    //cap nhap ket qua
    public static get UPDATE_CAMPAIGNCUSTOMER():string{
        return this.ROOT + 'campaigncustomer/update/';
    }
    // lay list shop cua campaing
    public static get FIND_CAMPAIGNSHOP_BY_CAMPAIGN_ID():string{
        return this.ROOT + 'campaignshop/get-by-campaign-id';
    }
    

    public static get FIND_STAFFS_CAMPAIGN_BY_SHOP_ID(): string{
        return this.ROOT + 'staff/get-saff-campaign-by-shop-id'; //?campaignId=1873&shopId=57099
    }

    public static get FIND_RESULT_STAFFS_CAMPAIGN_BY_SHOP_ID(): string{
        return this.ROOT + 'campaignstaff/get-by-campaign-shop-id'; //?campaignId=1873&shopId=57099
    }
    public static get FIND_CAMPAIGN_BY_STAFF_ID(): string{
        return this.ROOT + 'campaignstaff/get-campaign-by-staff-id';
    }
    
    public static get FIND_RESULT_CUSTOMERS_CAMPAIGN_BY_SHOP_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-campaign-staff-id'; //?campaignId=1873&staffId=39671
    }


    //lay danh sach campaign cua sieu thi
    public static get FIND_CAMPAIGNSHOP_BY_SHOP_ID(): string{
        return this.ROOT + 'campaignshop/get-by-shop-id';
    }

    // cap nhap
    public static get UPDATE_CAMPAIGNSHOP(): string{
        return this.ROOT + 'campaignshop/update/';
    }

    public static get FIND_CAMPAIGNSHOP(): string{
        return this.ROOT + 'campaignshop/get/';
    }

    //lay campaing cua sieu thi

    // man hinh campaign nvtt
    public static get FIND_CAMPAIGN_CUSTOMERS_BY_SHOP_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-shop-id';
    }
    public static get FIND_CAMPAIGN_CUSTOMERS_BY_CAMPAIGN_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-campaign-id';
    }

    public static get FIND_CAMPAIGN_CUSTOMERS_BY_CAMPAIGN_SHOP_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-campaign-shop-id';
    }
    //phân công
    public static get ASSIGN_STAFF_MANUAL(){
        return this.ROOT + 'campaigncustomer/assign-staff-manual';
    }

    public static get ASSIGN_STAFF_MANUAL_AGIAN(){
        return this.ROOT + 'campaigncustomer/re-assign-staff-manual';
    }
    public static get ASSIGN_STAFF_AUTO(){
        return this.ROOT + 'campaigncustomer/assign-staff-auto';
    }
    /**************/
    public static get FIND_CUSTOMERS_BY_CAMPAIGN_ID(): string{
        return this.ROOT + 'campaigncustomer/get-by-campaign-id';
    }
    
    
    //city
    public static get FIND_CITIES(): string {
        //return 'http://58ac1355f989751200f99309.mockapi.io/store/city';
        return this.ROOT + 'area/get-list-city';
    }
    public static get FIND_POPULATION(): string{
        //return this.ROOT + 'areapopulation/';
        return this.ROOT + 'area/get-all-ward';
    }
    public static get UPDATE_POPULATION(): string{
        return this.ROOT + 'area/update/';
    }
    //area
    public static get FIND_AREA(): string{
        return this.ROOT + 'bdkhareashop/get/';
    }
    public static get FIND_AREAS_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareashop/get-by-shop-id';
    }
    public static get ADD_AREA_SHOP(): string{
        return this.ROOT + 'bdkhareashop/create/';
    }
    public static get UPDATE_AREA_SHOP(): string{
        return this.ROOT + 'bdkhareashop/update/';
    }
    public static get DELETE_AREA_SHOP(): string{
        return this.ROOT + 'bdkhareashop/delete/';
    }
    //http://192.168.1.94:8083/area/get-city-by-center-code-in?centerCodeIn=1,2
    public static get FIND_CITY_BY_CENTER_CODE_IN(): string{
        return this.ROOT + 'area/get-city-by-center-code-in';
    }

    //danh sach cac vung
    public static get FIND_AREAS(): string{
        return this.ROOT + 'domain/get-list-area/';
    }
    //lay sanh sach tinh theo ma vung
    public static get FIND_CITY_BY_CENTER_CODE(): string{
        return this.ROOT + 'area/get-city-by-center-code';//?centerCode={code}
    }
    // lay sanh sach shop theo tinh
    public static get FIND_SHOPS_BY_PROVINCE_IN():string{
        return this.ROOT + 'shop/get-by-province-in'; //?province={province}
    }

    public static get FIND_COMPETITOR_BY_AREA_CODE(): string{
        return this.ROOT + 'bdkhareacompetitor/get-by-pay-area-code';
    }
    public static get FIND_COMPETITOR_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareacompetitor/get-by-shop-id';
    }
    public static get ADD_COMPETITOR(): string{
        return this.ROOT + 'bdkhareacompetitor/create/';
    }
    public static get UPDATE_COMPETITOR(): string{
        return this.ROOT + 'bdkhareacompetitor/update/';
    }
    public static get DELETE_COMPETITOR(): string{
        return this.ROOT + 'bdkhareacompetitor/delete/';
    }
    public static get FIND_GROUP_BY_AREA_ID(): string{
        return this.ROOT + 'bdkhareacompetitor/get-by-pay-area-code';
    }
    public static get FIND_COMPANYS_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareashopdetail/get-by-shop-id-and-type?type=1&';
    }
    public static get FIND_MARKETS_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareashopdetail/get-by-shop-id-and-type?type=2&';

    }
    public static get FIND_DV_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareashopdetail/get-by-shop-id-and-type?type=3&';
    }
    public static get FIND_SCHOOLS_BY_SHOP_ID(): string{
        return this.ROOT + 'bdkhareashopdetail/get-by-shop-id-and-type?type=4&';
    }
    public static get ADD_AREA(): string{
        return this.ROOT + 'bdkhareashopdetail/create/';
    }
    public static get UPDATE_AREA(): string{
        return this.ROOT + 'bdkhareashopdetail/update/';
    }
    public static get DELETE_AREA(): string{
        return this.ROOT + 'bdkhareashopdetail/delete/';
    }

    //localhost:8083/district/get-by-city-id?cityId=1
    public static get FIND_DISTRICTS_BY_CITY_ID(): string{
        return this.ROOT + 'area/get-list-district';
    }

    //125.212.220.184:8083/ward/get-by-district-id?districtId=11
    public static get FIND_WARDS_BY_DISTRICT_ID(): string{
        return this.ROOT + 'area/get-list-ward';
    }

    //to dan pho
    public static get FIND_POPGGROUP_BY_AREA_CODE(): string{
        return this.ROOT + 'areapopgroup/get-by-pay-area-code';
    }
    public static get ADD_POPGGROUP(): string{
        return this.ROOT + 'areapopgroup/create/';
    }
    public static get UPDATE_POPGGROUP(): string{
        return this.ROOT + 'areapopgroup/update/';
    }
    public static get DELETE_POPGGROUP_BY_AREA_CODE(): string{
        return this.ROOT + 'areapopgroup/delete/';
    }

    //nganh hang
    public static get FIND_PRODUCT_TYPE(): string{
        return this.ROOT + 'domain/get-list-product-type/';
    }
    //nhom hang
    public static get FIND_GOODSGROUP_BY_PRODUCT_TYPE(): string{
        return this.ROOT + 'goodsgroup/get-by-product-in-type'; //?productInType={code}';
    }
    //Mat hang
    public static get FIND_GOODS(): string{
        return this.ROOT + 'goods'; //?productType={code}';
    }
    
    //mat hang
    public static get FIND_GOODS_BY_GROUP_ID(): string{
        return this.ROOT + 'goods/get-by-group-id-in'; //?groupIdIn={groupId}
    }
    public static get FIND_GOODS_BY_KEY(): string{
        return this.ROOT + 'goods/get-by-key-word'; //?groupIdIn={groupId}
    }

    public static get ADD_GOODS_TO_CAMPAIGN(): string{
        return this.ROOT + 'bdkhmediacampaigngoods/create';
    }
    public static get DELETE_GOODS_TO_CAMPAIGN(): string{
        return this.ROOT + 'bdkhmediacampaigngoods/delete/';
    }
    public static get FIND_GOODS_IN_REQUEST():string{
        return this.ROOT + 'requestcampaign/get-goods-by-id';
    }

    public static get FIND_SHOP_IN_REQUEST():string{
        return this.ROOT + 'requestcampaign/get-media-campaign-shop-by-id';
    }

    public static get ADD_SHOP_TO_REQUEST():string{
        return this.ROOT + 'bdkhmediacampaign/create';
    }
    public static get FIND_REQUEST():string{
        return this.ROOT + 'requestcampaign/get/';
    }
    
    public static get GEN_SHOP_AREA(): string{
        return this.ROOT + 'bdkhmediacampaign/gen-shop-area-target';
    }

    //run sql
    public static get RUNSQL(): string{
        return this.ROOT + 'campaign/run-sql';//?campaignId='
    }
    public static get DOMAIN_RUNSQL(): string{
        return this.ROOT + 'domain/get-sql-result';//?campaignId='
    }

    //survey
    public static get FIND_SURVEYS(): string{
        return this.ROOT + 'survey/get-all';
    }
    public static get FIND_SURVEY_BY_ID(): string{
        return this.ROOT + 'survey/get/';
    }
    public static get ADD_SURVEY(): string{
        return this.ROOT + 'survey/create/';
    }
    public static get UPDATE_SURVEY(): string{
        return this.ROOT + 'survey/update/';
    }
    public static get DELETE_SURVEY(): string{
        return this.ROOT + 'survey/delete/';
    }

    //question
    public static get FIND_QUESTIONS(): string{
        return this.ROOT + 'question/';
    }
    public static get FIND_QUESTIONS_ANSWERS(): string{
        return this.ROOT + 'question/get-all';
    }
    public static get FIND_QUESTIONS_BY_SURVEY_ID(): string{
        return this.ROOT + 'question/get-by-survey-id';//?surveyId=1270';
    }

    public static get ADD_QUESTION_TO_SURVEY(): string{
        return this.ROOT + 'surveyquestion/create/';
    }

    public static get DELETE_QUESTION_IN_SURVEY(): string{
        return this.ROOT + 'surveyquestion/delete/';
    }
    
    public static get FIND_QUESTION_BY_ID(): string{
        return this.ROOT + 'question/get/';
    }
    public static get ADD_QUESTION(): string{
        return this.ROOT + 'question/create/';
    }
    public static get UPDATE_QUESTION(): string{
        return this.ROOT + 'question/update/';
    }
    public static get DELETE_QUESTION(): string{
        return this.ROOT + 'question/delete/';
    }
    
    //answer
    public static get FIND_ANSWERS_BY_QUESION_ID(): string{
        return this.ROOT + 'questionresponse/get-by-question-id';
    }
    public static get ADD_ANSWER(): string{
        return this.ROOT + 'questionresponse/create/';
    }
    public static get UPDATE_ANSWER(): string{
        return this.ROOT + 'questionresponse/update/';
    }
    public static get DELETE_ANSWER(): string{
        return this.ROOT + 'questionresponse/delete/';
    }
    //lich su giao dich
    public static get HISTORY_TRAN(): string{
        return this.ROOT + 'questionresponse/delete/';
    }   
    public static get HISTORY_TRAN1(): string{
        return this.ROOT + 'questionresponse/delete/';
    } 
    //lich su bao hanh

    // yeu cau tao chien dich
    public static get REQUEST_CAMPAIGN(): string{
        return this.ROOT + 'requestcampaign/create/';
    }
    public static get FIND_REQUEST_CAMPAIGN(): string{
        return this.ROOT + 'requestcampaign/get-all';
    }
    public static get REQUEST_CAMPAIGN_DETAIL(): string{
        return this.ROOT + 'requestcampaign/get-by-id'; // ?requestId=1';
    }
    public static get FIND_REQUEST_BY_SHOP_ID(): string{
        return this.ROOT + 'requestcampaign/get-by-shop-id';
    }
    public static get FIND_DOANHTHU(): string{
        return this.ROOT + 'bdkhmediashoptarget/get-by-campaign-request-id';
    }
    public static get UPDATE_DOANHTHU(): string{
       // return this.ROOT + 'bdkhmediashoptarget/update/';
       return this.ROOT + 'mediashopareatarget/update/';
    }
    // lay survey
    public static get FILL_SURVEY(): string{
        return this.ROOT + 'survey/get-by-survey-id';//?surveyId=27720';
    }
    public static get ALL_SURVEY(): string{
        return this.ROOT + 'survey/get-all';//?surveyId=27720';
    }
    
    public static get UP_SURVEY():string{
        return this.ROOT + 'campaigncustomer/submit-response';
    }
    public static get RESULT_SURVEY():string{
        return this.ROOT + 'survey/get-response-by-campaign-cus-id';
    }
    //thong bao
    public static get STAFF_NOTIFICATION():string{
        return this.ROOT + 'notification/get-message-by-staff-id';
    }
    public static get VERIFY_CAMPAIGN():string{
        return this.ROOT + 'campaign/verify-campaign';
    }
    public static get UPDATE_TARGET_STAFF():string{
        return this.ROOT + 'bdkhshopareatarget/update-targets';
    }
    public static get UPDATE_STAFF_TARGET():string{
        return this.ROOT + 'bdkhshopareatarget/update/';
    }
    public static get UPDATE_STAFF_ROLE():string{
        return this.ROOT + 'staff/update/';
    }

    public static get FIND_CHANNEL_OF_REQUEST():string{
        return this.ROOT + 'bdkhmediashopprice/get-by-shop-id';
    }

    public static get ADD_CAMPAIGN_DETAIL(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/create-campaign-detail';
    }
    public static get UPDATE_CAMPAIGN_DETAIL(): string{
        return this.ROOT + 'bdkhmediacampaigndetail/update-campaign-detail';
    }
    public static get FIND_GOODS_CAMPAIGN(): string{
        return this.ROOT + 'bdkhmediashoptarget/get-by-campaign-request-and-shop-id';
    }

    public static get FIND_MEDIA_CHANNEL():string{
        return this.ROOT + 'bdkhmediachannel/get-all';
    }
}


