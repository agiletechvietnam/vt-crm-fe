export class StaffSearchModel {
    area: string;
    age = {
        from: 1,
        to: 100
    }
    gender: number;
    job: string;
    hobbit: string;
    fromDate: Date;
    toDate: Date;
    keySearch: string;

}