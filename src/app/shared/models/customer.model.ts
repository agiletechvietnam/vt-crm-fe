export class CustomerModel {
    firstName : string;
    lastName: string;
    mobile: string;
    code: string;
    birthday: string;
    id: number;
    payAreaCode: string;
    cityId:string;
    districtId: string;
}