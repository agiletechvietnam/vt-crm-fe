import { QuestionModel } from './question.model';
export class SurveyModel {
    id: number;
    name: string;
    questions : QuestionModel[];    
}