export class RequestModel {
    id: number;
    content: string;
    mediaId: string;
}