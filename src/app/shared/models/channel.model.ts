export class ChannelModel {
    id: number;
    name: string;
    code: string;
    description: string;
}