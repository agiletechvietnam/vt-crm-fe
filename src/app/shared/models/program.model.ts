export class ProgramModel {
    id: number;
    name : string;
    startDate: string;
    endDate: string;
    status: number;
    
}