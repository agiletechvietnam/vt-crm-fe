export class CampaignModel {
    id: number;
    name: string;
    areaJson: string;
    viettelstoreJson: string;
    gender: number;
    fromAge: number;
    toAge: number;
    startDate: string;
    endDate: string;
    productGroup: number;
    status: number;
}