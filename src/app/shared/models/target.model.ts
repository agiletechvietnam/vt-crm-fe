export class TargetModel {
    id: number;
    name: string;
    status: number;
    startDate: string;
    endDate: string;
    targetDescription: string;
    rating: number;
}