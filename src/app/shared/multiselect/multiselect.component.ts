import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';

@Component({
    selector: 'multi-select',
    styleUrls: ['./multiselect.component.css'],
    template:
    "<div style='position: absolute;'>"
    +"<div style='background: transparent;padding:0;' class='btn dropdown-toggle btn-default' >"
    +"  <span class='list-group-item pull-left'  style='margin: 0 5px 0 0;' *ngIf='noItem()'>Select item</span>"
    +"  <span class='list-group-item pull-left'  style=' margin: 0 5px 0 0;' *ngFor='let ad of selectedItems'>"
    +"	<button type='button' class='close text-select' aria-label='Close'(click)='removeItem(ad)'>{{showLabel(ad)}}<span aria-hidden='true' >&times;</span></button>"
    +"  </span>"
    +"  <div (click) = 'show = !show' style='border: transparent;' class='pull-left list-group-item glyphicon glyphicon-triangle-bottom'></div>"
    +"  <div style='clear: both;'></div>"
    +"</div>"
    +"<ul class='list-group' *ngIf='show'>"
    +"	<li class='list-group-item' (click)='selectItem(a)' *ngFor='let a of inputItems'>{{showLabel(a)}}<span *ngIf='isSelected(a)' class='pull-right glyphicon glyphicon-ok'></span></li>"
    +"</ul><div class='help-block has-error' >{{errorMessage}}</div>"
    +"</div>",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiSelectComponent implements  OnInit {
    @Input() show: boolean;
    @Input() selectedItems: any =[] ;
    @Input() inputItems: any;
    @Input() label: string;
    @Input() errorMessage: string;

    constructor() {
    }

    ngOnInit() {
    }

    selectItem(item: any) {
        let idx = this.checkExisted(item);
        if (idx > -1) {
            this.removeItemAndReset(item, idx);
        } else {
            item.selected = true;
            this.selectedItems.push(item);
            this.errorMessage = '';
        }
    }

    showLabel(item: any): string{
        return item[this.label];
    }
    noItem(): boolean {
        return this.selectedItems == null || this.selectedItems.length == 0;
    }
    removeItem(item: any) {
        let idx = this.checkExisted(item);
        this.removeItemAndReset(item, idx);
    }
    private removeItemAndReset(item: any, idx: number) {
        item.selected = false;
        this.selectedItems.splice(idx, 1);
    }
    private checkExisted(item: any): number {
        for (let i = 0; i < this.selectedItems.length; i++) {
            if (this.selectedItems[i] == item) {
                return i;
            }
        }
        return -1;
    }
    isSelected(item: any): boolean {
        return this.checkExisted(item) > -1;
    }
}

