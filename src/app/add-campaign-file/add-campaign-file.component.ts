import {Component, OnInit, ViewChild} from "@angular/core";
import { CampaignService } from "../shared/services/campaign.service";
import { CampaignModel } from "../shared/models/campaign.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import { Router} from '@angular/router';
import {StoreService} from '../shared/services/store.service';
import {GoodsService} from '../shared/services/goods.service';
import {AreaService} from '../shared/services/area.service';
import {SurveyService} from '../shared/services/survey.service';
import { HostConstant } from "../shared/host.constant";
import { FilenameConstant } from "../shared/filename.constant";
import {FileUpload} from 'primeng/primeng';
import {CustomerService} from "../shared/services/customer.service";
@Component({
    selector: 'app-add-campaign-file',
    templateUrl: './add-campaign-file.component.html',
    styleUrls: ['./add-campaign-file.component.css']
})
export class AddCampaignFileComponent implements OnInit {
	@ViewChild('upfile') upfile: FileUpload;

    private items:any[] = [];
    private mes: Message[];
	private form: FormGroup;
	private formdc: FormGroup; 
	private genders: SelectItem[];
	private customers: any[] = []; 
	private shops: any[] = []; 
	private isLoading: boolean = false;
	private cshops: any[] = [];
	private id: Number = 0;
	private uploadForm: FormGroup;
	private areas: SelectItem[] = [];
	private provinces: SelectItem[] = [];

    private displayForm: boolean = false;
    private cangiao: number = 0;
    private name;
    private dagiao: number = 0;
    private chuathem: boolean = true;
    private chuagui: boolean = true;
	private daupload: boolean = true;
	// private slectedAreas: any[] = [];
	// private slectedProvinces: any[] = [];
	// private slectedShops: any[] = [];

	private domains: SelectItem[] = [];
	private goodsGroups: SelectItem[] = [];
	private goods: SelectItem[] = [];

	private surveys: SelectItem[] = [];
	// private slectedDomains: any[] = [];
	// private slectedGoodsGroups: any[] = [];
	// private slectedGoods: any[] = [];
	private api = HostConstant.API;
	private host = HostConstant.HOST;
	private customer: any = {};
	public displayCustomer:boolean = false;
	private transactions: any[] = [];
    private warrantys: any[] = [];

	private user;
	constructor(
        private service: CampaignService,
		private storeService: StoreService,
		private group: StoreService,
		private goodsService: GoodsService,
        private fb: FormBuilder,
		private router: Router,
		private surveyService: SurveyService,
		private customerService: CustomerService,
		private areaService: AreaService) { }
	ngOnInit() {
		this.genders = [];
		this.genders.push({label:'Tất cả', value: null}); 
		this.genders.push({label:'Nam', value: 1});
		this.genders.push({label:'Nữ', value: 0});
		
		this.uploadForm = this.fb.group({
			'file': new FormControl(),
		});
		this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'code': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(254), this.NoWhitespaceValidator])),
			'gender': new FormControl(),
			'fromTransDate': new FormControl('', Validators.compose([ Validators.minLength(5), Validators.maxLength(50)])),
			'toTransDate': new FormControl('', Validators.compose([ Validators.minLength(5), Validators.maxLength(50)])),
			'startDate': new FormControl('', Validators.compose([ Validators.required,Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([ Validators.required, Validators.minLength(5), Validators.maxLength(50)])), 
			'fromAge': new FormControl(),
			'toAge': new FormControl(),
			'fromTime': new FormControl(),
			'toTime': new FormControl(),
			'fromAmount': new FormControl(),
			'toAmount': new FormControl(),
			'surveyId': new FormControl(),
			'campaignDescription': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
			'shops': new FormControl(),
			'selectedAreas': new FormControl(),
			'selectedProvinces': new FormControl(),
			'selectedShops': new FormControl(),
			'selectedDomains': new FormControl(),
			'selectedGoodsGroups': new FormControl(),
			'selectedGoods': new FormControl()
		});
        this.formdc = this.fb.group({
			'id': new FormControl(),
			'customerNumber': new FormControl('', Validators.compose([Validators.required])),
		});
		
		


		this.loadSurvey();
	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	
	vDate(d){
		let date = new Date(d);
		return date;
	}
	loadSurvey(){
		this.isLoading = true;

		this.surveyService.findSurveys().subscribe(res => {
            this.surveys.push({label: "Chọn survey", value: null});
			for (let i=0; i<res.length; i++){
				this.surveys.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error => {});
	}
	

	initSelect(e, res){
		for (let i=0; i<res.length; i++)
			this.areas.push({label: res[i].name, value: res[i]});
	}

	//load địa chỉ
	loadAreas(){
		this.areaService.findAreas().subscribe( res => {
			this.initSelect(this.areas, res);	
		}, error => {});
	}
	AreaChange(){
        this.form.value.selectedProvinces = [];
		this.form.value.selectedShops = [];
		if (this.form.value.selectedAreas.length == 0) {
			
			return;
		}
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedAreas.length; i++){
			if (i <this.form.value.selectedAreas.length - 1)	
				ids += this.form.value.selectedAreas[i].code + ',';
			else 
				ids += this.form.value.selectedAreas[i].code;
		}

		this.areaService.findCityByCenterCodeIn(ids).subscribe(res => {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			this.provinces = [];
			for (let i=0; i<res.length; i++){
				this.provinces.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	ProvinceChange(){
        this.form.value.selectedShops = [];
		if (this.form.value.selectedProvinces.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedProvinces.length; i++){
			if (i <this.form.value.selectedProvinces.length - 1)	
				ids += this.form.value.selectedProvinces[i].payAreaCode+ ',';
			else 
				ids += this.form.value.selectedProvinces[i].payAreaCode;
		}

		this.areaService.findShopByProvinceIn(ids).subscribe(res => {
			
			this.shops = [];
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});	
		
	}
	loadCustomers(){
		this.isLoading = true;
		this.service.findCustomersByCampaignId(this.id).subscribe(
			res => {
				this.isLoading = false;
				this.customers = res;
			},
			error => {}
		)
	}
	loadShops(){
		this.isLoading = true;
		this.service.findShopCampaignByCampaignId(this.id).subscribe(
			res => {
				this.isLoading = false;
				this.shops = res;
			},
			error => {}
		)
	}

	onSubmit(value:any){
		if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
			 return;
		}
		if (this.form.controls['endDate'].value.toDateString() == this.form.controls['startDate'].value.toDateString()){
			this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc ít nhất 1 ngày');
			 return;
		}
		if ((new Date()).toDateString() != this.form.controls['startDate'].value.toDateString() && this.form.controls['startDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải lớn hơn hoặc bằng ngày hiện tại');
			 return;
		}
		
		this.isLoading = true;

		if (value.gender == null) delete value.gender;
		
		if (value.surveyId != null){
			value.surveyName = value.surveyId.name;
			value.surveyId = value.surveyId.id;
		}
		value.createdId = this.user.id;
		this.service.addCampaign(value).subscribe(
			res => {
                this.chuathem = false;
				this.isLoading = false;
				this.id = res.id;
				this.daupload = false;
                this.name = res.name;
				this.showMes('success', 'Thêm thành công');
			},
			error => {}
		);

	}
    
	guiDuyet(){
        if (this.cangiao != this.dagiao) {
            this.showMes('error', 'Số khách hàng đã giao phải bằng tổng số khách hàng');
            return;
        }
		this.isLoading = true;
        let data = {
            id: this.id,
            name: this.name,
            shops: this.items
        };
        this.service.assignShop(data).subscribe(res => {
            this.service.updateCampaign({status: 2}, this.id).subscribe(
                res => {
                    this.chuagui = false;
                    this.isLoading = false;
                    this.showMes('success','Gửi duyệt thành công');
					
                },
                error => {}
            )
        }, error => {})
		
	}
	
	shopChange(){
		let t = this.form.controls['shops'].value;
		this.items = [];
		for (let i=0; i<t.length; i++){
			this.items.push({name: t[i].name, id: t[i].id});
		}
	}
	delete(item){
		let t = this.form.controls['shops'].value;
		for (let i=0; i<t.length; i++){
			if (t[i].id == item.id){
				t.splice(i, 1);
				this.items.splice(i, 1);
				return;
			}
				
		}
	}
    update(item){
        this.displayForm = true;
        this.formdc.controls['id'].setValue(item.id, { onlySelf: true });
        this.formdc.controls['customerNumber'].setValue(item.customerNumber, { onlySelf: true });
    }
    onSubmitDc(value){
        for (let i=0; i<this.items.length; i++){
            if (this.items[i].id == value.id){

                this.items[i].customerNumber = value.customerNumber;
                break;
            }
        }
        this.dagiao = 0;
        for (let i=0; i< this.items.length; i++){
            console.log(i+' '+this.items[i].customerNumber);
            this.dagiao +=parseInt(this.items[i].customerNumber);
        }
        this.displayForm = false;
    }
    onBeforeUpload(event){
        this.isLoading = true;
    }
	onBeforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
	onUpload(event) {
        this.isLoading = false; 
        this.cangiao = event.xhr.response;
        this.dagiao = this.cangiao;
		this.daupload = true;
		this.isLoading = true;
		Promise.all([this.loadShops()]).then(values => { 
			this.isLoading = false;
		});

        
    }
	onSelect(event){
        if (event.files[0].name != FilenameConstant.campaign){  
            this.upfile.clear();
            this.showMes('error', 'File cần có tên là '+ FilenameConstant.campaign);
        }
    }

	loadCustomer(phone){
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
	viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 4000);
	}
	

}
