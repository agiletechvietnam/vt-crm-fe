import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
@Component({
    selector: 'app-area-shop',
    templateUrl: './area-shop.component.html',
    styleUrls: ['./area-shop.component.css']
})
export class AreaShopComponent implements OnInit {
    public subscription: Subscription;
    private items: any[] = [];
    private isLoading: boolean =  false;
    constructor(
		private activatedRoute: ActivatedRoute,
        private service: MediaCampaignService,
	) { }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.loadItems(params['id']);
        });
    }

    loadItems(shop_id){
        this.isLoading = true;
        let query = {
            "sql": "select * from CRM_BDKH_AREA_SHOP where CRM_BDKH_AREA_SHOP.shop_id = "+shop_id
        };
        this.service.runSql(query).subscribe(res => {
            this.items = res;
            this.isLoading = false;
        }, error => {})

    }
} 
