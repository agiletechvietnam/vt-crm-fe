/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AreaShopComponent } from './area-shop.component';

describe('AreaShopComponent', () => {
  let component: AreaShopComponent;
  let fixture: ComponentFixture<AreaShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
