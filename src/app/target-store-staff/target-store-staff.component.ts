import { Component, OnInit } from '@angular/core';
import { TargetService } from '../shared/services/target.service';
import { TargetModel } from '../shared/models/target.model';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {HostConstant} from '../shared/host.constant';
@Component({
    selector: 'app-target-store-staff',
    templateUrl: './target-store-staff.component.html',
    styleUrls: ['./target-store-staff.component.css']
})
export class TargetStoreStaffComponent implements OnInit {
    public subscription: Subscription;
    private items: any[] = [];
    private mes: Message[];
    private isLoading: boolean =  false;
    private user;
    private formLoyal: FormGroup;
    private formOld: FormGroup;
    private displayLoyalForm: boolean = false;
    private displayOldForm: boolean = false;
    private bdkhTargetId;
    private oldTotalLoyalCus: number = 0;
    private oldTotalOldCus: number = 0;
    private newTotalLoyalCus: number = 0;
    private newTotalOldCus: number = 0;
    private chodieuchinh: boolean = false;
    constructor(
        private service: TargetService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem("user"));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.bdkhTargetId = params['id'];
            this.loadItems();
            this.isLoading = true;
            this.service.findTargetById(this.bdkhTargetId).subscribe(res=>{
                localStorage.setItem('target', JSON.stringify(res));
                localStorage.setItem('api', HostConstant.API + 'bdkhshopareatarget/get-by-target-shop-id?shopId='+this.user.shopId+'&bdkhTargetId='+this.bdkhTargetId);
                if (res.status == 2) this.chodieuchinh = true;
                this.isLoading = false;
            }, error => {this.isLoading = false;});
        });
        this.formLoyal = this.fb.group({
			'id': new FormControl(),
            'targetLoyalCus': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
		});

        this.formOld = this.fb.group({
			'id': new FormControl(),
			'targetOldCus': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
		});
        
    }
    updateLoyal(c){
        this.displayLoyalForm = true;
        this.formLoyal.reset();
        this.formLoyal.controls['id'].setValue(c.id, { onlySelf: true });
        this.formLoyal.controls['targetLoyalCus'].setValue(c.targetLoyalCus, { onlySelf: true });
    }
    updateOld(c){
        this.displayOldForm = true;
        this.formOld.reset();
        this.formOld.controls['id'].setValue(c.id, { onlySelf: true });
        this.formOld.controls['targetOldCus'].setValue(c.targetOldCus, { onlySelf: true });
    }
    loadItems(){
		this.isLoading = true;
		this.service.findShopAreaTargetByShopTargetId(this.bdkhTargetId, this.user.shopId).subscribe(
			res => {
                this.oldTotalLoyalCus = 0;
                this.oldTotalOldCus = 0;
    
				this.items = res;
				for (let i=0; i<res.length; i++){
                    this.oldTotalLoyalCus += res[i].targetLoyalCus;
                    this.oldTotalOldCus += res[i].targetOldCus;
                    if (res[i].staffName == 'Truong sieu thi')
                        res[i].shopFriendly = 3;
                }
                this.newTotalLoyalCus = this.oldTotalLoyalCus;
                this.newTotalOldCus = this.oldTotalOldCus;

                this.isLoading = false;
			},
			error => {
				error => console.log(error);
			}
		);
	}
    baocao(){
        window.open(HostConstant.HOST+'/baocao/export_chitieu_st.html');
    }
    dieuchinh(){
        if (this.oldTotalLoyalCus != this.newTotalLoyalCus || this.oldTotalOldCus != this.newTotalOldCus){
            this.showMes('warn', 'Số lượng khách hàng sau khi điều chỉnh phải không đổi');
            return;
        }
        this.isLoading = true;
        let datas = [];
        for (let i=0; i<this.items.length; i++){
            let data = {
                id: this.items[i].id,
                targetLoyalCus: this.items[i].targetLoyalCus,
                targetOldCus: this.items[i].targetOldCus
            }
            datas.push(data);
        }
        this.service.updateSaffTarget(datas).subscribe(res => {
            this.showMes('success', 'Điều chỉnh thành công');
            this.isLoading = false;
            this.service.updateTarget({status: 3}, this.bdkhTargetId).subscribe(res=>{});
            this.chodieuchinh = false;
            this.loadItems();
        }, error => {
            this.showMes('danger', 'Điều chỉnh thất bại');
            this.isLoading = false;
        })
        
    }
    onLoyalSubmit(value){
        for (let i=0; i<this.items.length; i++){
            if (this.items[i].id == value.id){
                this.newTotalLoyalCus -= this.items[i].targetLoyalCus;
                this.newTotalLoyalCus += parseInt(value.targetLoyalCus);
                this.items[i].targetLoyalCus = value.targetLoyalCus;
                break;
            }
        }
        this.displayLoyalForm = false; 
        // this.service.updateSafffTarget(value).subscribe(res => {
        //     this.loadItems();
        //     this.displayLoyalForm = false; 
        //     this.showMes('success', 'Sửa thành công');
        // }, error => {});
    }
    onOldSubmit(value){
        for (let i=0; i<this.items.length; i++){
            if (this.items[i].id == value.id){
                this.newTotalOldCus -= this.items[i].targetOldCus;
                this.newTotalOldCus += parseInt(value.targetOldCus);
                this.items[i].targetOldCus = value.targetOldCus;
                break;
            }
        }
        this.displayOldForm = false;
        //  this.service.updateSafffTarget(value).subscribe(res => {
        //     this.loadItems();
        //     this.displayOldForm = false;
        //     this.showMes('success', 'Sửa thành công');
        // }, error => {});
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}

}
