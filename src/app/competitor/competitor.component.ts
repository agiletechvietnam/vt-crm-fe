import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {AreaService} from '../shared/services/area.service';
import {StoreService} from '../shared/services/store.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";

@Component({
    selector: 'app-competitor',
    templateUrl: './competitor.component.html',
    styleUrls: ['./competitor.component.css']
})
export class CompetitorComponent implements OnInit {
    private items: any[] = [];
    private mes: Message[];
    
    private displayForm: boolean = false;
    private formCreate: boolean;
	private displayDel: boolean = false;
    private isLoading: boolean = false;

    private delItem;
    private cities: SelectItem[] = [];
    private districts: SelectItem[] = [];
    private wards: SelectItem[] = [];

    private selectedCity;
	private selectedDistrict;

    private user;
    private form: FormGroup;
    private title;
    private payAreaCode;
    constructor(
        private fb: FormBuilder,
        private service: AreaService,
		private storeService: StoreService,
		private cityService: CityService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'competitorName': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'distance': new FormControl('', Validators.compose([Validators.required])),
			'address': new FormControl('', Validators.compose([Validators.required,Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
            'payAreaCode': new FormControl('', Validators.compose([Validators.required]))
		});
        this.loadItems();
    }
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
    loadItems(){
		this.isLoading = true;
		this.service.findCompetitorByShopId(this.user.shopId).subscribe( res => {
			this.items = res;
			this.isLoading = false;
		}, error => {
			this.showMes('error', 'Không thể tải dữ liệu!');
			this.isLoading = false;
		})
	}

    loadCities(){
		this.cities = [];
		this.cities.push({label: "Chọn tỉnh", value: null});
		this.cityService.findCities().subscribe(
			res => {
				for (let i=0; i<res.length; i++){
					this.cities.push({label: res[i].name, value: res[i].payAreaCode});
				}
			},	
			error => {
				error> console.log(error);
			}
		)
	} 

    CityDropDownChange(){
        //if (this.selectedCity == null) return;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
		this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findDistrictsByCityId(this.selectedCity).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.districts.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }

    DistrictDropDownChange(){
        //if (this.selectedDistrict == null) return;
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
		this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findWardsByDistrictId(this.selectedDistrict).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.wards.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }

    
    create(){
		this.displayForm = true;
		this.form.reset();
		this.formCreate = true;
		this.title = "Thêm";
		this.selectedCity = this.user.payAreaCode.substr(0, 4);
		this.selectedDistrict = null;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
		this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
		this.loadCities();
		this.CityDropDownChange();

	}
	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.init(item);
	}
	
	onSubmit(value){
		this.isLoading = true;
		if (this.formCreate){
			value.shopId = this.user.shopId;
			this.service.addCompetitor(value).subscribe(res => {
				this.isLoading = false;
				this.displayForm = false;
				this.showMes('success', 'Thêm thành công');
				this.loadItems();
			}, error => {
				this.isLoading = false;
				this.showMes('error', 'Thêm thất bại');
				this.displayForm = false;
			});
		}else{
			this.service.updateCompetitor(value, value.id).subscribe(res => {
				this.isLoading = false;
				this.displayForm = false;
				this.showMes('success', 'Sửa thành công');
				this.loadItems();
			}, error => {
				this.isLoading = false;
				this.showMes('error', 'Sửa thất bại');
				this.displayForm = false;
			});

		}
	}
	init(res){
       	this.form.controls['id'].setValue(res.id, {onlySelf: true});
		this.form.controls['competitorName'].setValue(res.competitorName, { onlySelf: true });
		this.form.controls['distance'].setValue(res.distance, { onlySelf: true });
		this.form.controls['address'].setValue(res.address, { onlySelf: true });
		this.selectedCity = res.payAreaCode.substr(0, 4);
		this.selectedDistrict = res.payAreaCode.substr(0, 7);
		// this.form.controls['cityId'].setValue(res.payAreaCode.substr(0, 4), {onlySelf: true});
		// this.form.controls['districtId'].setValue(res.payAreaCode.substr(0, 7), {onlySelf: true});
		this.form.controls['payAreaCode'].setValue(res.payAreaCode, {onlySelf: true});
		

		this.cities = [];
		this.cityService.findCities().subscribe(
			res => {
				for (let i=0; i<res.length; i++){
					this.cities.push({label: res[i].name, value: res[i].payAreaCode});
				}
			}
		);

		this.districts = [];
		this.districts.push({label:'Quận/Huyện', value:null});
		this.cityService.findDistrictsByCityId(res.payAreaCode.substr(0, 4)).subscribe(
			res => {
				for (let i=0; i<res.length; i++){
					this.districts.push({label: res[i].name, value: res[i].payAreaCode});
				}
			},
			error =>{
				error> console.log(error);
			}
		)
		
		this.wards = [];
		this.wards.push({label:'Phường/Xã', value:null});
		this.cityService.findWardsByDistrictId(res.payAreaCode.substr(0, 7)).subscribe(
			res => {
				for (let i=0; i<res.length; i++){
					this.wards.push({label: res[i].name, value: res[i].payAreaCode});
				}
			},
			error =>{
				error> console.log(error);
			}
		)
    }

	delete(item, type){
		this.delItem = item;
		this.delItem.typeDel = type;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delCompetitor(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
			},
			error => {}
		);

	}


    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
	 	}, 2500);
	}
}
