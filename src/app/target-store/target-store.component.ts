import { Component, OnInit } from '@angular/core';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {TargetService} from  '../shared/services/target.service';
import {TargetModel} from  '../shared/models/target.model';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
	selector: 'app-target-store',
	templateUrl: './target-store.component.html',
	styleUrls: ['./target-store.component.css']
})
export class TargetStoreComponent implements OnInit {
	private items: any[] = [];
	private mes: Message[];
	private isLoading: boolean =  false;
	private formdc: FormGroup;
	private formkq: FormGroup;
	private displayDc: boolean = false;
	private displayKq: boolean = false;
	private shopId;
	constructor(
		private service: TargetService,
		private fb: FormBuilder
	) { }

	ngOnInit() {
		this.formdc = this.fb.group({
			'id': new FormControl(),
			'targetNumber': new FormControl('', Validators.compose([Validators.required])),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
		this.formkq = this.fb.group({
			'id': new FormControl(),
			'resultNumber': new FormControl('', Validators.compose([Validators.required])),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
		var user = JSON.parse(localStorage.getItem("user"));
		this.shopId = user.shopId;
		this.loadItems();
	}
	loadItems(){
		this.isLoading = true;
		this.service.findShopTargetByShopId(this.shopId).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			},
			error => {
				error => console.log(error);
				this.isLoading = false;
			}
		);
	}
	dieuchinh(item: any){
		this.displayDc = true;
		this.displayKq = false;
		this.formdc.reset();
		this.formdc.controls['targetNumber'].setValue(item.TARGET_NUMBER);
		this.formdc.controls['id'].setValue(item.BDKH_SHOP_TARGET_ID);
		
		
	}
	ketqua(item: any){
		this.formkq.reset()
		this.displayKq = true;
		this.displayDc = false;
		this.formkq.controls['id'].setValue(item.BDKH_SHOP_TARGET_ID);
		
	}
	submitDc(value: any){
		this.isLoading = true;
		value.status = 2;
		this.service.updateShopTarget(value).subscribe(res => {
			this.isLoading = false;
			this.displayDc = false;
			this.showMes('success', 'Yêu cầu điều chỉnh đã được gửi');
			this.loadItems();
		}, error => {
			this.isLoading = false;
			this.displayDc = false;
			this.showMes('error', 'Chưa gửi được yêu cầu');
		})
	}
	submitKq(value: any){
		this.isLoading = true;
		value.status = 3;
		this.service.updateShopTarget(value).subscribe(res => {
			this.isLoading = false;
			this.displayKq = false;
			this.showMes('success', 'Đã gửi kết quả');
			this.loadItems();
		}, error => {
			this.isLoading = false;
			this.displayKq = false;
			this.showMes('error', 'Chưa gửi được kết quả');
		})
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
