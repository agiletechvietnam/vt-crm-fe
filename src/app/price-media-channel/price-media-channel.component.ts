import {Component, OnInit, ViewChild} from "@angular/core";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {ChannelService} from "../shared/services/channel.service";
import { Subscription } from 'rxjs';
import {AreaService} from '../shared/services/area.service';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import { HostConstant } from "../shared/host.constant";
import { FilenameConstant } from "../shared/filename.constant";
import {FileUpload} from 'primeng/primeng';
@Component({
    selector: 'app-price-media-channel',
    templateUrl: './price-media-channel.component.html',
    styleUrls: ['./price-media-channel.component.css']
})
export class PriceMediaChannelComponent implements OnInit {
	@ViewChild('upfile') upfile: FileUpload;

    public subscription: Subscription;
    private items: any[] = [];
    private displayForm : boolean = false;
    private form: FormGroup;
    private title;
    private isLoading :boolean = false;
    private id: number;
    private mes: Message[];
    private formCreate;
    private delItem;
    private displayDel: boolean = false;
	private displayFormUpload: boolean = false;
    private areas: SelectItem[] = [];
	private provinces: SelectItem[] = [];
	private shops: SelectItem[] = [];
	private user;
	private api= HostConstant.API;
	private host = HostConstant.HOST;
    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private service: ChannelService,
        private areaService: AreaService
    ) { }

    ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
        this.form = this.fb.group({
			'id': new FormControl(),
			'shopName': new FormControl(''),
			'price': new FormControl('', Validators.compose([Validators.required])),
		});
		
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
			this.loadItems();
		});
	
    }
    loadItems(){
		this.isLoading = true;
		this.service.channelShopPrices(this.id).subscribe(
			res => {
				this.isLoading = false;
				this.items = res;
			},
			error => {}
		)
	}
    initSelect(e, res){
		for (let i=0; i<res.length; i++)
			this.areas.push({label: res[i].name, value: res[i]});
	}
    loadAreas(){
		this.areaService.findAreas().subscribe( res => {
			this.initSelect(this.areas, res);	
		}, error => {});
	}
	AreaChange(){
		if (this.form.value.selectedAreas.length == 0) {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			return;
		}
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedAreas.length; i++){
			if (i <this.form.value.selectedAreas.length - 1)	
				ids += this.form.value.selectedAreas[i].code + ',';
			else 
				ids += this.form.value.selectedAreas[i].code;
		}

		this.areaService.findCityByCenterCodeIn(ids).subscribe(res => {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			this.provinces = [];
			for (let i=0; i<res.length; i++){
				this.provinces.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	ProvinceChange(){
		if (this.form.value.selectedProvinces.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedProvinces.length; i++){
			if (i <this.form.value.selectedProvinces.length - 1)	
				ids += this.form.value.selectedProvinces[i].payAreaCode+ ',';
			else 
				ids += this.form.value.selectedProvinces[i].payAreaCode;
		}

		this.areaService.findShopByProvinceIn(ids).subscribe(res => {
			this.form.value.selectedShops = [];
			this.shops = [];
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});	
		
	}
    create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
	}
    update(item){
		this.form.reset();
		this.title = "Sửa giá";
		this.formCreate = false;
		this.displayForm = true;
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['shopName'].setValue(item.shopName, { onlySelf: true });
		this.form.controls['price'].setValue(item.price, { onlySelf: true });
	}
    onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			this.service.addShopPrice(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{
			this.service.updateShopPrice(value.id, value ).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
                    this.showMes('warn', 'Sửa thành công');
                    for (let i=0; i<this.items.length; i++)
                        if (this.items[i].id == value.id){
                            this.items[i].price = value.price;
                            break;
                        }
					
				},
				error => {}
			)
		}
	}
    delete(item){
		this.delItem = item; 
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.deleteShopPrice(this.delItem).subscribe(
			res => {
                for (let i=0; i<this.items.length; i++)
                    if (this.items[i].id == this.delItem.id){
                        this.items.splice(i, 1);
                        break;
                    }
                this.isLoading = false;
				this.displayDel = false;
				this.delItem = null;
                this.showMes('warn', 'Xóa thành công');
			},
			error => {}
		);
	}
	onBeforeUpload(event){
        this.isLoading = true;
    }
	onUpload(event) {
		this.displayFormUpload = false;
		this.showMes('success', 'Up load file thành công');
		this.loadItems();
    }
	exportFile(){
		localStorage.setItem('api', this.api+'bdkhmediashopprice/get-by-media-id?mediaId='+this.id);
		window.open(this.host+'/baocao/export_gia_kenh.html');
	}
	onBeforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
	onSelect(event){
        if (event.files[0].name != FilenameConstant.giakenh){  
            this.upfile.clear();
            this.showMes('error', 'File cần có tên là '+ FilenameConstant.giakenh);
        }
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 4000);
	}

    
}
