import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { CampaignModel } from "../shared/models/campaign.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {StoreService} from '../shared/services/store.service';
import {GoodsService} from '../shared/services/goods.service';
import {AreaService} from '../shared/services/area.service';
import {SurveyService} from '../shared/services/survey.service';
import {CustomerService} from "../shared/services/customer.service";
@Component({
	selector: 'app-add-campaign',
	templateUrl: './add-campaign.component.html',
	styleUrls: ['./add-campaign.component.css']
})
export class AddCampaignComponent implements OnInit {
	private items:any[];
    private mes: Message[];
	private form: FormGroup;
	private formsql: FormGroup; 
	private genders: SelectItem[];
	private customers: any[] = []; 
	private isLoading: boolean = false;
	private cshops: any[] = [];
	private id: Number = 0;
	private uploadForm: FormGroup;
	private areas: SelectItem[] = [];
	private provinces: SelectItem[] = [];
	private shops: SelectItem[] = [];
	private chuagui: boolean = false;
	public displayCustomer:boolean = false;
	private dathem: boolean = false;
	private customer: any = {};
	private transactions: any[] = [];
    private warrantys: any[] = [];
	private transaction_details: any[] = [];
    private displayFormTD: boolean = false;
	// private slectedAreas: any[] = [];
	// private slectedProvinces: any[] = [];
	// private slectedShops: any[] = [];

	private domains: SelectItem[] = [];
	private goodsGroups: SelectItem[] = [];
	private goods: SelectItem[] = [];

	private all = false;
	private surveys: SelectItem[] = [];
	// private slectedDomains: any[] = [];
	// private slectedGoodsGroups: any[] = [];
	// private slectedGoods: any[] = [];
	private darun:boolean = false;
	private tieuchi: any[] = [];
	private sokhach = 0;
	private user;
	private goodsTable: any[] = [];
	private dataToRender: any[] = [];
	constructor(
        private service: CampaignService,
		private storeService: StoreService,
		private group: StoreService,
		private goodsService: GoodsService,
        private fb: FormBuilder,
		
		private surveyService: SurveyService,
		private areaService: AreaService,
		private customerService: CustomerService
	) { }
	ngOnInit() {
		this.genders = [];
		this.genders.push({label:'Tất cả', value: null}); 
		this.genders.push({label:'Nam', value: 1});
		this.genders.push({label:'Nữ', value: 0});



		this.user = JSON.parse(localStorage.getItem('user'));
		this.uploadForm = this.fb.group({
			'file': new FormControl(),
		});
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(254), this.NoWhitespaceValidator])),
			'code': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(254), this.NoWhitespaceValidator])),
			'gender': new FormControl(),
			'fromTransDate': new FormControl('', Validators.compose([ Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'toTransDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])), 
			'fromAge': new FormControl(),
			'toAge': new FormControl(),
			'fromTime': new FormControl(),
			'toTime': new FormControl(),
			'fromAmount': new FormControl(),
			'toAmount': new FormControl(),
			'surveyId': new FormControl(),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
			'shops': new FormControl(),
			'selectedAreas': new FormControl(),
			'selectedProvinces': new FormControl(),
			'selectedShops': new FormControl(), 
			'selectedDomains': new FormControl(),
			'selectedGoodsGroups': new FormControl(),
			// 'selectedGoods': new FormControl(),
		});

		this.formsql = this.fb.group({
			'sqlSelectData': new FormControl('', Validators.required),
		});
		
		this.tieuchi.push({label: "Chọn theo các tiêu chí", value: 1}, {label: "Chọn từ file", value: 2});
		this.loadAreas();
		this.loadProductType();
		this.loadSurvey();
		
	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	
	vDate(d){
		let date = new Date(d);
		return date;
	}
	loadSurvey(){
		this.isLoading = true;

		this.surveyService.findSurveys().subscribe(res => {
			this.surveys.push({label: "Chọn survey", value: null});
			for (let i=0; i<res.length; i++){
				this.surveys.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error => {});
	}
	//load ngành hàng
	loadProductType(){
		this.isLoading = true;
		this.goodsService.findProductType().subscribe(res => {
			this.isLoading = false;
			for (let i=0; i<res.length; i++){
				this.domains.push({label: res[i].name, value: res[i]});
			}
		}, error=> {

		});
	}

	//change product type thay doi nhom group
	ProductTypeChange(){
		if (this.form.value.selectedDomains.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedDomains.length; i++){
			if (i <this.form.value.selectedDomains.length - 1)	
				ids += this.form.value.selectedDomains[i].code + ',';
			else 
				ids += this.form.value.selectedDomains[i].code;
		}
		
		this.goodsService.findGoodsgroupByProductType(ids).subscribe(res => {
			this.form.value.selectedGoodsGroups = [];
			// this.form.value.selectedGoods = [];
			this.goodsGroups = [];
			for (let i=0; i<res.length; i++){
				this.goodsGroups.push({label: res[i].name, value: res[i]});
			}
			this.all = false;
			this.isLoading = false;
		}, error=> {

		});
	}

	// change goodsGroups 
	GoodGroupChange(){
		if (this.form.value.selectedGoodsGroups.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedGoodsGroups.length; i++){
			if (i <this.form.value.selectedGoodsGroups.length - 1)	
				ids += this.form.value.selectedGoodsGroups[i].id + ',';
			else 
				ids += this.form.value.selectedGoodsGroups[i].id;
		}

		this.goodsService.findGoodByGroupId(ids).subscribe(res => {
			this.goodsTable = [];
			// this.form.value.selectedGoods = [];
			this.goods = [];
			for (let i=0; i<res.length; i++){
				this.goods.push({label: res[i].name, value: res[i]});
				this.goodsTable.push(res[i]);
				this.goodsTable[i].selected = false;
			}
			this.all = false;
			this.isLoading = false;
		}, error=> {

		});
	}
	private sgoods:any = [];
	GoodChange(){
		// console.log(this.form.value.selectedGoods);
		// if (this.sgoods.length == 0) {this.sgoods = this.form.value.selectedGoods; return; }
		// console.log(this.form.value.selectedGoods);
		// let t = this.sgoods;
		// console.log(this.sgoods);
		// for (let i=0; i<this.form.value.selectedGoods.length; i++){
		// 	let j = 0;
		// 	while(j<t.length && t[j].id != this.form.value.selectedGoods[i].id){
		// 		j++;
		// 	}
		// 	if (j < t.length){
		// 		this.sgoods.push(this.form.value.selectedGoods[i]);
		// 	}
		// }
		// console.log(this.sgoods);
	}
	initSelect(e, res){
		for (let i=0; i<res.length; i++)
			this.areas.push({label: res[i].name, value: res[i]});
	}

	//load địa chỉ
	loadAreas(){
		this.areaService.findAreas().subscribe( res => {
			this.initSelect(this.areas, res);	
		}, error => {});
	}

	AreaChange(){
		if (this.form.value.selectedAreas.length == 0) {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			return;
		}
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedAreas.length; i++){
			if (i <this.form.value.selectedAreas.length - 1)	
				ids += this.form.value.selectedAreas[i].code + ',';
			else 
				ids += this.form.value.selectedAreas[i].code;
		} 

		this.areaService.findCityByCenterCodeIn(ids).subscribe(res => {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			this.provinces = [];
			for (let i=0; i<res.length; i++){
				this.provinces.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	
	ProvinceChange(){
		if (this.form.value.selectedProvinces.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedProvinces.length; i++){
			if (i <this.form.value.selectedProvinces.length - 1)	
				ids += this.form.value.selectedProvinces[i].payAreaCode+ ',';
			else 
				ids += this.form.value.selectedProvinces[i].payAreaCode;
		}

		this.areaService.findShopByProvinceIn(ids).subscribe(res => {
			this.form.value.selectedShops = [];
			this.shops = [];
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].shopCode +' - '+res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});	
		
	}
	loadCustomers(){
		this.isLoading = true;
		this.service.findCustomersByCampaignId(this.id).subscribe(
			res => {
				this.isLoading = false;
				this.customers = res;
			},
			error => {}
		)
	}
	
	filltered(event){
		this.dataToRender = event.filteredValue;
		this.all = false;
	}

	onSubmit(value:any){

		var selectedGoods = [];
		for (let i in this.goodsTable)
			if (this.goodsTable[i].selected)
				selectedGoods.push(this.goodsTable[i].id);
	
		if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
			 return;
		}
		if (this.form.controls['endDate'].value.toDateString() == this.form.controls['startDate'].value.toDateString()){
			this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc ít nhất 1 ngày');
			 return;
		}
		if (this.form.controls['fromTransDate'].value > this.form.controls['toTransDate'].value){
			 this.showMes('error', 'Ngày bắt đầu mua hàng phải nhỏ hơn ngày kết thúc');
			 return;
		}

		if (value.selectedShops == null || value.selectedShops.length == 0){
			this.showMes('error', 'Bạn cần chọn siêu thị');
			return;
		}
		if (selectedGoods.length > 100){
			this.showMes('error', 'Chỉ được chọn tối đa 100 mặt hàng');
			return;
		}
		if ((new Date()).toDateString() != this.form.controls['startDate'].value.toDateString() && this.form.controls['startDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải lớn hơn hoặc bằng ngày hiện tại');
			 return;
		}
		this.isLoading = true;
		value.goods = selectedGoods;
		value.shops = value.selectedShops;
		if (value.surveyId != null){
			value.surveyName = value.surveyId.name;
			value.surveyId = value.surveyId.id;
		}
		if (value.gender == null) delete value.gender;
		value.createdId = this.user.id;
		this.service.addCampaign(value).subscribe(
			res => {
				this.isLoading = false;
				this.id = res.id;
				this.dathem = true;
				this.formsql.controls['sqlSelectData'].setValue(res.sqlSelectData, {onlySelf: true});
				this.showMes('success', 'Thêm thành công');
			},
			error => {}
		);

	}
	onSubmitSql(){ 
		this.isLoading = true;
		this.service.runSql(this.id).subscribe(res => {
			this.sokhach = res;
			this.isLoading = false;
			this.chuagui = true;
			this.darun = true;
		}, error => {

		})
	}
	guiDuyet(){
		this.isLoading = true;
		this.service.updateCampaign({status: 2}, this.id).subscribe(
			res => {
				this.isLoading = false;
				this.showMes('success','Gửi duyệt thành công');
				this.chuagui = false;
			},
			error => {}
		)
	}
	sqlSelectData
	shopChange(){
		let t = this.form.controls['shops'].value;
		this.items = [];
		for (let i=0; i<t.length; i++){
			this.items.push({name: t[i].name, id: t[i].id});
		}
	}
	delete(item){
		let t = this.form.controls['shops'].value;
		for (let i=0; i<t.length; i++){
			if (t[i].id == item.id){
				t.splice(i, 1);
				this.items.splice(i, 1);
				return;
			}
				
		}
	}
	onUpload(event) {
		console.log(event);
    }
	loadCustomer(phone){
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
	viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }
    view(id){
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[1]);
        c.style.width = "700px";
		c.style.height = "500px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
	checkAll(){
		if (this.dataToRender.length > 0){
			for (let i in this.dataToRender)
				for (let j in this.goodsTable)
					if (this.goodsTable[j] == this.dataToRender[i])
						this.goodsTable[j].selected = this.all;
			return;
		}
		let that = this;
		this.goodsTable.map(function(item){
			return item.selected = that.all;
		})
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
	

}
