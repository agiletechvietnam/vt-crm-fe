import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from "../shared/services/survey.service";
import { CampaignService } from "../shared/services/campaign.service";
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import {CustomerService} from "../shared/services/customer.service";
@Component({
    selector: 'app-up-survey',
    templateUrl: './up-survey.component.html',
    styleUrls: ['./up-survey.component.css']
})
export class UpSurveyComponent implements OnInit {
    private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    private surveyId;
    private survey: any = {};
    private campaignCustomerId;
    private xong: boolean = false;
    private displayCustomer: boolean = false;
    private customer: any = {};
    private transactions: any[] = [];
    private warrantys: any[] = [];
    private hinhthucs: any[] =[];
    private careMethodId;
    private customerId;
    private customerPhone;
    private displayFormTD: boolean = false;
    private transaction_details: any [] = [];
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: SurveyService,
        private campaignService: CampaignService,
        private customerService: CustomerService
    ) { }

    ngOnInit() {
        
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.surveyId = params['surveyId'];
            this.customerPhone = params['phone'];
            this.campaignCustomerId = params['cusid'];
            // = params['customerId'];
            this.loadItems();
            this.viewCustomer();
        });
        this.hinhthucs = [
			{label: "Chọn hình thức", value: null},
			{label: "Gọi điện", value: 1},
			{label: "Nhắn tin", value: 2},
			{label: "Trực tiếp", value: 3},
			{label: "Qua facebook", value: 4},
			{label: "Khác", value: 5}
		];
    }
    loadCustomer(customerPhone){
		this.customerService.findCustomerByPhone(customerPhone).subscribe(
            res => {
				this.customer = res[0];
                let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
                
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    viewCustomer(){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "700px";
		//this.displayCustomer = true;
		this.loadCustomer(this.customerPhone);
	}
    view(id){
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[1]);
        c.style.width = "700px";
		c.style.height = "700px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }

    loadItems() {
        this.isLoading = true;
        this.service.fillSurvey(this.surveyId).subscribe(
            res => {
                this.survey = res;
                for (let i=0; i<this.survey.questions.length; i++){
                    this.survey.questions[i].realAnswers = [];
                }
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    Done(){
        console.log(this.survey);
        if (this.careMethodId == null) {
            this.showMes('error', 'Bạn cần nhập hình thức');
            return;
        }
        for (let i = 0; i < this.survey.questions.length; i++){
            if (this.survey.questions[i].realAnswers.length == null || this.survey.questions[i].realAnswers.length == 0){
                this.showMes('error', 'Bạn cần điền hết nội dung câu trả lời');
                return;
            }
        }

        
        var data = {
            id: this.campaignCustomerId,
            surveyResponse: []
        };
        for (let i=0; i<this.survey.questions.length; i++){
            let q = this.survey.questions[i];
            let d = {
                questionId: q.id,
                responseText: '',
                responseIds: '',
                questionType: q.questionType,
                note: ''
            }
            if (q.questionType == 1){
                d.responseText = q.realAnswers;
            }
            if (q.questionType == 2){
                d.responseIds = q.realAnswers;
                d.note = q.responseText;
                
                    
            }
            if (q.questionType == 3){
                for (let j=0; j<q.realAnswers.length; j++)
                    if (j < q.realAnswers.length - 1)
                        d.responseIds += q.realAnswers[j] + ',';
                    else 
                        d.responseIds += q.realAnswers[j];
                d.note = q.responseText;
            }
            data.surveyResponse.push(d);
            
        }

        this.isLoading = true;
        this.service.upSurvey(data).subscribe(res=> {
            
            
            this.campaignService.updateCampaignCustomer({careStatus: 6, careResult: "1", careContent: "1", cusCareMethodId: this.careMethodId}, this.campaignCustomerId).subscribe(res => {
                this.isLoading = false;
                this.xong = true;
                this.showMes('success', 'Thêm thành công');
            }, error => {});

        }, error => {})
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}