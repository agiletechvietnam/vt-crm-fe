/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UpSurveyComponent } from './up-survey.component';

describe('UpSurveyComponent', () => {
    let component: UpSurveyComponent;
    let fixture: ComponentFixture<UpSurveyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UpSurveyComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UpSurveyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
