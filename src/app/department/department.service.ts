import {Injectable} from "@angular/core";
import {HttpService} from "../shared/http.service";
import {Observable} from "rxjs";
import {DepartmentModel} from "./department.model";
import {APIConstant} from "../shared/api.constant";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class DepartmentService {

    constructor(private http: HttpService) {
        
    }
    
    findDepartments(): Observable<DepartmentModel[]> {
        return this.http.get(APIConstant.FIND_DEPARTMENTS)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    delDepartment(c: DepartmentModel){
        return this.http.delete(APIConstant.DELETE_DEPARTMENT+c.id,c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    addDepartment(c: DepartmentModel){
        return this.http.post(APIConstant.ADD_DEPARTMENT, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
    updateDepartment(c: DepartmentModel, id){
        return this.http.post(APIConstant.UPDATE_DEPARTMENT+id, c)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findDepartmentById(id): Observable<DepartmentModel>{
        return this.http.get(APIConstant.FIND_DEPARTMENT_BY_ID+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
    
}