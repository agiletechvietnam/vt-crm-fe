import { Component, OnInit } from '@angular/core';
import {DepartmentService} from './department.service';
import {DepartmentModel} from "./department.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
@Component({
    selector: 'app-department',
    templateUrl: './department.component.html',
    styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
    private items: DepartmentModel[];
    private mes: Message[];

	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;

    constructor(
        private service: DepartmentService,
        private fb: FormBuilder) { }
    
    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
		});
		this.loadItems();
        
    }
    loadItems(){
		this.service.findDepartments().subscribe(
            res => {
                this.items = res;
            },
            error => {
                error => console.log(error);
            }
        );
	}

    create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;

		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		
		

	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.service.delDepartment(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.mes = [];
				this.mes.push({severity:'warn', summary:'Xóa thành công!', detail:''});
				let t = this;
				setTimeout(function(){
					t.mes = [];
				}, 1500);
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		if (this.formCreate){
			this.service.addDepartment(value).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.mes = [];
					this.mes.push({severity:'success', summary:'Thêm thành công!', detail:''});
					let t = this;
					setTimeout(function(){
						t.mes = [];
					}, 1500);
				},
				error => {}
			)
		}else{
			this.service.updateDepartment(value, value.id).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.mes = [];
					this.mes.push({severity:'success', summary:'Sửa thành công!', detail:''});
					let t = this;
					setTimeout(function(){
							t.mes = [];
						}, 1500);
					},
				error => {}
			)
		}
	}

}
