import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {AreaService} from '../shared/services/area.service';
import {StoreService} from '../shared/services/store.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";
import {StaffService} from "../shared/services/staff.service";

@Component({
	selector: 'app-area-manager',
	templateUrl: './area-manager.component.html',
	styleUrls: ['./area-manager.component.css']
})
export class AreaManagerComponent implements OnInit {
	private items: any[] = [];
	private areas: any[] = [];
	private competitor: any[] = [];
	private listCompany: any[] = [];
	private listDv: any[] = [];
	private listMarket: any[] =[];
	private listSchool: any[] =[];
	private displayForm: boolean = false;
	private loais: SelectItem[] = [];
	private staffs: SelectItem[] = [];
	private mes: Message[];
	private displayFormDoithu: boolean = false;
	private displayFormCompany: boolean = false;
	private displayFormMarket: boolean = false;
	private displayFormDv: boolean = false;
	private displayFormSchool: boolean = false;

	private formDoithu: FormGroup;
	private formCompany: FormGroup;
	private formMarket: FormGroup;
	private formDv: FormGroup;
	private formSchool: FormGroup;
	private user;
	private isLoading: boolean =  false;
	private payAreaCode;
	private title;
	private formCreate: boolean;
	private delItem;
	private displayDel: boolean = false;
	private form: FormGroup;
	private cities: SelectItem[] = [];
    private districts: SelectItem[] = [];
    private wards: SelectItem[] = [];

    private wardsdata;
	private staffdata;
    private selectedCity;
	private selectedDistrict;
	constructor(
		private fb: FormBuilder,
		private service: AreaService,
		private storeService: StoreService,
		private cityService: CityService,
        private staffService: StaffService

	) { }
	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'staffId': new FormControl('', Validators.required),
			'shopFriendly': new FormControl('', Validators.required),
			'distance': new FormControl('', Validators.required),
            'payAreaCode': new FormControl('', Validators.required)
		});

        this.districts.push({ label: 'Quận/Huyện', value: null });
        this.wards.push({ label: 'Phường/Xã', value: null });

		this.staffs.push({label: 'Nhân viên', value: null});
		
		this.loais.push({label:'Loại thị trường', value:null});
		this.loais.push({label: 'Trọng điểm', value: 1});
		this.loais.push({label: 'Tiềm năng', value: 2});

		this.loadItems();
		
	}
	loadCities() {
        this.cities = [];
        this.cities.push({ label: "Tỉnh", value: null });
        this.cityService.findCities().subscribe(
            res => {
                for (let i = 0; i < res.length; i++) {
                    this.cities.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }
	loadStaffs(){

   
        this.staffService.findStaffsByShopId2(this.user.shopId).subscribe(
            res => {
                this.staffdata = res;
				for (let i=0; i<res.length; i++){
					this.staffs.push({label: res[i].name, value: res[i].id});
				}
            },
            error => {
            }
        );
    
	}
	CityDropDownChange() {
        if (this.selectedCity == null) return;
        this.districts = [];
        this.districts.push({ label: 'Quận/Huyện', value: null });
        this.wards = [];
        this.wards.push({ label: 'Phường/Xã', value: null });
        this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findDistrictsByCityId(this.selectedCity).subscribe(
            res => {
                for (let i = 0; i < res.length; i++) {
                    this.districts.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }

    DistrictDropDownChange() {
        if (this.selectedDistrict == null) return;
        this.wards = [];
        this.wards.push({ label: 'Phường/Xã', value: null });
        this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findWardsByDistrictId(this.selectedDistrict).subscribe(
            res => {
                this.wardsdata = res;
                for (let i = 0; i < res.length; i++) {
                    this.wards.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }

	loadItems(){
		this.isLoading = true;
		this.service.findAreasByShopId(this.user.shopId).subscribe( res => {
			this.areas = res;
			this.isLoading = false;
		}, error => {
			this.showMes('error', 'Không thể tải dữ liệu!');
            this.isLoading = false;
		})
	}

	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
	 	}, 2500);
	}
	create(){
		this.displayForm = true;
		this.form.reset();
		this.formCreate = true;
		this.title = "Thêm";
		this.selectedCity = this.user.payAreaCode.substr(0, 4);
		this.selectedDistrict = null;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
		this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        
		this.loadCities();
        this.CityDropDownChange();
        this.loadStaffs();

	}
    update(item) {

    
        this.form.reset();
        this.title = "Sửa";
        this.formCreate = false;
        this.displayForm = true;
        this.isLoading = true;


        this.form.controls['id'].setValue(item.id, { onlySelf: true });
        this.form.controls['staffId'].setValue(item.staffId, { onlySelf: true });
        this.form.controls['shopFriendly'].setValue(item.shopFriendly, { onlySelf: true });
        this.form.controls['distance'].setValue(item.distance, { onlySelf: true });
        this.form.controls['payAreaCode'].setValue(item.payAreaCode, { onlySelf: true });
        
        this.selectedCity = item.payAreaCode.substr(0, 4);
		this.selectedDistrict = item.payAreaCode.substr(0, 7);
        this.loadCities();
        this.loadStaffs();

        this.districts = [];
        this.districts.push({ label: 'Quận/Huyện', value: null });
        if (item.payAreaCode != null) {
            this.selectedCity = item.payAreaCode.substr(0, 4);
            this.selectedDistrict = item.payAreaCode.substr(0, 7);

            this.cityService.findDistrictsByCityId(item.payAreaCode.substr(0, 4)).subscribe(
                res => {
                    for (let i = 0; i < res.length; i++) {
                        this.districts.push({ label: res[i].name, value: res[i].payAreaCode });
                    }
                },
                error => {
                    error > console.log(error);
                }
            )

            this.wards = [];
            this.wards.push({ label: 'Phường/Xã', value: null });
            this.cityService.findWardsByDistrictId(item.payAreaCode.substr(0, 7)).subscribe(
                res => {
                    for (let i = 0; i < res.length; i++) {
                        this.wards.push({ label: res[i].name, value: res[i].payAreaCode });
                    }

                },
                error => {
                    error > console.log(error);
                }
            );
        }
        this.isLoading = false;
    }

    onSubmit(value) {
        this.isLoading = true;
        if (this.formCreate) {
            // value.type = 4; //schoool
            value.shopId = this.user.shopId;
            

            for (let i=0; i<this.staffdata.length; i++)
                if (this.staffdata[i].id == value.staffId){
                    value.staffName = this.staffdata[i].name;
                    value.staffCode = this.staffdata[i].staffCode;
                    break;
                }

            for (let i=0; i<this.wardsdata.length; i++)
                if (this.wardsdata[i].payAreaCode == value.payAreaCode){
                    value.address = this.wardsdata[i].fullName;
                    break;
                }

            
            value.shopId = this.user.shopId;
            value.shopCode = this.user.shopCode;
            
            this.service.addAreaShop(value).subscribe(res => {
                this.isLoading = false;
                this.displayForm = false;
                this.showMes('success', 'Thêm thành công');
                this.loadItems();
            }, error => {
                this.isLoading = false;
                this.showMes('error', 'Thêm thất bại');
                this.displayForm = false;
            });
        } else {
             for (let i=0; i<this.staffdata.length; i++)
                if (this.staffdata[i].id == value.staffId){
                    value.staffName = this.staffdata[i].name;
                    value.staffCode = this.staffdata[i].staffCode;
                    break;
                }
            this.service.updateAreaShop(value, value.id).subscribe(res => {
                this.isLoading = false;
                this.displayForm = false;
                this.showMes('success', 'Sửa thành công');
                this.loadItems();
            }, error => {
                this.isLoading = false;
                this.showMes('error', 'Sửa thất bại');
                this.displayForm = false;
            });
        }
    }
	delete(item) {
        this.delItem = item;
        this.displayDel = true;
    }
    acceptDel() {
        this.isLoading = true;
        this.service.delAreaShop(this.delItem).subscribe(
            res => {
                this.isLoading = false;
                this.delItem = null;
                this.displayDel = false;
                this.loadItems();
                this.showMes('warn', 'Xóa thành công');
            },
            error => { }
        );

    }
}
 