import { Component, Input, OnInit, EventEmitter, ViewChild, trigger, state, transition, style, animate } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';
import { HostConstant } from "./shared/host.constant";

@Component({
    selector: 'app-menu',
    template: `
        <div class="menu">
            <ul app-submenu [item]="model" root="true"></ul>
        </div>
    `
})
export class AppMenuComponent implements OnInit {
    model: MenuItem[];
    private user = null;
    constructor(

    ) {
        try {
            this.user = JSON.parse(localStorage.getItem("user"));
        } catch (e) { }
        if (this.user == null) {
            window.location.href = HostConstant.HOST + '/login.html';
            return;
        }
    }
    ngOnInit() {
        
        //localhost
        if (this.user.staffRole == 'TTT') {

            this.model = [
                { label: 'Dashboard', icon: 'fa-bell', routerLink: ['/'] },
                { label: 'Quản lý khách hàng', icon: 'fa-users', routerLink: ['/customer'] },
                {
                    label: 'Bản đồ khách hàng', icon: 'fa-map', routerLink: ['/target-tt'],
                    items: [
                        { label: 'Quản lý chỉ tiêu', icon: 'fa-line-chart', routerLink: ['/target-tt'] },
                        { label: 'Yêu cầu truyền thông', icon: 'fa-inbox', routerLink: ['/request-media-campaign'] },
                        { label: 'Chương trình truyền thông', icon: 'fa-bullhorn', routerLink: ['/communication-program'] },
                        { label: 'Kênh truyền thông', icon: 'fa-television', routerLink: ['/media-channel'] },

                    ]
                },
                {
                    label: 'Chăm sóc khách hàng', icon: 'fa-life-saver', routerLink: ['/campaign'],
                    items: [
                        { label: 'Quản lý chiến dịch', icon: 'fa-rocket', routerLink: ['/campaign'] },
                        { label: 'Chiến dịch phòng ban', icon: 'fa-rocket', routerLink: ['/campaign-store'] },
                        { label: 'Survey', icon: 'fa-object-group', routerLink: ['/survey'] },
                        { label: 'Câu hỏi', icon: 'fa-question-circle', routerLink: ['/question'] }

                    ]
                },
                {
                    label: 'Master data', icon: 'fa-database', routerLink: ['/store'],
                    items: [
                        { label: 'Quản lý nhân viên', icon: 'fa-square-o', routerLink: ['/staff-store'] },
                        {
                            label: 'Quản lý siêu thị', icon: 'fa-home', routerLink: ['/store'],

                        },
                        { label: 'Nhân viên toàn hệ thống', icon: 'fa-columns', routerLink: ['/staff'] },
                        // { label: 'Quản lý phòng ban, chức vụ', icon: 'fa-columns', routerLink: ['/department'] },
                        { label: 'Quản lý cư dân khu vực', icon: 'fa-columns', routerLink: ['/population'] },
                        // { label: 'Thành phố', icon: 'fa-square-o', routerLink: ['/city'] },

                    ]
                }
            ];
        } else
        if (this.user.staffRole == 'NVTT') {
            this.model = [
                { label: 'Dashboard', icon: 'fa-bell', routerLink: ['/'] },
                { label: 'Quản lý khách hàng', icon: 'fa-users', routerLink: ['/customer'] },
                {
                    label: 'Bản đồ khách hàng', icon: 'fa-map', routerLink: ['/target'],
                    items: [
                        { label: 'Quản lý chỉ tiêu', icon: 'fa-line-chart', routerLink: ['/target'] },
                        { label: 'Yêu cầu truyền thông', icon: 'fa-inbox', routerLink: ['/request-media-campaign'] },
                        { label: 'Chương trình truyền thông', icon: 'fa-bullhorn', routerLink: ['/communication-program'] },

                        { label: 'Kênh truyền thông', icon: 'fa-television', routerLink: ['/media-channel'] },

                    ]
                },
                {
                    label: 'Chăm sóc khách hàng', icon: 'fa-life-saver', routerLink: ['/campaign-center'],
                    items: [
                        { label: 'Quản lý chiến dịch', icon: 'fa-rocket', routerLink: ['/campaign-center'] },
                        { label: 'Chiến dịch của tôi', icon: 'fa-rocket', routerLink: ['/campaign-staff'] },
                        { label: 'Survey', icon: 'fa-object-group', routerLink: ['/survey'] },
                        { label: 'Câu hỏi', icon: 'fa-question-circle', routerLink: ['/question'] }
                    ]
                },
                {
                    label: 'Master data', icon: 'fa-database', routerLink: ['/store'],

                    items: [
                        {
                            label: 'Quản lý siêu thị', icon: 'fa-home', routerLink: ['/store'],
                        },
                        { label: 'Nhân viên toàn hệ thống', icon: 'fa-columns', routerLink: ['/staff'] },
                        { label: 'Quản lý cư dân khu vực', icon: 'fa-columns', routerLink: ['/population'] },

                    ]
                }
            ];
        } else
        if (this.user.staffRole == 'TST') {
            //sieu thi
            this.model = [


                { label: 'Quản lý khách hàng', icon: 'fa-users', routerLink: ['/customer-store'] },

                {
                    label: 'Bản đồ khách hàng', icon: 'fa-map', routerLink: ['/target-store'],
                    items: [
                        { label: 'Quản lý chỉ tiêu', icon: 'fa-line-chart', routerLink: ['/target-store'] },
                        // { label: 'Chương trình truyền thông', icon: 'fa-bullhorn', routerLink: ['/program-store'] },
                        { label: 'Chương trình truyền thông', icon: 'fa-bullhorn', routerLink: ['/program-store-new'] },
                    ]
                },

                {
                    label: 'Chăm sóc khách hàng', icon: 'fa-life-saver', routerLink: ['/campaign-store'],
                    items: [
                        { label: 'Quản lý chiến dịch', icon: 'fa-rocket', routerLink: ['/campaign-store'] },
                    ]
                },
                {
                    label: 'Master Data', icon: 'fa-database', routerLink: ['/staff-store'],
                    items: [
                        { label: 'Quản lý nhân viên', icon: 'fa-square-o', routerLink: ['/staff-store'] },
                        {
                            label: 'Thông tin địa bàn', icon: 'fa-square-o', routerLink: ['/area-manager'],
                            items: [
                                { label: 'Khai báo địa bàn', icon: 'fa-square-o', routerLink: ['/area-manager'] },
                                { label: 'Đối thủ cạnh tranh', icon: 'fa-square-o', routerLink: ['/competitor'] },
                                {
                                    label: 'Cơ quan xí nghiệp', icon: 'fa-square-o', routerLink: ['/company'],
                                    items: [
                                        { label: 'KCN/công ty', icon: 'fa-square-o', routerLink: ['/company'] },
                                        { label: 'TTTM/Chợ', icon: 'fa-square-o', routerLink: ['/market'] },
                                        { label: 'Đơn vị hành chính', icon: 'fa-square-o', routerLink: ['/administrative'] },
                                        { label: 'Trường học', icon: 'fa-square-o', routerLink: ['/school'] },
                                    ]
                                },
                            ]
                        },
                    ]
                }];
        } else
        if (this.user.staffRole == 'NVST') {
            // Nhân viênh
            this.model = [
                { label: 'Thông báo', icon: 'fa-bell', routerLink: ['/staff-notification'] },
                {
                    label: 'Bản đồ khách hàng', icon: 'fa-map', routerLink: ['/target-staff'],
                    items: [
                        { label: 'Danh sách chỉ tiêu', icon: 'fa-line-chart', routerLink: ['/target-staff'] },
                        { label: 'Chương trình truyền thông', icon: 'fa-bullhorn', routerLink: ['/staff-program'] },
                    ]
                },
                {
                    label: 'Chăm sóc khách hàng', icon: 'fa-life-saver', routerLink: ['/campaign-staff'],
                    items: [
                        { label: 'Danh sách chiến dịch', icon: 'fa-rocket', routerLink: ['/campaign-staff'] },
                    ]
                }
            ];
        }
        else {
            window.location.href = HostConstant.HOST + '/login.html';
        }

    }
}

@Component({
    selector: '[app-submenu]',
    template: `
        <ul>
            <template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
                <li [ngClass]="{'active-menuitem': isActive(i)}">
                    <a [href]="child.url||'#'" (click)="itemClick($event,child,i)">
                        <i class="fa fa-fw" [ngClass]="child.icon"></i>
                        <span>{{child.label}}</span>
                        <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                    </a>
                    <ul app-submenu [item]="child" *ngIf="child.items" [@children]="isActive(i) ? 'visible' : 'hidden'" ></ul>
                </li>
            </template>
        </ul>
    `,
    animations: [
        trigger('children', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenu {

    @Input() item: MenuItem;

    @Input() root: boolean;

    activeIndex: number;

    constructor(public router: Router, public location: Location) { }

    itemClick(event: Event, item: MenuItem, index: number) {
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        this.activeIndex = (this.activeIndex === index) ? null : index;

        if (!item.url || item.routerLink) {
            event.preventDefault();
        }

        if (item.command) {
            if (!item.eventEmitter) {
                item.eventEmitter = new EventEmitter();
                item.eventEmitter.subscribe(item.command);
            }

            item.eventEmitter.emit({
                originalEvent: event,
                item: item
            });
        }

        if (item.routerLink) {
            this.router.navigate(item.routerLink);
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }
}