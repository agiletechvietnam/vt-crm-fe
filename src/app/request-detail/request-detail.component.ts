import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {MediaCampaignService} from '../shared/services/mediacampaign.service';
import {RequestModel} from '../shared/models/request.model';
@Component({
    selector: 'app-request-detail',
    templateUrl: './request-detail.component.html',
    styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit {
    private items: any[] = [];
    public subscription: Subscription;
    private requestId;
    private isLoading: boolean =  false;
    private request;
    private mes: Message[];
    constructor(
        private activatedRoute: ActivatedRoute,
        private service: MediaCampaignService
    ) { }

    ngOnInit() {
        this.request = {};
        this.request.content = '';
        this.request.mediaId = '';
        this.request.requestDate = '';
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.requestId = params['id'];
            this.loadItems();
        });
    }
    loadItems(){
        this.isLoading = true;
		this.service.requestCampaignDetail(this.requestId).subscribe(res => {
			this.request.content = res.content;
            this.request.mediaChannels = res.mediaChannels;
            this.request.createDate = res.createDate;
            this.items = res.shops;
            console.log(this.request);
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
			this.showMes("error", "Không thể tải dữ liệu");
		});

    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
