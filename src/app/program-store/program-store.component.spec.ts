/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ProgramStoreComponent } from './program-store.component';

describe('ProgramStoreComponent', () => {
    let component: ProgramStoreComponent;
    let fixture: ComponentFixture<ProgramStoreComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProgramStoreComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgramStoreComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
