import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {ChannelService} from  '../shared/services/channel.service';
import {AreaService} from '../shared/services/area.service';
import {MediaCampaignService} from '../shared/services/mediacampaign.service';
import {Router} from '@angular/router';
@Component({
    selector: 'app-request-media-campaign',
    templateUrl: './request-media-campaign.component.html',
    styleUrls: ['./request-media-campaign.component.css']
})
export class RequestMediaCampaignComponent implements OnInit {
    private items: any[] = [];
    private mediaIds: SelectItem[] = [];
	private mes: Message[];
	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
	private user;

    private areas: SelectItem[] = [];
	private provinces: SelectItem[] = [];
	private shops: SelectItem[] = []; 


    constructor( 
        private fb: FormBuilder,
        private channel: ChannelService,
		private service: MediaCampaignService,
        private areaService: AreaService,
		private router: Router
    ) {}

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'startDate': new FormControl('', Validators.compose([Validators.required])),
            'endDate': new FormControl('', Validators.compose([Validators.required])),
			'content': new FormControl('', Validators.compose([Validators.required,Validators.minLength(5), Validators.maxLength(1000), this.NoWhitespaceValidator])),
			'expectProfit': new FormControl('', Validators.compose([Validators.required]))
		});
		this.user = JSON.parse(localStorage.getItem('user'));
		this.loadItems();
        
    }
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
    loadItems(){

		this.isLoading = true;
		this.service.findRequestCampaign().subscribe(res => {
			this.items = res;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
			this.showMes("error", "Không thể tải dữ liệu");
		});
        // this.items.push({content: "Tạo chương trình truyền thông cho cho mùa hè", mediaId: "Phát tờ rơi", created: "02/10/2017"});
		
	}
    loadMeidaIds(){
        
        if (this.mediaIds.length != 0) return;
        this.isLoading = true;
        this.channel.findChannels().subscribe(res => {

            for (let i=0; i<res.length; i++){
                this.mediaIds.push({label: res[i].name, value: res[i].id});
            }
        }, error => {});
        
        this.isLoading = false;
    }
    //select sieu this
    initSelect(e, res){
		for (let i=0; i<res.length; i++)
			e.push({label: res[i].name, value: res[i]});
	}

	//load địa chỉ
	loadAreas(){
		this.areaService.findAreas().subscribe( res => {
			this.initSelect(this.areas, res);	
		}, error => {});
	}
	AreaChange(){
		if (this.form.value.selectedAreas.length == 0) {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			return;
		}
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedAreas.length; i++){
			if (i <this.form.value.selectedAreas.length - 1)	
				ids += this.form.value.selectedAreas[i].code + ',';
			else 
				ids += this.form.value.selectedAreas[i].code;
		}

		this.areaService.findCityByCenterCodeIn(ids).subscribe(res => {
			this.form.value.selectedProvinces = [];
			this.form.value.selectedShops = [];
			this.provinces = [];
			for (let i=0; i<res.length; i++){
				this.provinces.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	ProvinceChange(){
		if (this.form.value.selectedProvinces.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.form.value.selectedProvinces.length; i++){
			if (i <this.form.value.selectedProvinces.length - 1)	
				ids += this.form.value.selectedProvinces[i].payAreaCode+ ',';
			else 
				ids += this.form.value.selectedProvinces[i].payAreaCode;
		}

		this.areaService.findShopByProvinceIn(ids).subscribe(res => {
			this.form.value.selectedShops = [];
			this.shops = [];
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});	
		
	}
    //
    create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
        // this.loadMeidaIds();
        // this.loadAreas();
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;

		this.form.controls['content'].setValue(item.content, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		// this.form.controls['mediaId'].setValue(item.mediaId, { onlySelf: true });
		
 
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		
		// this.service.delTarget(this.delItem).subscribe(
		// 	res => {
		// 		this.isLoading = false;
		// 		this.delItem = null;
		// 		this.displayDel = false;
		// 		this.loadItems();
		// 		this.showMes('warn', 'Xóa thành công');
				
		// 	},
		// 	error => {}
		// );
	}
	onSubmit(value:any){
		if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
			 return;
		}
		// if (this.form.controls['startDate'].value < new Date().getTime()){
		// 	 this.showMes('error', 'Ngày bắt đầu phải lớn hơn ngày hiện tại');
		// 	 return;
		// }
		if (this.form.controls['endDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày kết thúc phải lớn hơn ngày hiện tại');
			 return;
		}
		if ((new Date()).toDateString() != this.form.controls['startDate'].value.toDateString() && this.form.controls['startDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải lớn hơn hoặc bằng ngày hiện tại');
			 return;
		}
		this.isLoading = true;
		// value.shopId = '';
		// value.mediaId = '';
		// for (let i=0; i<value.selectedShops.length; i++){
		// 	if (i < value.selectedShops.length - 1)
		// 		value.shopId += value.selectedShops[i].id +',';
		// 	else
		// 		value.shopId += value.selectedShops[i].id;
		// }

		// for (let i=0; i<value.mediaIds.length; i++){
		// 	if (i < value.mediaIds.length - 1)
		// 		value.mediaId += value.mediaIds[i] +',';
		// 	else
		// 		value.mediaId += value.mediaIds[i];
		// }
		// delete value.selectedAreas;
		// delete value.selectedProvinces;
		// delete value.selectedShops;
		// delete value.mediaIds;
		value.sourceId = 0;
		if (this.formCreate){

			this.service.requestCampaign(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
					this.router.navigate(['/add-request-media-campaign', res.id]);
				},
				error => {}
			)
		}else{

			// value.startDate = this.vDate(value.startDate);
			// value.endDate = this.vDate(value.endDate);
			// this.service.updateTarget(value, value.id).subscribe(
			// 	res => {
			// 		this.isLoading = false;
			// 		this.displayForm = false;
			// 		this.loadItems();
			// 		this.showMes('warn', 'Sửa thành công');
			// 	},
			// 	error => {}
			// )
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}

}
