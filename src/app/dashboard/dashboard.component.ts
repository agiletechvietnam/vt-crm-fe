import {Component, OnInit, ViewChild} from "@angular/core";
import {DashboardService} from "../shared/services/dashboard";
import { UIChart } from 'primeng/primeng';
import {Router} from '@angular/router';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
    @ViewChild('chart1') chart1: UIChart;
    @ViewChild('chart2') chart2: UIChart;
    @ViewChild('chart3') chart3: UIChart;
    @ViewChild('chart4') chart4: UIChart;
    @ViewChild('chart5') chart5: UIChart;
    @ViewChild('chart6') chart6: UIChart;
    datas: any= [];
    isLoading: boolean = false;
    constructor(
        private service: DashboardService,
        private router: Router
    ) {
        
        let user = JSON.parse(localStorage.getItem('user'));
        if (user.staffRole == 'TST') 
            this.router.navigate(['/customer-store']);
        if (user.staffRole == 'NVST')
            this.router.navigate(['/staff-notification']);
            
        for (let i=0; i<6; i++){
            this.datas[i] ={
                labels: ['Thành công', 'Thất bại', 'Chưa chăm sóc'],
                datasets: [
                    {
                        data: [1,1,1],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }
                ]
            };
        }
        let f = {"sql":"select * from (select ID,NAME, NVL((TARGET_NUMBER - RESULT_NUMBER),0) as UNCARE_NO,NVL(CARE_SUCCESS,0) as SUCCESS_NO, NVL((RESULT_NUMBER-CARE_SUCCESS),0) as FAIL_NO,Start_Date,End_date, status  from CRM_CAMPAIGN cc order by id desc) where rownum <=6 AND STATUS NOT IN (1,2,4)"};

        this.service.findCampaign(f).subscribe(res =>{
            for (let i=0; i<res.length; i++){
                let d = {
                    labels: ['Thành công', 'Thất bại', 'Chưa chăm sóc'],
                    name: res[i].NAME,
                    startDate: res[i].START_DATE,
                    endDate: res[i].END_DATE,
                    datasets: [
                        {
                            data: [res[i].SUCCESS_NO, res[i].FAIL_NO, res[i].UNCARE_NO],
                            backgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                        
                            ]
                        }]
                };
                this.datas[i] = Object.assign({}, d);
                let that = this;
                setTimeout(function(){
                    that.chart1.reinit();
                    that.chart2.reinit();
                    that.chart3.reinit();
                    that.chart4.reinit();
                    that.chart5.reinit();
                    that.chart6.reinit();

                }, 100);
            }
    

        },error =>{});
        
     }
    ngOnInit() {
        
    }
    

}
