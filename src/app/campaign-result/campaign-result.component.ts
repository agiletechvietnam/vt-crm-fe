import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { CampaignService } from "../shared/services/campaign.service";
import { Router, ActivatedRoute } from '@angular/router';
import {HostConstant} from '../shared/host.constant';
import {StoreService} from "../shared/services/store.service";
@Component({
    selector: 'app-campaign-result',
    templateUrl: './campaign-result.component.html',
    styleUrls: ['./campaign-result.component.css']
})
export class CampaignResultComponent implements OnInit {
    private items: any[] = [];
    private isLoading: boolean = false;
    public subscription: Subscription;
    private user;
    private campaignId: number;
    private mes: Message[];
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: CampaignService,
        private storeService: StoreService
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['id'];
            this.loadItems();
        });

    }
    loadItems(){
        this.isLoading = true;
        this.service.findResultStaffsCampaignByShopId(this.user.shopId, this.campaignId).subscribe(
			res => {
                this.items =res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
    }
    baocao(){
        
        this.isLoading = true;
        let that = this;
        Promise.all([that.findStore(), that.findCampaign()]).then(function(values){

            let query1 = {
                "sql":"SELECT * FROM CRM_RPT_NANGSUAT_SIEUTHI WHERE RPT_TYPE='Tonghop' and CAMPAIGN_ID= "+that.campaignId+" and shop_ID="+that.user.shopId
            };
            let query2 = {
                "sql":"select * from CRM_RPT_NANGSUAT_SIEUTHI where CAMPAIGN_ID = "+that.campaignId+" and RPT_TYPE='Survey' and  shop_id = "+that.user.shopId+" order by STT asc, STT_SUB asc"
            }
            let query3= {
                "sql":"select STAFF_NAME,TARGET_NUMBER,RESULT_NUMBER,ROUND(RESULT_NUMBER*100/TARGET_NUMBER) as PERCENT from CRM_CAMPAIGN_STAFF where CAMPAIGN_ID = "+that.campaignId+" and shop_id = "+that.user.shopId+" and TARGET_NUMBER>0"
            }
            let query4 ={
                "sql":"select * from CRM_CAMPAIGN_CUSTOMER where shop_Id = "+that.user.shopId+" and campaign_id = "+that.campaignId
            };

            

            let query5 = {
                "sql":"SELECT SHOP_CODE,STAFF_CODE, STAFF_NAME,TARGET_NUMBER,RESULT_NUMBER,ROUND(RESULT_NUMBER*100/TARGET_NUMBER) as HoanThanh,SUCCESS_NO, ROUND(SUCCESS_NO*100/TARGET_NUMBER) as ThanhCong,FAIL_NO,ROUND(FAIL_NO*100/TARGET_NUMBER) as ThatBai, TARGET_NUMBER-RESULT_NUMBER as UNCARE_NO,100 - ROUND(RESULT_NUMBER*100/TARGET_NUMBER) as ChuaThucHien, ALARM_1,RESULT_1,ALARM_2,RESULT_2,ALARM_3,RESULT_3 from CRM_CAMPAIGN_STAFF where campaign_id = "+that.campaignId+" and target_number > 0 and SHOP_CODE = '"+that.user.shopCode+"'order by SHOP_CODE"
            };
            localStorage.setItem('report_url', HostConstant.API + 'domain/get-sql-result');
            localStorage.setItem('report_query1', JSON.stringify(query1));
            localStorage.setItem('report_query2', JSON.stringify(query2));
            localStorage.setItem('report_query3', JSON.stringify(query3));
            localStorage.setItem('report_query4', JSON.stringify(query4));
            
            localStorage.setItem('report_query5', JSON.stringify(query5));
            localStorage.setItem('report_shop', JSON.stringify(values[0]));
            localStorage.setItem('report_campaign', JSON.stringify(values[1]));
            window.open(HostConstant.HOST+'/baocao/chi_tieu_cua_sieu_thi.html');

            that.isLoading = false;
        });
    }
    baocaochamsoc(){
        this.isLoading = true;
        let that = this;
        Promise.all([that.findStore(), that.findCampaign()]).then(function(values){
            let resultCare = HostConstant.API+'survey/get-response-by-campaign-and-shop-id?campaignId='+that.campaignId+'&shopId='+that.user.shopId;
            localStorage.setItem('report_campaign', JSON.stringify(values[1]));
            let survey = HostConstant.API+'survey/get-by-survey-id?surveyId=';
            localStorage.setItem('resultCare',resultCare);
            localStorage.setItem('survey', survey);
            window.open(HostConstant.HOST+'/baocao/survey_shop.html');
            that.isLoading = false;
        });
    }
    findStore(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.storeService.findStoreById(that.user.shopId).subscribe(res => {
                resolve(res);
            }, error => {});
        });
    }
    findCampaign(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.findCampaignById(that.campaignId).subscribe(res => {
                resolve(res);
            }, error => {});
        });
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
