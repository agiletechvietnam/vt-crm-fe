import {Component, OnInit, ViewChild} from "@angular/core";
import {StaffService} from "../shared/services/staff.service";
import {StaffSearchModel} from "../shared/models/staff.search";
import {StaffModel} from  "../shared/models/staff.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {StoreService} from '../shared/services/store.service';
import {Message} from 'primeng/primeng';
import {FileUpload} from 'primeng/primeng';
import { FilenameConstant } from "../shared/filename.constant";
import { HostConstant } from "../shared/host.constant";
@Component({
    selector: 'app-staff',
    templateUrl: './staff.component.html',
    styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
    @ViewChild('upfile') upfile: FileUpload;
    private mes: Message[];
    private staffSearch = new StaffSearchModel();
    private idDel: number;
    private staffs: any[] = [];
    private isLoading: boolean = false;
    private form: FormGroup;
    private displayForm:boolean = false;
    private roles: any[] = [];
    private shops: any[] = [];
    private staffStatus: any[] = [];
    private shopOptions: any[] = [];
    private totalRecords: number;
    private page:number;
    private user;
    private search = "";
    private api = HostConstant.API;
	private host = HostConstant.HOST;
    private displayFormUpload: boolean = false;
    constructor(private staffService: StaffService, private fb: FormBuilder, private storeService: StoreService) { }
    
 
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        
        this.form = this.fb.group({
			'staffCode': new FormControl(),
            'id': new FormControl(),
            'shopCode': new FormControl(),
            'shopId': new FormControl(),
            'mobile': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'name': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'email': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
            'staffRole': new FormControl(),
			
		});
        if (this.user.staffRole == 'TTT')
            this.roles = [
                {label: 'Vai trò', value: null},
                {label: 'Trưởng trung tâm', value: 'TTT'},
                {label: 'Nhân viên trung tâm', value: 'NVTT'},
                {label: 'Trưởng siêu thị', value: 'TST'},
                {label: 'Nhân viên siêu thị', value: 'NVST'},
                {label: 'Khóa', value: 'BLOCK'},
            ];
        else
            this.roles = [
                {label: 'Vai trò', value: null},
                {label: 'Nhân viên trung tâm', value: 'NVTT'},
                {label: 'Trưởng siêu thị', value: 'TST'},
                {label: 'Nhân viên siêu thị', value: 'NVST'},
                {label: 'Khóa', value: 'BLOCK'},
            ];
        this.staffStatus = [
            {label: 'Đang làm việc', value: 1},
            {label: 'Đã Nghỉ việc', value: -1},
        ];
        this.loadStaffs();

    }
    loadLazy(event: any) {
        this.isLoading = true;
        this.page  = event.first/event.rows;	
        this.staffService.findStaffs(this.search, this.page, event.rows).subscribe(
            res => {
                this.staffs= res.content;
                this.totalRecords = res.totalElements;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    change(){
        this.search = "";
        this.loadStaffs();
    }
    loadStaffs(){
        this.isLoading = true;
        this.staffService.findStaffs(this.search, 0, 20).subscribe(
            res => {
                
                this.staffs = res.content;
                this.totalRecords = res.totalElements;            
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    loadStores(){
        this.shopOptions = [];  
        this.isLoading = true;
		this.storeService.findAllStores().subscribe(
			res => {
				this.shops = res;
                this.shopOptions.push({label: "Siêu thị", value: null});
                for (let i=0; i<this.shops.length; i++)
                    this.shopOptions.push({label: this.shops[i].shopCode +' - '+ this.shops[i].name, value: this.shops[i].id});

                this.isLoading = false;
            },
			error => {
				error => console.log(error);
			}
		);
	}

    public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
    

    update(item){
        this.form.reset();
		this.displayForm = true;
		this.form.controls['staffCode'].setValue(item.staffCode, { onlySelf: true });
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['email'].setValue(item.email, { onlySelf: true });
        this.form.controls['mobile'].setValue(item.mobile, { onlySelf: true });
        this.form.controls['staffRole'].setValue(item.staffRole, { onlySelf: true });
        this.form.controls['shopCode'].setValue(item.shopCode, { onlySelf: true });
        this.form.controls['shopId'].setValue(item.shopId, { onlySelf: true });
        this.form.controls['name'].setValue(item.name, { onlySelf: true });
        this.loadStores();
    }
    onSubmit(value){
       
        for (let i=0; i<this.shops.length; i++)
            if (this.shops[i].id == value.shopId){
                value.shopCode = this.shops[i].shopCode;
                break;
            }
        this.staffService.updateRole(value, value.id).subscribe(res => {
            this.showMes('success', 'Sửa thành công');
            this.displayForm = false;
            
            this.loadStaffs();
        }, error => {

        })
    }
    onBeforeUpload(event){
        this.isLoading = true;
    }
    onBeforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
	onUpload(event) {
		this.showMes('success', 'Up load file thành công');
		this.loadStaffs();
		this.isLoading = false;
		this.displayFormUpload = false;
    }
    onSelect(event){
        if (event.files[0].name != FilenameConstant.staff){  
            this.upfile.clear();
            this.showMes('error', 'File cần có tên là '+ FilenameConstant.staff);
        }
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
