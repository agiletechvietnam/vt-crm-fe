/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CampainComponent } from './campain.component';

describe('CampainComponent', () => {
  let component: CampainComponent;
  let fixture: ComponentFixture<CampainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
