import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { CampaignModel } from "../shared/models/campaign.model";
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
    selector: 'app-campaign',
    templateUrl: './campaign.component.html',
    styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {
    //private campaigns: CampaignModel[];
    private items:any[];
    private isLoading: boolean = false;
    private user;
    private form: FormGroup;
    private title: string;
    private formCreate: boolean;
    private displayForm: boolean = false;
    private delItem: any;
    private displayDel: boolean = false;
    private mes: Message[];
    constructor(
        private service: CampaignService,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'campaignDescription': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000), this.NoWhitespaceValidator])),
		});

        this.loadItems();
        
    }
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
    loadItems(){
        this.isLoading = true;
        this.service.findCampaignsByStatus().subscribe(
            res => {
                this.items = res;
                this.isLoading = false;
            },
            error => {
                error => console.log(error);
            }
        );
    }
    create(){
		this.form.reset();
		this.title = "Thêm chiến dịch";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa chiến dịch";
		this.formCreate = false;
		this.displayForm = true;

		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: true });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: true });
		this.form.controls['campaignDescription'].setValue(item.targetDescription, { onlySelf: true });
		
 
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delCampaign(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			this.service.addCampaign(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('warn', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			value.startDate = this.vDate(value.startDate);
			value.endDate = this.vDate(value.endDate);
			this.service.updateCampaign(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('warn', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
 