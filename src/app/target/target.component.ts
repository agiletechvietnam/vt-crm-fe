import { Component, OnInit } from '@angular/core';
import {TargetModel} from  '../shared/models/target.model';
import {TargetService} from  '../shared/services/target.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
	selector: 'app-target',
	templateUrl: './target.component.html',
	styleUrls: ['./target.component.css']
})
export class TargetComponent implements OnInit {
	private items: TargetModel[];
	private mes: Message[];
	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
	private user;
	constructor(
		private service: TargetService,
		private fb: FormBuilder,
		 
	) { }
 
	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'targetDescription': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
		this.loadItems();
 
	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	loadItems(){
		this.isLoading = true;
		this.service.findTargets().subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
	}
	create(){
		this.form.reset();
		this.title = "Thêm chỉ tiêu";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa chỉ tiêu";
		this.formCreate = false;
		this.displayForm = true;

		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: true });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: true });
		this.form.controls['targetDescription'].setValue(item.targetDescription, { onlySelf: true });
		
 
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	delete(item){
		this.delItem = item; 
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delTarget(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
			 return;
		}
		if ((new Date()).toDateString() != this.form.controls['startDate'].value.toDateString() && this.form.controls['startDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày bắt đầu phải lớn hơn hoặc bằng ngày hiện tại');
			 return;
		}
		if (this.form.controls['endDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày kết thúc phải lớn hơn ngày hiện tại');
			 return;
		}
		this.isLoading = true;
		if (this.formCreate){
			this.service.addTarget(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			value.startDate = this.vDate(value.startDate);
			value.endDate = this.vDate(value.endDate);
			this.service.updateTarget(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('warn', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}

} 
