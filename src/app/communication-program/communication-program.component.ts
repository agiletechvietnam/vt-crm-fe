import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {ChannelService} from "../shared/services/channel.service";
@Component({
    selector: 'app-communication-program',
    templateUrl: './communication-program.component.html',
    styleUrls: ['./communication-program.component.css']
})
export class CommunicationProgramComponent implements OnInit {
    private items: any[];
    private mes: Message[];
    private isLoading: boolean = false;
	private form: FormGroup;
	private title: string;
	private displayForm: boolean = false;
	private channels: SelectItem[] = [];
    constructor(
        private service: MediaCampaignService,
		private fb: FormBuilder,
		private channelService: ChannelService,
    ) { }

    ngOnInit() {
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'expectedResult': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
			'mediaId': new FormControl('', Validators.compose([Validators.required])),
			'staffName': new FormControl('', Validators.compose([Validators.required])),
			'shopId': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
        this.loadItems();
    }
    
    loadItems(){
		this.isLoading = true;
		this.service.findMediaCampaigns('2,3,4,5').subscribe(
			res => {
				this.isLoading = false;
				this.items = res;
			},
			error => {
				error => console.log(error);
			}
		);
	}
	view(item){
		
		if (this.channels.length == 0){
			this.isLoading = true;
			this.channels.push({label:'Hình thức', value:null});
			this.channelService.findChannels().subscribe(res => {
				for(let i=0; i<res.length; i++){
					this.channels.push({label: res[i].name, value: res[i].id});
				}
				
				this.isLoading = false;
			}, error => {

			});
		}
		this.form.reset();
		this.title = "Xem chi tiết";
		this.displayForm = true;
		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: true });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: true });
		this.form.controls['expectedResult'].setValue(item.expectedResult, { onlySelf: true });
		this.form.controls['mediaId'].setValue(item.mediaId, { onlySelf: true });
		this.form.controls['staffName'].setValue(item.staffName, { onlySelf: true });
		

	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
    

}
