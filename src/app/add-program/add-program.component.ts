import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {ProgramModel} from "../shared/models/program.model";
import {SelectItem} from 'primeng/primeng';
import {ChannelService} from "../shared/services/channel.service";
import {AreaService} from '../shared/services/area.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
@Component({
	selector: 'app-add-program',
	templateUrl: './add-program.component.html',
	styleUrls: ['./add-program.component.css']
})
export class AddProgramComponent implements OnInit {
	private items: any[] = [];
	private mes: Message[];
	private displayForm: boolean = false;
	private form: FormGroup;
	private channels: any[] = [];
	private formCreate: boolean;
	private title: string;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean = false;
	private staffs: any[] = [];
	private areas: any[] = [];
	public program: any={};
	public subscription: Subscription;
	private ichannels: any[] = [];
	private shopId;
	private goods: any[] =[];

	constructor(
		private channelService: ChannelService,
		private fb: FormBuilder,
		private areaService: AreaService,
		private service: MediaCampaignService,
		private activatedRoute: ActivatedRoute,
	) { }
  
	ngOnInit() {
		var user = JSON.parse(localStorage.getItem("user"));
		this.shopId = user.shopId;
		this.form = this.fb.group({
			'id': new FormControl(),
			'areaShopId': new FormControl('', Validators.compose([Validators.required])),
			'startDate': new FormControl('', Validators.compose([Validators.required])),
			'endDate': new FormControl('', Validators.compose([Validators.required])),
			'amount': new FormControl(),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
		
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this.program.id = params['id'];
			this.loadProgram();
			this.loadSelect();
        });
		this.form.controls['amount'].setValue(0, {onlySelf: false});
		this.ichannels.push({name: "Phát tờ rơi", number: 1}, {name: "Quảng các", number: 1});

	}
	
	loadProgram(){
        this.isLoading = true;
        this.service.findMediaCampaignById(this.program.id).subscribe(
			res => {
				this.program = res;
				this.isLoading = false;
				this.loadGoodsOfCampaign();
				this.loadItems();
			}, 
			error => {
				error> console.log(error);
			}
		)
    }
	loadItems(){
		this.isLoading = true;
		this.service.findAreaByMediaId(this.program.id).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
	loadChannelsOfCampaign(){
		this.isLoading = true;
		this.service.findChannelsOfCampgin(this.shopId, this.program.campaignRequestId).subscribe(res=> {
			this.channels = res;
			this.isLoading = false;
			
		}, error=>{

		})
	} 
	loadGoodsOfCampaign(){
		this.isLoading = true;
		this.service.findGoodsOfCampgin(this.shopId, this.program.campaignRequestId).subscribe(res=> {
			this.goods = res;
			this.isLoading = false;
			
		}, error=>{

		})
	}
	
	create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
		if (this.channels.length == 0){
			this.loadChannelsOfCampaign();
		}
		for (let i =0; i<this.channels.length; i++)
			this.channels[i].quantity = 0;
	}

	loadSelect(){
		
		if (this.areas.length == 0){
			this.isLoading = true;
			this.areas.push({label:'Địa bàn', value:null});
			this.areaService.findAreasByShopId(this.shopId).subscribe(res => {	
				for(let i=0; i<res.length; i++){
					this.areas.push({label: res[i].address, value: res[i].id, ob: res[i]});
				}
				this.isLoading = false;
			}, error => {});
		}
	}
	suane(){
		let total = 0;
		for (let i=0; i<this.channels.length; i++){
			this.channels[i].amount = this.channels[i].quantity * this.channels[i].price;
			total += this.channels[i].amount;
		}
		
	}

	update(item){
		console.log(item.id);
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: false });
		this.form.controls['amount'].setValue(item.amount, { onlySelf: false });
		this.form.controls['description'].setValue(item.description, { onlySelf: false });
		this.form.controls['areaShopId'].setValue(item.areaShopId, { onlySelf: false });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: false });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: false });

		this.channels = [{mediaId: item.mediaId, name: item.mediaName, quantity: item.quantity, amount: item.amount, price: item.price}];
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	guiDuyet(){
		if (this.program.totalCost > this.program.budget || ((this.program.totalCost/this.program.budget) * 100) <80){
			this.showMes('error', 'Tổng tiền phải lớn hơn 80% ngân sách và nhỏ hơn ngân sách');
			 return;
		}
		// if (this.program.budget != null && this.program.totalCost > this.program.budget){
		// 	this.showMes('error', 'Tổng tiền không được lớn hơn ngân sách');
		// 	return; 
		// } 
		this.isLoading = true;
		this.service.updateMediaCampaign({status: 2}, this.program.id).subscribe(
			res => {
				this.displayForm = false;
				this.loadProgram();
				this.isLoading = false;
				this.showMes('success', 'Gửi duyệt thành công');
			},
			error => {}
		)
	}
	acceptDel(){
		this.service.delAreaInMedia(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	onSubmit(value:any){
		// console.log('onsubmit');
		// console.log(value);
		// for (let i=0; i<this.items.length && this.formCreate == true; i++)
		// 	if (this.items[i].areaShopId == value.areaShopId){
		// 		this.showMes('error', 'Bạn đã chọn địa bàn này rồi');
		// 		return;
		// 	} 

		if (this.formCreate){
			if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
				this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
				return;
			}
			if (this.form.controls['startDate'].value < this.program.startDate){
				this.showMes('error', 'Ngày bắt đầu của địa bàn phải lớn hơn hoặc bằng ngày bắt đầu của chiến dịch');
				return;
			}
			if (this.form.controls['endDate'].value > this.program.endDate){
				this.showMes('error', 'Ngày kết thúc của địa bàn phải nhỏ hơn hoặc bằng ngày kết thúc của chiến dịch');
				return;
			}
		}
		
		 
		 
		let data = {
			id: this.program.id,
			campaignDetail:{
                areaShopId : value.areaShopId,
                startDate: value.startDate,
                endDate: value.endDate,
                description : value.description
        	},
			prices: [],
		};

		for (let i=0;i<this.channels.length; i++){
			if (this.channels[i].quantity != 0 && this.channels[i].quantity != '' && this.channels[i].quantity != null){
				data.prices.push({
					mediaId: this.channels[i].mediaId,
					price: this.channels[i].price,
					quantity : this.channels[i].quantity,
					amount : this.channels[i].amount
				});
			}
		}
	

		if (data.prices.length == 0) {
			this.showMes('error', 'Bạn điền số lượng cho ít nhất 1 hình thức truyền thông');
			return;
		}
		if (this.formCreate == true){
			
			this.isLoading = true;
			this.service.addMediaCampaignDetail(data).subscribe(res => {
				this.isLoading = false;
				this.displayForm = false;
				this.loadItems();
				this.loadProgram();
				this.showMes('success', 'Thêm thành công');
			}, error => {
				this.isLoading = false;
				this.showMes('error', 'Thêm thất bại');
			});
		}else{
			this.isLoading = true;
			// this.service.updateAreaToMedia(data, value.id).subscribe(res => {
			// 	this.isLoading = false;
			// 	this.loadItems();
			// 	this.loadProgram();
			// 	this.showMes('success', 'Sửa thành công');
			// }, error => {
			// 	this.isLoading = false;
			// 	this.showMes('error', 'Sửa thất bại');
			// });

			delete data.campaignDetail.endDate;
			delete data.campaignDetail.startDate;
			// console.log('sua');
			this.service.updateMediaCampaignDetail(data).subscribe(res => {
				this.loadItems();
				this.loadProgram();
				this.displayForm = false;
			}, error => {

			});
			
			
			
		}

	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 5000);
	}
}


