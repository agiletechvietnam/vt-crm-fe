export class WardModel {
    id: number;
    name: string;
    city_id: number;
    district_id: number;
    sm_ward_id: number;
    status: number;
    payAreaCode: string;
}