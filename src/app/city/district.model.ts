export class DistrictModel {
    id: number;
    name: string;
    city_id: number;
    create_date: string;
    update_date: string;
    bh_district_id: number;
    sm_district_id: number;
    payAreaCode: string;
}