import { Component, OnInit } from '@angular/core';
import { CityService } from "./city.component.service";
import {CitySearchModel} from "./city.search";
@Component({
	selector: 'app-city',
	templateUrl: './city.component.html',
	styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

	constructor(private cityService: CityService) { }
	private cities = [];
	private isLoadding: boolean = false;
	private citySearchModel = new CitySearchModel();
	ngOnInit() {
		this.isLoadding = true;
		this.cityService.findCities().subscribe((response: any) => {
			this.cities = response;
			this.isLoadding = true;
		});
	}
}
