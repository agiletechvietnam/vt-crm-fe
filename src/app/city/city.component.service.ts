import {Injectable} from "@angular/core";
import {HttpService} from "../shared/http.service";
import {Observable} from "rxjs";
import {APIConstant} from "../shared/api.constant";
import 'rxjs/add/operator/map';
import {CitySearchModel} from "./city.search";
import {CityModel} from "./city.model";
import {DistrictModel} from "./district.model";
import {WardModel} from "./ward.model";
import {Http, Response} from "@angular/http";
@Injectable()
export class CityService {

    constructor(private http: HttpService) {}

    findCities(): Observable<CityModel[]> {
        return this.http.get(APIConstant.FIND_CITIES)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findDistrictsByCityId(id): Observable<DistrictModel[]>{
        return this.http.get(APIConstant.FIND_DISTRICTS_BY_CITY_ID+'?parentCode='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    findWardsByDistrictId(id): Observable<WardModel[]>{
        return this.http.get(APIConstant.FIND_WARDS_BY_DISTRICT_ID+'?parentCode='+id)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error));
    }
}