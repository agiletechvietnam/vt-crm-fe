export class CityModel {
    id: number;
    name: string;
    sm_city_id: number;
    create_date: string;
    update_date: string;
    payAreaCode: string;
}