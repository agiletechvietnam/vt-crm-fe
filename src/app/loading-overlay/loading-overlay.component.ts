import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

@Component({
    selector: 'loading-overlay',
    templateUrl: './loading-overlay.component.html',
    styleUrls: ['./loading-overlay.component.css']
})
export class LoadingOverlayComponent implements OnInit, AfterViewInit {
    @Input()    
    isLoading: boolean;
    private height;
    private width;
    constructor() { }

    ngOnInit() {
        
    }
    
    
    ngAfterViewInit(){
        var a = <HTMLElement>(document.getElementsByClassName('wrapper')[0]);
 
        this.height = a.clientWidth;

        var b = <HTMLElement>(document.getElementsByClassName('main')[0]);
        this.width = b.offsetWidth;
        
    }        
}
