import {Location} from '@angular/common';
import {OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from "./shared/http.service";
import {Component,AfterViewInit,OnDestroy,ViewChild,ElementRef,Renderer,trigger,state,transition,style,animate} from '@angular/core';
import {HostConstant} from "./shared/host.constant";

declare var jQuery: any;

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
        trigger('submenu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppComponent implements OnInit,AfterViewInit,OnDestroy {
    location: Location
    private user: any;
    constructor(private _location: Location,
                private router: Router,
                private http: HttpService,
                public renderer: Renderer) {
    
        this.user = JSON.parse(localStorage.getItem('user'));
        
    }
    

    ngOnInit() {
    }

    displayHeader() {
    }

    getFullName() {
    }

    getEmail() {
    }

    logout() {
        localStorage.removeItem("user");
        window.location.href = HostConstant.HOST+'/login.html';
    }
    public menuInactiveDesktop: boolean;
    
    public menuActiveMobile: boolean;
    
    public profileActive: boolean;
    
    public topMenuActive: boolean;
    
    public topMenuLeaving: boolean;
    
    @ViewChild('scroller') public scrollerViewChild: ElementRef;
    
    public scroller: HTMLDivElement;
    
    documentClickListener: Function;
    
    menuClick: boolean;
    
    topMenuButtonClick: boolean;
    


    ngAfterViewInit() {
        this.scroller = <HTMLDivElement> this.scrollerViewChild.nativeElement;
        
        //hides the overlay menu and top menu if outside is clicked
        this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
            if(!this.isDesktop()) {
                if(!this.menuClick) {
                    this.menuActiveMobile = false;
                }
                
                if(!this.topMenuButtonClick) {
                    this.hideTopMenu();
                }
                
                this.menuClick = false;
                this.topMenuButtonClick = false;
            }
        });
    }    
    
    toggleMenu(event: Event) {
        this.menuClick = true;
        if(this.isDesktop()) {
            this.menuInactiveDesktop = !this.menuInactiveDesktop;
            if(this.menuInactiveDesktop) {
                this.menuActiveMobile = false;
            }
        }
        else {
            this.menuActiveMobile = !this.menuActiveMobile;
            if(this.menuActiveMobile) {
                this.menuInactiveDesktop = false;
            }
        }
        
        if(this.topMenuActive) {
            this.hideTopMenu();
        }
        
        event.preventDefault();
    }
    
    toggleProfile(event: Event) {
        this.profileActive = !this.profileActive;
        event.preventDefault();
    }
    
    toggleTopMenu(event: Event) {
        this.topMenuButtonClick = true;
        this.menuActiveMobile = false;
        
        if(this.topMenuActive) {
            this.hideTopMenu();
        }
        else {
            this.topMenuActive = true;
        }
        
        event.preventDefault();
    }
    
    hideTopMenu() {
        this.topMenuLeaving = true;
        setTimeout(() => {
            this.topMenuActive = false;
            this.topMenuLeaving = false;
        }, 500);
    }
        
    onMenuClick() {
        this.menuClick = true;
        
        setTimeout(() => {
            jQuery(this.scroller).nanoScroller();
        }, 600);
    }
    
    isDesktop() {
        return window.innerWidth > 1024;
    }
    
    onSearchClick() {
        this.topMenuButtonClick = true;
    }
    
    ngOnDestroy() {
        if(this.documentClickListener) {
            this.documentClickListener();
        }
    }
}