import {Component, OnInit, ViewChild} from "@angular/core";
import {ChannelModel} from "../shared/models/channel.model";
import {ChannelService} from "../shared/services/channel.service";
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';

import {Message} from 'primeng/primeng';
import {AreaService} from '../shared/services/area.service';
import {GoodsService} from '../shared/services/goods.service';
import {SelectItem} from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { HostConstant } from "../shared/host.constant";
import { FilenameConstant } from "../shared/filename.constant";
import { Router, ActivatedRoute } from '@angular/router';
import {FileUpload} from 'primeng/primeng';
@Component({
    selector: 'app-add-gn',
    templateUrl: './add-request-media-campaign.component.html',
    styleUrls: ['./add-request-media-campaign.component.css']
})
export class AddRequestMediaCampaignComponent implements OnInit {
	@ViewChild('upfile') upfile: FileUpload;
	@ViewChild('upfile1') upfile1: FileUpload;
	public subscription: Subscription;
    private items: ChannelModel[]; 
	private mes: Message[];

	private form: FormGroup;
	// private formAddGoodGroup: FormGroup;
	private formShop: FormGroup;
	private formDoanhthu: FormGroup;
	private formNgansach: FormGroup;
	private formCreate: boolean;
    private displayFormGoods: boolean = false;
	private displayFormDoanhthu: boolean = false;
	private title;
    private channels: any[] = [];

    private areas: SelectItem[] = [];
	private provinces: SelectItem[] = [];
	private shops: SelectItem[] = [];

    private isLoading: boolean = false;

    private selectedAreas;
    private selectedProvinces;
    private selectedShops;
	private channelsInRequest;
	private id:number;
	private selectedChannel: any = {};
	private selectedGoods: any = [];
	private goodsInRequest;
	private goodGroupsInRequest;
	private shopsInRequest;

	private doanhthuTong;
	private doanhthuNhom;
	private doanhthuSanpham;
	private campaignShops;

	private api = HostConstant.API;
	private host = HostConstant.HOST;
	private user;

	private domains: SelectItem[] = [];
	private goodsGroups: SelectItem[] = [];
	private goods: SelectItem[] = [];

	private displayFormUploadNgansach: boolean = false;
	private displayFormUploadSieuthi: boolean = false;

	// private displayFormGoodGroup: boolean = false;
	private displayUpdateNgansach:boolean = false;
	private filename = FilenameConstant;
	private TYPES = {
		TONG: 2, NHOM: 1, SANPHAM: 0
	};
    constructor(
        private fb: FormBuilder,
        private channelService: ChannelService,
		private goodsService: GoodsService,
        private areaService: AreaService,
		private service: MediaCampaignService,
		private activatedRoute: ActivatedRoute
    ) { }
 
    ngOnInit() { 
		this.user = JSON.parse(localStorage.getItem('user'));
        this.form = this.fb.group({
			'selectedDomains': new FormControl('', Validators.compose([Validators.required])),
			'selectedGoodsGroups': new FormControl('', Validators.compose([Validators.required])),
			'selectedGoods':  new FormControl(''),
		});

		this.formShop = this.fb.group({
			'selectedAreas': new FormControl(),
			'selectedProvinces': new FormControl(),
			'selectedShops': new FormControl(),
		});

		this.formDoanhthu = this.fb.group({
			'id': new FormControl(''),
			'targetNumber': new FormControl('', Validators.compose([Validators.required])),
		});
		this.formNgansach = this.fb.group({
			'id': new FormControl(''),
			'budget': new FormControl('', Validators.compose([Validators.required])),
		});
		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
		});
		this.loadPreChannel();
		this.loadGoodsInRequest();
		this.loadShopInRequest();

		this.loadAllDoanhThu();
		this.loadCampaignShop();
    }
	genSql(type){
		return {
			"sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = "+type+" and CAMPAIGN_REQUEST_ID = "+this.id
		}
	}
	loadCampaignShop(){
		let sql = {
			"sql":"select * from CRM_BDKH_MEDIA_CAMPAIGN where CAMPAIGN_REQUEST_ID = "+this.id
		};
		this.service.runSql(sql).subscribe(res => {
			this.campaignShops = res;
		}, error => {
			
		});

	}
	// channel;
	loadPreChannel(){
		let that = this;
		Promise.all([this.loadChannels(), this.loadChannelsInRequest()]).then(function(){
			for (let i = 0; i<that.channelsInRequest.length; i++)
				for (let j=1; j<that.channels.length; j++){
					if (that.channels[j].value.id == that.channelsInRequest[i].mediaId){
						that.channels.splice(j, 1);
					}
				}
		})
        this.loadAreas();
	}
	loadChannels(){
		let that = this;
		return new Promise(function(resolve, rej){
			that.channels = [];
			that.channelService.findChannels().subscribe(
				res => {
					that.channels.push({label: "Kênh", value: null});
					for (let i=0; i<res.length; i++){
						that.channels.push({label: res[i].name, value: res[i]});   
					}
					resolve();
				},
				error => {
					error => console.log(error);
					rej();
				}
			);
		})
		
	}
	loadChannelsInRequest(){
		let that = this;
		return new Promise(function(resolve, rej){
			that.channelService.findChannelsInRequest(that.id).subscribe(
				res => {
					that.channelsInRequest = res;	
					resolve();
				},
				error => {
					rej();
				}
			)
		})
		
	}
	addChannel(){
		if (this.selectedChannel == null) {
			this.showMes('error', 'Bạn cần chọn kênh');
			return;
		}
		this.isLoading = false;
		for (let i=0; i<this.channelsInRequest.length; i++)
			if (this.selectedChannel.id == this.channelsInRequest[i].mediaId){
				this.showMes('error', 'Bạn đã thêm kênh này rồi');
				return;
			}
		this.channelService.addChannelToRequest({
			campaignRequestId: this.id,
			mediaId: this.selectedChannel.id,
			name: this.selectedChannel.name		
		}).subscribe(res => {
			this.loadPreChannel();
			this.showMes('success', 'Thêm thành công');
			this.isLoading = false;
			this.selectedChannel = null;
		}, error => {
			this.showMes('error', 'Thêm thất bại');
			this.isLoading = false;
		})
    }
	
	deleteChannel(cus){
		this.isLoading = true;
		this.channelService.deleteChannelInRequest(cus.id).subscribe(res => {
			this.loadPreChannel();
			this.showMes('success', 'Xóa thành công');
			this.isLoading = false;
		}, error => {
			this.showMes('error', 'Xóa thất bại');
			this.isLoading = false;
		})
	}
	deleteGoods(cus){
		this.isLoading = true;
		this.goodsService.deleteGoodToCampaign(cus.id).subscribe(res => {
			this.loadGoodsInRequest();
			this.showMes('success', 'Xóa thành công');
			this.isLoading = false;
		}, error => {
			this.showMes('error', 'Xóa thất bại');
			this.isLoading = false;
		})
	}
	//end channel;

	//good
	//load ngành hàng
	loadProductType(){
		
		this.isLoading = true;
		this.goodsService.findProductType().subscribe(res => {
			this.isLoading = false;
			this.domains = [];
			this.domains.push({label: "Chọn ngành hàng", value: null});
			
			for (let i=0; i<res.length; i++){
				this.domains.push({label: res[i].name, value: res[i]});
			}
		}, error=> {

		});
	}
	ProductTypeChange(){
		// if (this.form.value.selectedDomains.length == 0) return;
		
		if (this.form.value.selectedDomains ==  null) return;
		this.isLoading = true;

		this.goodsService.findGoodsgroupByProductType(this.form.value.selectedDomains.code).subscribe(res => {
			this.form.value.selectedGoodsGroups = [];
			this.form.value.selectedGoods = [];
			this.goodsGroups = [];
			this.goodsGroups.push({label: "Chọn nhóm hàng", value: null});

			this.form.controls['selectedGoodsGroups'].setValue('', { onlySelf: true });
			this.goods = [];
			this.goods.push({label: "Chọn mặt hàng", value: null});
			for (let i=0; i<res.length; i++){
				this.goodsGroups.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	// change goodsGroups 
	GoodGroupChange(){
		// if (this.form.value.selectedGoodsGroups.length == 0) return;
		

		if (this.form.value.selectedGoodsGroups == null) return;
		this.isLoading = true;
		this.goodsService.findGoodByGroupId(this.form.value.selectedGoodsGroups.id).subscribe(res => {
			this.form.value.selectedGoods = [];
			this.goods = [];
			this.goods.push({label: "Chọn mặt hàng", value: null});
			for (let i=0; i<res.length; i++){
				this.goods.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}


    //chon shop
    initSelect(e, res){
		for (let i=0; i<res.length; i++)
			this.areas.push({label: res[i].name, value: res[i]});
	}

	//load địa chỉ
	loadAreas(){
		this.areaService.findAreas().subscribe( res => {
			this.initSelect(this.areas, res);	
		}, error => {});
	}
    AreaChange(){
		if (this.formShop.value.selectedAreas.length == 0) {
			this.formShop.value.selectedProvinces = [];
			this.formShop.value.selectedShops = [];
			return;
		}
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.formShop.value.selectedAreas.length; i++){
			if (i <this.formShop.value.selectedAreas.length - 1)	
				ids += this.formShop.value.selectedAreas[i].code + ',';
			else 
				ids += this.formShop.value.selectedAreas[i].code;
		} 

		this.areaService.findCityByCenterCodeIn(ids).subscribe(res => {
			this.formShop.value.selectedProvinces = [];
			this.formShop.value.selectedShops = [];
			this.provinces = [];
			for (let i=0; i<res.length; i++){
				this.provinces.push({label: res[i].name, value: res[i]});
			}
			this.isLoading = false;
		}, error=> {

		});
	}
	
	ProvinceChange(){
		if (this.formShop.value.selectedProvinces.length == 0) return;
		this.isLoading = true;
		let ids = '';
		for(let i=0; i<this.formShop.value.selectedProvinces.length; i++){
			if (i <this.formShop.value.selectedProvinces.length - 1)	
				ids += this.formShop.value.selectedProvinces[i].payAreaCode+ ',';
			else 
				ids += this.formShop.value.selectedProvinces[i].payAreaCode;
		}

		this.areaService.findShopByProvinceIn(ids).subscribe(res => {
			this.formShop.value.selectedShops = [];
			this.shops = [];
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].shopCode +' - '+res[i].name, value: res[i]});
			}
			console.log(this.shops);
			this.isLoading = false;
		}, error=> {

		});	
		
	}
    //end chon shop

	// good
	loadGoodsInRequest(){
		this.goodsInRequest = [];
		this.goodGroupsInRequest = [];
		this.goodsService.findGoodsInRequest(this.id).subscribe(res=> {
			for(let i in res){
				if (res[i].goodsType == 0)
					this.goodsInRequest.push(res[i]);
				else 
					this.goodGroupsInRequest.push(res[i]);
			}
			
		}, error => {

		});
	}
    addGoods(){
        this.title = "Thêm";
		this.displayFormGoods = true;
		// if (this.form.controls['selectedGoods'].value == ""){
		// 	this.form.reset();
		if (this.domains.length == 0){
			this.loadProductType();
			this.goods = [];
			this.goods.push({label: "Chọn mặt hàng", value: null});
			this.goodsGroups = [];
			this.goodsGroups.push({label: "Chọn nhóm hàng", value: null});
		}
		// }else{
		// 	this.form.controls['selectedGoods'].setValue("", { onlySelf: true });
		// }	
	}
	onSubmit(value){
		let data = {
			campaignRequestId: this.id,
			goodsName: '',
			goodsIds: '',
			goodsCode: '',
			goodsType: 0
		};
		if (value.selectedGoods.goodsCode != null){
			for (let i=0; i < this.goodsInRequest.length && this.goodsInRequest != null; i++){
				if (this.goodsInRequest[i].goodsCode == value.selectedGoods.goodsCode){
					this.showMes('error', 'Bạn đã thêm mặt hàng này rồi');
					return;
				}
			}
			data.goodsName = value.selectedGoods.name;
			data.goodsIds =  value.selectedGoods.id,
			data.goodsCode = value.selectedGoods.goodsCode,
			data.goodsType = 0
		}else{
			for (let i=0; i < this.goodGroupsInRequest.length && this.goodGroupsInRequest != null; i++){
				if (this.goodGroupsInRequest[i].goodsIds == value.selectedGoodsGroups.id){
					this.showMes('error', 'Bạn đã thêm nhóm hàng này rồi');
					return;
				}
			}
			data.goodsName = value.selectedGoodsGroups.name;
			data.goodsIds =  value.selectedGoodsGroups.id,
			data.goodsCode = value.selectedGoodsGroups.goodsGroupCode,
			data.goodsType = 1
		}
			
		this.isLoading = true;
		
		this.goodsService.addGoodToCampaign(data).subscribe(res=> {
			this.showMes('success', 'Thêm thành công');
			this.loadGoodsInRequest();
			this.isLoading = false;
			this.displayFormGoods = false;
			this.selectedGoods = [];
		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Thêm thất bại');
		});
	}

	
	addShop(value){
		for (let i in this.shopsInRequest)
			for (let j in value.selectedShops){
				console.log(this.shopsInRequest[i].id + '    '+value.selectedShops[j].id);
				if (this.shopsInRequest[i].shopId  == value.selectedShops[j].id){
					this.showMes('error', 'Bạn đã chọn siêu thị "'+value.selectedShops[j].name+'" rồi');
					return;
				}
			}
		this.isLoading = true;
		let ids = [];
		for (let i=0; i<value.selectedShops.length; i++){
			ids.push(value.selectedShops[i].id);
		}
		let data = {
			campaignRequestId: this.id,
			shopIds: ids,
			budget:0
		}
		this.channelService.addShopToRequest(data).subscribe(res=> {
			this.showMes('success', 'Thêm mặt hàng thành công');
			this.isLoading = false;
		}, error => {
			this.loadShopInRequest();
			this.isLoading = false;
		});
	}
	loadShopInRequest(){
		this.channelService.findShopInRequest(this.id).subscribe(res => {
			this.shopsInRequest = res;
		}, error => {});
	}
	//end shop
	loadDoanhthu(type){
		let that = this;
		return new Promise(function(resolve, reject){
			that.service.runSql(that.genSql(type)).subscribe(res => {
				if (type == that.TYPES.TONG) that.doanhthuTong = res;
				else if (type == that.TYPES.NHOM) that.doanhthuNhom = res;
				else if (type == that.TYPES.SANPHAM) that.doanhthuSanpham = res;
				resolve();
			}, error => {
				reject();
			})
		})
	}
	update(item){
		this.displayUpdateNgansach = true;
		this.formNgansach.reset();
		this.formNgansach.controls['id'].setValue(item.id, { onlySelf: true });
		this.formNgansach.controls['budget'].setValue(item.budget, { onlySelf: true });
		
	}
	updateDoanhthu(item){
		this.displayFormDoanhthu = true;
		this.formDoanhthu.reset();
		this.formDoanhthu.controls['id'].setValue(item.ID, { onlySelf: true });
		this.formDoanhthu.controls['quantity'].setValue(item.QUANTITY, { onlySelf: true });
		
	}
	onSubmitDoanhthu(value){
		this.isLoading = true;
		this.channelService.updateDoanhthu(value).subscribe(res=> {
			this.showMes('success', 'Cập nhật doanh thu thành công');
			this.loadAllDoanhThu();
			this.displayFormDoanhthu = false;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Cập nhật doanh thu thất bại');
		});
	}
	onSubmitNgansach(value){
		this.isLoading = true;
		this.channelService.updateMediaCampaign(value, value.id).subscribe(res=> {
			this.showMes('success', 'Cập nhật ngân sách thành công');
			this.loadShopInRequest();
			this.displayUpdateNgansach = false;
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Cập nhật ngân sách thành công');
		});

	}
	onTabChange(event) {
		console.log(event.index);
		if (event.index == 3){
			this.loadAllDoanhThu();
		}else if (event.index == 4){
			this.loadCampaignShop();
		} 
    }
	loadAllDoanhThu(){
		this.loadDoanhthu(this.TYPES.TONG);
		this.loadDoanhthu(this.TYPES.SANPHAM);
		this.loadDoanhthu(this.TYPES.NHOM);
	}
	beforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
	onBeforeUpload(event, name){
        this.isLoading = true;
    }
	onUpload(event) {
		this.showMes('success', 'Up load file thành công');
		this.loadAllDoanhThu();
		this.isLoading = false;
		this.displayFormUploadSieuthi = false;
    }
	exportFile(){
		let sql = {
			"sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where CAMPAIGN_REQUEST_ID = "+this.id
		}
		
		localStorage.setItem('api', this.api+'domain/get-sql-result');
		localStorage.setItem('query', JSON.stringify(sql));
		window.open(this.host+'/baocao/export_doanh_thu.html');
	}
	exportFileNganSach(){
		localStorage.setItem('api', this.api+'requestcampaign/get-media-campaign-shop-by-id?id='+this.id);
		window.open(this.host+'/baocao/export_ngan_sach.html');
	}
	onUploadNgansach(event){
		this.loadShopInRequest();
		this.isLoading= false;
		this.displayFormUploadNgansach = false;
	}
	onSelect(event, name){
        if (event.files[0].name != name){
			this.upfile.clear();
			this.upfile1.clear();
            this.showMes('error', 'File cần có tên là '+ name);

        }
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 4000);
	}
}
