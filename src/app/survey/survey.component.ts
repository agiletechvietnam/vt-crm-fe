import { Component, OnInit } from '@angular/core';
import {SurveyService} from  '../shared/services/survey.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
	selector: 'app-survey',
	templateUrl: './survey.component.html',
	styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
	private items: any[] = [];
	private mes: Message[];

	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
	private user;
	constructor(
		private fb: FormBuilder,
		private service: SurveyService
	) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
		this.loadItems();
	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	loadItems(){
		this.isLoading = true;
		this.service.findSurveys().subscribe(res=> {
			this.items = res;
			this.isLoading = false;
		}, error => {});
	}

	create(){
		this.form.reset();
		this.title = "Thêm survey";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa survey";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['description'].setValue(item.description, { onlySelf: true });
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delSurvey(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			this.service.addSurvey(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{

			this.service.updateSurvey(value, value.id).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
