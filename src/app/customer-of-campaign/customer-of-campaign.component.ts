import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {CustomerService} from "../shared/services/customer.service";

@Component({
    selector: 'app-customer-of-campaign',
    templateUrl: './customer-of-campaign.component.html',
    styleUrls: ['./customer-of-campaign.component.css']
})
export class CustomerOfCampaignComponent implements OnInit {
    private items: any[] = [];
    private user;
    private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    public campaignId;
    public shopId;
    public displayCustomer:boolean = false;
    public hasSurvey:boolean = false;
    private customer: any = {};
    private transactions: any[] = [];
    private warrantys: any[] = [];
	private transaction_details: any[] = [];
    private displayFormTD: boolean = false;
    constructor(
        private service: CampaignService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private customerService: CustomerService
    ) { }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['campaignId'];
            this.shopId = params['shopId'];
            this.loadItems();
        });
    }
    loadItems() {
        this.isLoading = true;
        this.service.findCampaignCustomerByCampaignShopId(this.campaignId, this.shopId).subscribe(
            res => {
                this.items = res;
                if (this.items[0].surveyId != null) this.hasSurvey = true;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    loadCustomer(phone){
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }
    view(id){
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[3]);
        c.style.width = "700px";
		c.style.height = "500px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }

}
