import {Routes} from "@angular/router";
import {CustomerComponent} from "./customer/customer.component";
import {CustomerStoreComponent} from "./customer-store/customer-store.component";
import {CustomerDetailComponent} from "./customer-detail/customer-detail.component";
import {CustomerHangComponent} from "./customer-hang/customer-hang.component";
import {TargetComponent} from "./target/target.component";
import {DepartmentComponent} from "./department/department.component";
import {StaffComponent} from "./staff/staff.component";
import {CityComponent} from "./city/city.component";
import {CampaignManagerStoreviewComponent} from "./campaign-manager-storeview/campaign-manager-storeview.component";
import {StoreComponent} from "./store/store.component";
import {AreaManagerComponent} from "./area-manager/area-manager.component";
import {ProgramStoreComponent} from "./program-store/program-store.component";
import {ProgramStoreNewComponent} from "./program-store-new/program-store-new.component";
import {CampaignComponent} from "./campaign/campaign.component";
import {CampaignCenterComponent} from "./campaign-center/campaign-center.component";
import {CampaignReportComponent} from "./campaign-report/campaign-report.component";
import {MediaChannelComponent} from "./media-channel/media-channel.component";
import {AddShopTargetComponent} from "./add-shop-target/add-shop-target.component";
import {ViewShopTargetComponent} from "./view-shop-target/view-shop-target.component";
import {CommunicationProgramComponent} from "./communication-program/communication-program.component";
import {AddShopCampaignComponent} from "./add-shop-campaign/add-shop-campaign.component";
import {AddCampaignComponent} from "./add-campaign/add-campaign.component";
import {AddCampaignFileComponent} from "./add-campaign-file/add-campaign-file.component";
import {ViewCampaignComponent} from "./view-campaign/view-campaign.component";
import {PopulationComponent} from "./population/population.component";
import {TargetResultComponent} from "./target-result/target-result.component";
import {CommunicationProgramDetailComponent} from "./communication-program-detail/communication-program-detail.component";
import {SurveyComponent} from "./survey/survey.component";
import {SurveyDetailComponent} from "./survey-detail/survey-detail.component";
import {SurveyAddComponent} from "./survey-add/survey-add.component";
import {FillSurveyComponent} from "./fill-survey/fill-survey.component";
import {UpSurveyComponent} from "./up-survey/up-survey.component";
import {QuestionComponent} from "./question/question.component";
import {AnswerComponent} from "./answer/answer.component";
import {RequestMediaCampaignComponent} from "./request-media-campaign/request-media-campaign.component";
import {RequestDetailComponent} from "./request-detail/request-detail.component";
import {AddMediaChannelComponent} from "./add-media-channel/add-media-channel.component";
import {PriceMediaChannelComponent} from "./price-media-channel/price-media-channel.component";
import {AddRequestMediaCampaignComponent} from "./add-request-media-campaign/add-request-media-campaign.component";
import {AddRequestMediaCampaignNewComponent} from "./add-request-media-campaign-new/add-request-media-campaign-new.component";
import {TargetTtComponent} from "./target-tt/target-tt.component";
import {CampaignStoreComponent} from "./campaign-store/campaign-store.component";
import {CampaignStoreUpdateComponent} from "./campaign-store-update/campaign-store-update.component";
import {StaffStoreComponent} from "./staff-store/staff-store.component";
import {CustomerOfStaffComponent} from "./customer-of-staff/customer-of-staff.component";
import {TargetStoreComponent} from "./target-store/target-store.component";
import {CustomerOfCampaignComponent} from "./customer-of-campaign/customer-of-campaign.component";
import {PopulationGroupComponent} from "./population-group/population-group.component";
import {CampaignAssignComponent} from "./campaign-assign/campaign-assign.component";
import {CampaignAssignAgainComponent} from "./campaign-assign-again/campaign-assign-again.component";
import {CampaignResultComponent} from "./campaign-result/campaign-result.component";
import {CampaignStaffResultComponent} from "./campaign-staff-result/campaign-staff-result.component";
import {CampaignStoreResultComponent} from "./campaign-store-result/campaign-store-result.component";
import {AddProgramComponent} from "./add-program/add-program.component";
import {TargetStoreStaffComponent} from "./target-store-staff/target-store-staff.component";
import {ReceiveComponent} from "./receive/receive.component";
import {CompetitorComponent} from "./competitor/competitor.component";
import {MarketComponent} from "./market/market.component";
import {SchoolComponent} from "./school/school.component";
import {CompanyComponent} from "./company/company.component";
import {AdministrativeComponent} from "./administrative/administrative.component";
import {CampaignStaffComponent} from "./campaign-staff/campaign-staff.component";
import {CampaignStaffCustomerComponent} from "./campaign-staff-customer/campaign-staff-customer.component";
import {TargetStaffComponent} from "./target-staff/target-staff.component";
import {StaffProgramComponent} from "./staff-program/staff-program.component";
import {CampaignStaffCustomerSurveyComponent} from "./campaign-staff-customer-survey/campaign-staff-customer-survey.component";
import {ResultSurveyComponent} from "./result-survey/result-survey.component";
import { StaffNotificationComponent} from "./staff-notification/staff-notification.component";
import { PlanMediaCampaignComponent} from "./plan-media-campaign/plan-media-campaign.component";
import { AddMediaCampaignFromRequestComponent } from './add-media-campaign-from-request/add-media-campaign-from-request.component';
import { StoreMediaCampaignDetailComponent } from './store-media-campaign-detail/store-media-campaign-detail.component';
import { NVTTGuard, TTTGuard, NVSTGuard, TSTGuard, TTTNVTTGuard, TTTTSTGuard, TTTNVTTTSTGuard } from './_guards/index';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AreaShopComponent } from './area-shop/area-shop.component';
export const rootRouterConfig: Routes = [
    {path: '', component: DashboardComponent},
    {path: 'customer', component: CustomerComponent, canActivate: [TTTNVTTGuard]},
    {path: 'customer-detail/:id', component: CustomerDetailComponent},
    {path: 'customer-hang', component: CustomerHangComponent},
    {path: 'target', component: TargetComponent, canActivate: [NVTTGuard]},
    {path: 'target-tt', component: TargetTtComponent, canActivate: [TTTGuard]},
    {path: 'department', component: DepartmentComponent},
    {path: 'staff', component: StaffComponent, canActivate: [TTTNVTTGuard]},
    {path: 'campaign-manager', component: CampaignManagerStoreviewComponent},
    {path: 'store', component: StoreComponent, canActivate: [TTTNVTTGuard]},
    {path: 'city', component: CityComponent, canActivate: [TTTNVTTGuard]},
    {path: 'area-manager', component:  AreaManagerComponent, canActivate: [TSTGuard]},
    {path: 'program-store', component: ProgramStoreComponent, canActivate: [TSTGuard]},
    {path: 'program-store-new', component: ProgramStoreNewComponent, canActivate: [TSTGuard]},
    {path: 'campaign', component: CampaignComponent, canActivate: [TTTGuard]},
    {path: 'campaign-center', component: CampaignCenterComponent, canActivate: [NVTTGuard]},
    {path: 'add-campaign', component: AddCampaignComponent},
    {path: 'add-campaign-file', component: AddCampaignFileComponent},
    {path: 'view-campaign/:id', component: ViewCampaignComponent},
    {path: 'campaign-report', component: CampaignReportComponent},
    {path: 'media-channel', component: MediaChannelComponent, canActivate: [TTTNVTTGuard]},
    {path: 'add-shop-target/:id', component: AddShopTargetComponent},
    {path: 'view-shop-target/:id', component: ViewShopTargetComponent},
    {path: 'communication-program', component: CommunicationProgramComponent, canActivate: [TTTNVTTGuard]},
    {path: 'add-shop-campaign/:id', component: AddShopCampaignComponent},
    {path: 'population', component: PopulationComponent, canActivate: [TTTNVTTGuard]},
    {path: 'campaign-store-result/:id', component: CampaignStoreResultComponent, canActivate: [TTTNVTTTSTGuard]},
    {path: 'target-result/:id', component: TargetResultComponent},
    {path: 'communication-program-detail/:id', component: CommunicationProgramDetailComponent, canActivate: [TTTNVTTGuard]},
    {path: 'survey', component: SurveyComponent, canActivate: [TTTNVTTGuard]},
    {path: 'survey-detail', component: SurveyDetailComponent, canActivate: [TTTNVTTGuard]},
    {path: 'survey-detail/:id', component: SurveyDetailComponent, canActivate: [TTTNVTTGuard]},
    {path: 'survey-add', component: SurveyAddComponent, canActivate: [NVTTGuard]},
    {path: 'fill-survey/:id', component: FillSurveyComponent},
    {path: 'up-survey/:surveyId/:phone/:cusid', component: UpSurveyComponent},
    {path: 'question', component: QuestionComponent, canActivate: [TTTNVTTGuard]},
    {path: 'answer/:id', component: AnswerComponent, canActivate: [TTTNVTTGuard]},
    {path: 'request-media-campaign', component: RequestMediaCampaignComponent, canActivate: [TTTNVTTGuard]},
    {path: 'request-detail/:id', component: RequestDetailComponent},
    {path: 'add-media-channel', component: AddMediaChannelComponent},
    {path: 'price-media-channel/:id', component: PriceMediaChannelComponent},
    {path: 'add-request-media-campaign/:id', component: AddRequestMediaCampaignComponent},
    {path: 'add-request-media-campaign-new/:id', component: AddRequestMediaCampaignNewComponent},
    {path: 'customer-store', component: CustomerStoreComponent, canActivate: [TSTGuard]},
    {path: 'request-detail/:id', component: RequestDetailComponent, canActivate: [TSTGuard]},
    {path: 'campaign-store', component: CampaignStoreComponent},
    {path: 'campaign-store-update', component: CampaignStoreUpdateComponent, canActivate: [TSTGuard]},
    {path: 'staff-store', component: StaffStoreComponent, canActivate: [TTTNVTTTSTGuard ]},
    {path: 'target-store', component: TargetStoreComponent, canActivate: [TSTGuard]},
    {path: 'customer-of-staff', component: CustomerOfStaffComponent},
    {path: 'customer-of-campaign/:campaignId/:shopId', component: CustomerOfCampaignComponent},
    {path: 'population-group/:id', component: PopulationGroupComponent},
    {path: 'campaign-assign/:id/:total/:campaignshopId', component: CampaignAssignComponent},
    {path: 'campaign-assign-again/:id/:total/:campaignshopId', component: CampaignAssignAgainComponent, canActivate: [TSTGuard]},
    {path: 'campaign-result/:id', component: CampaignResultComponent},
    {path: 'campaign-staff-result/:staffid/:campaignid', component: CampaignStaffResultComponent},
    {path: 'add-program/:id', component: AddProgramComponent},
    {path: 'target-store-staff/:id', component: TargetStoreStaffComponent},
    {path: 'receive', component: ReceiveComponent, canActivate: [TSTGuard]},
    {path: 'competitor', component: CompetitorComponent, canActivate: [TSTGuard]},
    {path: 'market', component: MarketComponent, canActivate: [TSTGuard]},
    {path: 'school', component: SchoolComponent, canActivate: [TSTGuard]},
    {path: 'company', component: CompanyComponent, canActivate: [TSTGuard]},
    {path: 'administrative', component: AdministrativeComponent, canActivate: [TSTGuard]},
    {path: 'campaign-staff', component: CampaignStaffComponent},
    {path: 'campaign-staff-customer/:campaignid', component: CampaignStaffCustomerComponent},
    {path: 'campaign-staff-customer-survey/:campaignid', component: CampaignStaffCustomerSurveyComponent},
    {path: 'target-staff', component: TargetStaffComponent},
    {path: 'staff-program', component: StaffProgramComponent},
    {path: 'staff-notification', component: StaffNotificationComponent},
    {path: 'result-survey/:campaignCusId', component: ResultSurveyComponent},
    {path: 'plan-media-campaign/:id', component: PlanMediaCampaignComponent, canActivate: [NVTTGuard]},
    {path: 'add-media-campaign-from-request/:id', component: AddMediaCampaignFromRequestComponent, canActivate: [TSTGuard]},
    {path: 'store-media-campaign-detail/:id', component: StoreMediaCampaignDetailComponent, canActivate: [TSTGuard]},
    {path: 'area-shop/:id', component: AreaShopComponent, canActivate: [TTTNVTTTSTGuard]},
];
