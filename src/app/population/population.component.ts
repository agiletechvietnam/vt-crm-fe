import { Component, OnInit } from '@angular/core';
import {AreaService} from "../shared/services/area.service";
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
	selector: 'app-population',
	templateUrl: './population.component.html',
	styleUrls: ['./population.component.css']
})
export class PopulationComponent implements OnInit {
	private items: any[] = [];
	private isLoading: boolean =  false;
	private mes: Message[];
	private form: FormGroup;
	private title;
	private formCreate;
	private displayForm: boolean = false;
	private totalRecords: number;
    private page:number;
	private search;
	constructor(
		private service: AreaService,
		private fb: FormBuilder,
	) { }

	ngOnInit() {
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl(),
			'population': new FormControl('', Validators.compose([Validators.required])),
		});
		this.loadPopulation();
	}
	loadLazy(event: any) {
        this.isLoading = true;
        this.page  = event.first/event.rows;	
        this.service.findPopulation(this.search, this.page, event.rows).subscribe(
            res => { 
                this.items = res.content;
                this.totalRecords = res.totalElements;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    change(){
        this.search = "";
        this.loadPopulation();
    }
	loadPopulation(){
		this.isLoading = true;
		this.service.findPopulation(this.search, 0, 20).subscribe(res => {
			this.items = res.content;
			this.page = 0;
			this.totalRecords = res.totalElements;            
			this.isLoading = false;
		}, error => {

		})
	}

	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['population'].setValue(item.population, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['name'].setValue(item.name, { onlySelf: true });
	}

	onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			
		}else{

			this.service.updatePopulation(value).subscribe(
				res => {
					this.isLoading = false;
					this.displayForm = false;
					for (let i=0; i<this.items.length; i++)
						if (this.items[i].id == value.id){
							this.items[i].population = value.population;
							break;
						}
					//this.loadPopulation();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
