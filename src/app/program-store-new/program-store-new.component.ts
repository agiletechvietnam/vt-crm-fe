import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { MediaCampaignService } from "../shared/services/mediacampaign.service";
import { SelectItem } from 'primeng/primeng';
import { ChannelService } from "../shared/services/channel.service";
import { StaffService } from "../shared/services/staff.service";
import { Router } from '@angular/router';

@Component({
    selector: 'app-program-store-new',
    templateUrl: './program-store-new.component.html',
    styleUrls: ['./program-store-new.component.css']
})
export class ProgramStoreNewComponent implements OnInit {

    private items: any[];
    private mes: Message[];
    private channels: SelectItem[] = [];
    private form: FormGroup;
    private formCreate: boolean;
    private title: string;
    private displayForm: boolean = false;
    private displayDel: boolean = false;
    private delItem: any;
    private isLoading: boolean = false;
    private shopId;
    constructor(
        private service: MediaCampaignService,
        private fb: FormBuilder,
        private channelService: ChannelService,
        private router: Router

    ) { }

    ngOnInit() {
        this.form = this.fb.group({
            'id': new FormControl(),
            'status': new FormControl(),
            'name': new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(250), this.NoWhitespaceValidator])),
            'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
            'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
            'shopId': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
        });
        var user = JSON.parse(localStorage.getItem("user"));
        this.shopId = user.shopId;
        this.loadItems();
    }
    public NoWhitespaceValidator(control: FormControl) {
        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true }
    }
    loadItems() {
        this.isLoading = true;
        this.service.findMediaCampaignsByShopId(this.shopId).subscribe(
            res => {
                this.isLoading = false;
                this.items = res;
            },
            error => {
                error => console.log(error);
            }
        );
    }
    create() {

        this.form.reset();
        this.title = "Thêm";
        this.formCreate = true;
        this.displayForm = true;

    }

    update(item) {

        this.form.reset();
        this.title = "Sửa";// else this.title = "Xem chi tiết";

        this.formCreate = false;
        this.displayForm = true;
        this.form.controls['name'].setValue(item.name, { onlySelf: true });
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
        this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: true });
        this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: true });
        this.form.controls['status'].setValue(item.status, { onlySelf: true });



    }
    gDate(d: number) {
        let date = new Date(d);
        let m = date.getMonth() + 1;
        return date.getDate() + "/" + m + "/" + date.getFullYear();
    }
    vDate(d) {
        let date = new Date(d);
        return date;
    }
    delete(item) {
        this.delItem = item;
        this.displayDel = true;
    }
    guiDuyet(value) {
        this.service.updateMediaCampaign({ status: 1 }, value.id).subscribe(
            res => {
                this.displayForm = false;
                this.loadItems();
                this.showMes('success', 'Gửi duyệt thành công');
            },
            error => { }
        )
    }
    acceptDel() {
        this.service.delMediaCampaign(this.delItem).subscribe(
            res => {
                this.delItem = null;
                this.displayDel = false;
                this.loadItems();
                this.showMes('warn', 'Xóa thành công');

            },
            error => { }
        );
    }
    onSubmit(value: any) {
        if (this.form.controls['startDate'].value > this.form.controls['endDate'].value){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
			 return;
		}

		if (this.form.controls['endDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày kết thúc phải lớn hơn ngày hiện tại');
			 return;
		} 
        if ((new Date()).toDateString() != this.form.controls['startDate'].value.toDateString() && this.form.controls['startDate'].value < new Date().getTime()){
			 this.showMes('error', 'Ngày bắt đầu chiến dịch phải lớn hơn hoặc bằng ngày hiện tại');
			 return;
		}
		this.isLoading = true;
		value.shopId = this.shopId;
        value.sourceId = 1;
		if (this.formCreate){

			this.service.requestCampaign(value).subscribe(
				res => {
					this.displayForm = false;
					this.showMes('success', 'Thêm thành công');
                    this.addShopToRequest(res.id);
					
				},
				error => {}
			)
		}else{

			// value.startDate = this.vDate(value.startDate);
			// value.endDate = this.vDate(value.endDate);
			// this.service.updateTarget(value, value.id).subscribe(
			// 	res => {
			// 		this.isLoading = false;
			// 		this.displayForm = false;
			// 		this.loadItems();
			// 		this.showMes('warn', 'Sửa thành công');
			// 	},
			// 	error => {}
			// )
		}
    }
    addShopToRequest(id){
        console.log('add shop to request');
        let that = this;
        return new Promise(function(resolve, reject){
            let data = {
                campaignRequestId: id,
                shopIds: [that.shopId],
                budget:0,
                sourceId: 1
            }
            that.channelService.addShopToRequest(data).subscribe(res=> {
                that.router.navigate(['/add-request-media-campaign-new', id]);
                this.isLoading = false;
                resolve();
            }, error => { 
                that.router.navigate(['/add-request-media-campaign-new', id]); 
                reject();
            });
        })
        
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
