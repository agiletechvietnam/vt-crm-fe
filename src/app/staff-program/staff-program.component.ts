import { Component, OnInit } from '@angular/core';
import {MediaCampaignService} from '../shared/services/mediacampaign.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
	selector: 'app-staff-program',
	templateUrl: './staff-program.component.html',
	styleUrls: ['./staff-program.component.css']
})
export class StaffProgramComponent implements OnInit {
	private displayForm: boolean = false;
	private items: any[];
	private mes: Message[];
	private isLoadding: boolean = false;
	private user;
	private form: FormGroup;
	private isLoading: boolean = false;
	constructor(
		private service: MediaCampaignService,
		private fb: FormBuilder
	) { }

	ngOnInit() {
		this.form = this.fb.group({
			'id': new FormControl(),
			'realResult': new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
		});
		this.user = JSON.parse(localStorage.getItem('user'));
		this.loadItems();
	}
	loadItems(){
		this.isLoadding = true;
		this.service.findMediaCampaignsByStaffId(this.user.id).subscribe(res=>{
			this.items = res;
			this.isLoadding = false;
		}, error =>{

		})
	}
	update(item){
		this.form.reset();
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['realResult'].setValue(item.realResult, { onlySelf: true });

	}
	onSubmit(value){
        this.isLoading = true;
        this.service.updateAreaToMedia(value, value.id).subscribe(
            res => {
				this.isLoading = false;
                this.showMes('success', 'Đã cập nhập');
                this.displayForm = false;
				this.loadItems();
            },
            error => { }
        )
    }
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
