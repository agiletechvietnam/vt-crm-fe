import { Component, OnInit } from '@angular/core';
import {ChannelModel} from "../shared/models/channel.model";
import {ChannelService} from "../shared/services/channel.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
    selector: 'app-add-media-channel',
    templateUrl: './add-media-channel.component.html',
    styleUrls: ['./add-media-channel.component.css']
})
export class AddMediaChannelComponent implements OnInit {
    private items: ChannelModel[];
	private mes: Message[];

	private form: FormGroup;
	private formCreate: boolean;

    constructor(
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
    }

}
