import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
    selector: 'app-survey-add',
    templateUrl: './survey-add.component.html',
    styleUrls: ['./survey-add.component.css']
})
export class SurveyAddComponent implements OnInit {
    private form: FormGroup;
    constructor(
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'description': new FormControl('', Validators.compose([Validators.required])),
		});
    }

}
