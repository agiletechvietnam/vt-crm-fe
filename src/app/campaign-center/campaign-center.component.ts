import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { CampaignModel } from "../shared/models/campaign.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
	selector: 'app-campaign-center',
	templateUrl: './campaign-center.component.html',
	styleUrls: ['./campaign-center.component.css']
})
export class CampaignCenterComponent implements OnInit {

	private items:any[];
    private mes: Message[];

	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
    
    constructor(
        private service: CampaignService,
        private fb: FormBuilder) { }

    ngOnInit() {
		//so lan mua, so tien mua
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
            'gender': new FormControl(),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'campaignDescription': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
		this.loadItems();
    }
    loadItems(){
        this.service.findCampaigns().subscribe(
            res => {
                this.items = res.content;
            },
            error => { 
                error => console.log(error);
            }
        );
    }
    
    create(){
		this.form.reset();
		this.title = "Thêm chiến dịch";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa chiến dịch";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: true });
		this.form.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: true });
		this.form.controls['campaignDescription'].setValue(item.description, { onlySelf: true });
	}
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.service.delCampaign(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {} 
		);
	}
	onSubmit(value:any){
		if (this.formCreate){
			this.service.addCampaign(value).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
				},
				error => {}
			)
		}else{
			value.startDate = this.vDate(value.startDate);
			value.endDate = this.vDate(value.endDate);
			this.service.updateCampaign(value, value.id).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
				},
				error => {}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
