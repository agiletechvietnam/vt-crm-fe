import { Component, OnInit } from '@angular/core';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {CampaignService} from  '../shared/services/campaign.service';
@Component({
    selector: 'app-campaign-staff',
    templateUrl: './campaign-staff.component.html',
    styleUrls: ['./campaign-staff.component.css']
})
export class CampaignStaffComponent implements OnInit {
    private items: any[] = [];
    private mes: Message[];
    private isLoading: boolean =  false;
    private user;
    constructor(
        private service: CampaignService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));   
        this.loadItems();                
    }
    loadItems(){
        this.isLoading = true;
        this.service.findCampaignByStaffId(this.user.id).subscribe(res => {
            this.items = res;
            this.isLoading = false;
        }, error => {

        })
    }

} 
