import { Component, OnInit } from '@angular/core';
import {TargetModel} from  '../shared/models/target.model';
import {TargetService} from  '../shared/services/target.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {HostConstant} from '../shared/host.constant';
@Component({
    selector: 'app-target-result',
    templateUrl: './target-result.component.html',
    styleUrls: ['./target-result.component.css']
})
export class TargetResultComponent implements OnInit {
    private items: any[];
	private mes: Message[];
    private isLoading: boolean =  false;
	private user;
    public subscription: Subscription;
    private targetId: number;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: TargetService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.targetId = params['id'];
            this.loadItems();
        });
    }

     loadItems(){
        this.isLoading = true;
        this.service.findShopTargetByTarget(this.targetId).subscribe(
			res => {
                this.items =res;
                console.log(res);
                let d = {
                    shopCode: '',
                    shopName: 'Tổng',
                    targetNumber: 0,
                    targetLoyalCusNo: 0,
                    curLoyalCusNo: 0,
                    targetOldCusNo: 0,
                    curOldCusNo: 0,
                    e: 0,
                    ee: 0
                };
                for (let i=0; i<res.length; i++){
                    d.targetNumber += res[i].targetNumber;
                    d.targetLoyalCusNo += res[i].targetLoyalCusNo;
                    d.curLoyalCusNo += res[i].curLoyalCusNo;
                    d.targetOldCusNo += res[i].targetOldCusNo;
                    d.curOldCusNo += res[i].curOldCusNo
                }
                if (d.targetLoyalCusNo != 0)
                    d.e = Math.floor((d.curLoyalCusNo/d.targetLoyalCusNo)*100);
                else d.e = 0;
                if (d.targetOldCusNo != 0)
                    d.ee = Math.floor((d.curOldCusNo/d.targetOldCusNo)*100);
                else d.ee = 0;
                this.items.push(d);
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
    }
    
    baocao(){
        this.isLoading = true;
        this.service.findTargetById(this.targetId).subscribe(res=>{
            localStorage.setItem('target', JSON.stringify(res));
            localStorage.setItem('api', HostConstant.API + 'bdkhshoptarget/get-by-target-id?targetId='+this.targetId);
            window.open(HostConstant.HOST+'/baocao/export_chitieu_tt.html');
            this.isLoading = false;
        }, error => {this.isLoading = false;});
        
           
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
//