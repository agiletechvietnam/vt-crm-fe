import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {CustomerModel} from "../shared/models/customer.model";
import {CustomerService} from "../shared/services/customer.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";
@Component({
    selector: 'app-customer-detail',
    templateUrl: './customer-detail.component.html',
    styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit, OnDestroy {

    public subscription: Subscription;
    private customerModel: CustomerModel;
    private form: FormGroup;
    private genders: SelectItem[];
    private cities: SelectItem[];
    private districts: SelectItem[];
    private wards: SelectItem[];
    private msgs: Message[];
    private isLoading: boolean = false;
    private transactions: any[] = [];
    private warrantys: any[] = [];
    private customer = {};
    private transaction_details: any = [];
    private displayForm: boolean = false;
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private customerService: CustomerService, private fb: FormBuilder, private cityService: CityService,
    ) { }
    
    ngOnInit() {
        this.form = this.fb.group({
            'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
            'gender':  new FormControl('', Validators.required),
            'mobile': new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
            'idNo': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
            'job': new FormControl(''),
            'income': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
            'cityId': new FormControl('', Validators.required),
            'districtId': new FormControl('', Validators.required),
            'payAreaCode': new FormControl('', Validators.required),
        });
        this.genders = [];
        this.genders.push({label:'Giới tính', value: null});
        this.genders.push({label:'Nam', value: 1});
        this.genders.push({label:'Nữ', value: 2});

        this.cities = [];
        this.cities.push({label:'Tỉnh/TP', value:null});

        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});

        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});

        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.customerService.findCustomerById(params['id']).subscribe(
                res => {
                    this.customer = res;
                    this.loadTransactions(res.mobile);
                    this.loadWarrantys(res.mobile); 
                }, 
                error => {
                    error> console.log(error);
                }
            )
        });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    view(id){ 
        
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "700px";
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
                 this.displayForm = true;
            },
            error => {}
        );
    }
    CityDropDownChange(){
        console.log('city dropdown change');
        if (this.form.value.cityId == null) return;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findDistrictsByCityId(this.form.value.cityId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.districts.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    DistrictDropDownChange(){
        console.log('districts dropdown change');
        if (this.form.value.districtId == null) return;
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findWardsByDistrictId(this.form.value.districtId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.wards.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    onSubmit(value:any){
        this.customerService.updateCustomer(value, this.customerModel.id).subscribe(
            res => {
                this.msgs = [];
                this.msgs.push({severity:'success', summary:'Sửa thành công', detail:''});

            },
            error => {
                this.msgs = [];
                this.msgs.push({severity:'error', summary:'Chưa sửa được', detail:''});
                error> console.log(error);
            }
        )
        console.log(value);

    }
    loadTransactions(phone){
        this.isLoading = true;
        this.customerService.findTransactions(phone).subscribe(res => {
            this.transactions = res;
            this.isLoading = false;
        }, error => {})
    }
    loadWarrantys(phone){
        this.isLoading = true;
        this.customerService.findWarrantys(phone).subscribe(res => {
            this.warrantys = res;
            this.isLoading = false;
        }, error => {})

       
    }
}

