import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { CampaignService } from "../shared/services/campaign.service";
import { StaffService } from "../shared/services/staff.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
@Component({
    selector: 'app-campaign-assign',
    templateUrl: './campaign-assign.component.html',
    styleUrls: ['./campaign-assign.component.css']
})
export class CampaignAssignComponent implements OnInit {
    private items: any[] = [];
   
    private staffs: any[] = [];
    private user;
    private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    private campaignId: number;
    private total: number;
    private dagiao: number;
    private form: FormGroup;
    private displayForm:boolean = false;
    private campaignShopId;
    private status = 1;
    private click: boolean = false;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: CampaignService,
        private staffService: StaffService,
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['id'];
            this.total = params['total'];
            this.campaignShopId = params['campaignshopId'];
            this.dagiao = this.total;
            this.check();
            this.loadItems();

        });
        this.form = this.fb.group({
			'id': new FormControl(),
			'customerNumber': new FormControl('', Validators.compose([Validators.required])),
		});

    } 
    check(){
        this.isLoading = true;
        this.service.findCampaignShopById(this.campaignShopId).subscribe(res=> {
            this.status = res.status;
            if (this.status == 2){
                 
            }
        }, error => {});
    }
    loadItems() {
        this.isLoading = true;

        

        this.staffService.findStaffsByShopId2(this.user.shopId).subscribe(
            res => {
                if (this.total > res.length){
                    let d = Math.floor(this.total/res.length);
                    for (let i = 0; i < res.length; i++)
                        this.items.push({ id: res[i].id, staffCode: res[i].staffCode, name: res[i].name, customerNumber: d });
                    
                    let t = this.total - (d * res.length);
                    for (let j=0; j<t; j++)
                        this.items[j].customerNumber += 1;
                }else{
                    for (let j=0; j<res.length; j++)
                        if (j < this.total){
                            this.items.push({ id: res[j].id, staffCode: res[j].staffCode, name: res[j].name, customerNumber: 1 });
                        }else{
                            this.items.push({ id: res[j].id, staffCode: res[j].staffCode, name: res[j].name, customerNumber: 0 });
                        }
                        
                }
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    loadDaphancong(){
        this.isLoading = true;
        this.service.findStaffsCampaignByShopId(this.user.shopId, this.campaignId).subscribe(
        	res => {
                this.items =res;
        		this.isLoading = false;
        	},
        	error => {
        		this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
        	}
        );
    }
    update(item){
        this.displayForm = true;
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
        this.form.controls['customerNumber'].setValue(item.customerNumber, { onlySelf: true });
    }
    onSubmit(value){
        for (let i=0; i<this.items.length; i++){
            if (this.items[i].id == value.id){

                this.items[i].customerNumber = value.customerNumber;
                break;
            }
        }
        this.dagiao = 0;
        for (let i=0; i< this.items.length; i++){
            console.log(i+' '+this.items[i].customerNumber);
            this.dagiao +=parseInt(this.items[i].customerNumber);
        }
        this.displayForm = false;
    }
    assignAuto() {
        this.isLoading = true;
        this.service.assignAuto({ campaignId: this.campaignId, shopId: this.user.shopId, shopCode: this.user.shopCode, shopName: this.user.shopName }).subscribe(res => {
            this.showMes('success', 'Phân công thành công');
            this.isLoading = false;
        }, erorr => {

        })
    }

    assignManual() {
        if (this.total != this.dagiao) {
            this.showMes('error', 'Số khách hàng đã giao phải bằng tổng số khách hàng');
            return;
        }
        if (this.click == true){
            return
        }else this.click = true;
        
        this.isLoading = true;
        let data = {
            campaignId: this.campaignId,
            shopId: this.user.shopId,
            shopCode: this.user.shopCode,
            staffs: this.items,
        }
        this.service.assignManual(data).subscribe(res => {
            this.service.updateCampaign({status: 5}, this.campaignId).subscribe(res => {
            }, error => {});

            this.service.updateCampaignShop({status: 2}, this.campaignShopId).subscribe(res => {
                this.router.navigate(['/campaign-assign', this.campaignId, this.total, this.campaignShopId]);
                this.showMes('success', 'Phân công thành công');
            }, error => {
                this.router.navigate(['/campaign-assign', this.campaignId, this.total, this.campaignShopId]);
                this.showMes('success', 'Phân công thành công');
            });
            
           
            this.isLoading = false;
            //this.loadDaphancong();
        }, erorr => {
        })
    }
    phancong(event){
        
        this.dagiao = 0;
        for (let i=0; i< this.items.length; i++){
            console.log(i+' '+this.items[i].customerNumber);
            this.dagiao +=parseInt(this.items[i].customerNumber);
        }
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
