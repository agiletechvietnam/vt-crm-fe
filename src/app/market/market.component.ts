import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {AreaService} from '../shared/services/area.service';
import {StoreService} from '../shared/services/store.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";

@Component({
    selector: 'app-market',
    templateUrl: './market.component.html',
    styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit {

    private items: any[] = [];
    private mes: Message[];

    private displayForm: boolean = false;
    private formCreate: boolean;
    private displayDel: boolean = false;
    private isLoading: boolean = false;

    private delItem;
    private cities: SelectItem[] = [];
    private districts: SelectItem[] = [];
    private wards: SelectItem[] = [];

    private selectedCity;
    private selectedDistrict;

    private user;
    private form: FormGroup;
    private title;
    private payAreaCode;
    constructor(
        private fb: FormBuilder,
        private service: AreaService,
        private storeService: StoreService,
        private cityService: CityService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'address': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'distance': new FormControl('', Validators.compose([Validators.required])),
			'population': new FormControl('', Validators.compose([Validators.required])),
            'payAreaCode': new FormControl('', Validators.required)
		});
        this.districts = [];
        this.districts.push({ label: 'Quận/Huyện', value: null });
        this.wards = [];
        this.wards.push({ label: 'Phường/Xã', value: null });
        this.loadItems();
    }
    public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
    // cho nayko lay duoc payAreaCode
    loadItems() {
        this.service.findMarketsByShopId(this.user.shopId).subscribe( res => {
            this.items = res;
            this.isLoading = false;
        }, error => {
            this.showMes('error', 'Không thể tải dữ liệu!');
            this.isLoading = false;
        })
    }

    loadCities() {
        this.cities = [];
        this.cities.push({ label: "Tỉnh", value: null });
        this.cityService.findCities().subscribe(
            res => {
                for (let i = 0; i < res.length; i++) {
                    this.cities.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }

    CityDropDownChange() {
        if (this.selectedCity == null) return;
        this.districts = [];
        this.districts.push({ label: 'Quận/Huyện', value: null });
        this.wards = [];
        this.wards.push({ label: 'Phường/Xã', value: null });
        this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findDistrictsByCityId(this.selectedCity).subscribe(
            res => {
                for (let i = 0; i < res.length; i++) {
                    this.districts.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }

    DistrictDropDownChange() {
        if (this.selectedDistrict == null) return;
        this.wards = [];
        this.wards.push({ label: 'Phường/Xã', value: null });
        this.form.controls['payAreaCode'].setValue(null, {onlySelf: false});
        this.cityService.findWardsByDistrictId(this.selectedDistrict).subscribe(
            res => {
                for (let i = 0; i < res.length; i++) {
                    this.wards.push({ label: res[i].name, value: res[i].payAreaCode });
                }
            },
            error => {
                error > console.log(error);
            }
        )
    }


    create() {
        this.displayForm = true;
        this.form.reset();
        this.formCreate = true;
        this.title = "Thêm";
        
        this.selectedCity = this.user.payAreaCode.substr(0, 4);
        this.selectedDistrict = null;
        this.loadCities();
        this.CityDropDownChange();
    }
    update(item) {
        this.form.reset();
        this.title = "Sửa";
        this.formCreate = false;
        this.displayForm = true;
        this.isLoading = true;
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
        this.form.controls['name'].setValue(item.name, { onlySelf: true });
        this.form.controls['address'].setValue(item.address, { onlySelf: true });
        this.form.controls['distance'].setValue(item.distance, { onlySelf: true });
        this.form.controls['payAreaCode'].setValue(item.payAreaCode, { onlySelf: true });
        this.form.controls['population'].setValue(item.population, { onlySelf: true });
        this.selectedCity = item.payAreaCode.substr(0, 4);
		this.selectedDistrict = item.payAreaCode.substr(0, 7);
        this.loadCities();

        this.districts = [];
        this.districts.push({ label: 'Quận/Huyện', value: null });
        if (item.payAreaCode != null) {

            this.selectedCity = item.payAreaCode.substr(0, 4);
            this.selectedDistrict = item.payAreaCode.substr(0, 7);
            this.cityService.findDistrictsByCityId(item.payAreaCode.substr(0, 4)).subscribe(
                res => {
                    for (let i = 0; i < res.length; i++) {
                        this.districts.push({ label: res[i].name, value: res[i].payAreaCode });
                    }
                }, 
                error => {
                    error > console.log(error);
                }
            )

            this.wards = [];
            this.wards.push({ label: 'Phường/Xã', value: null });
            this.cityService.findWardsByDistrictId(item.payAreaCode.substr(0, 7)).subscribe(
                res => {
                    for (let i = 0; i < res.length; i++) {
                        this.wards.push({ label: res[i].name, value: res[i].payAreaCode });
                    }

                },
                error => {
                    error > console.log(error);
                }
            );
        }
        this.isLoading = false;
    }

    onSubmit(value) {
        this.isLoading = true;
        if (this.formCreate) {
            value.type = 2; //market
            value.shopId = this.user.shopId;
            this.service.addArea(value).subscribe(res => {
                this.isLoading = false;
                this.displayForm = false;
                this.showMes('success', 'Thêm thành công');
                this.loadItems();
            }, error => {
                this.isLoading = false;
                this.showMes('error', 'Thêm thất bại');
                this.displayForm = false;
            });
        } else {
            this.service.updateArea(value, value.id).subscribe(res => {
                this.isLoading = false;
                this.displayForm = false;
                this.showMes('success', 'Sửa thành công');
                this.loadItems();
            }, error => {
                this.isLoading = false;
                this.showMes('error', 'Sửa thất bại');
                this.displayForm = false;
            });
        }
    }
    delete(item, type) {
        this.delItem = item;
        this.delItem.typeDel = type;
        this.displayDel = true;
    }
    acceptDel() {
        this.isLoading = true;
        this.service.delArea(this.delItem).subscribe(
            res => {
                this.isLoading = false;
                this.delItem = null;
                this.displayDel = false;
                this.loadItems();
                this.showMes('warn', 'Xóa thành công');
            },
            error => { }
        );

    }


    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
