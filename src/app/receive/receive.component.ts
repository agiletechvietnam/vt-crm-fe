import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {MediaCampaignService} from '../shared/services/mediacampaign.service';
import {RequestModel} from '../shared/models/request.model';
@Component({
    selector: 'app-receive',
    templateUrl: './receive.component.html',
    styleUrls: ['./receive.component.css']
})
export class ReceiveComponent implements OnInit {
    private items: any[] = [];
    private isLoading: boolean =  false;
    private mes: Message[];
    private shopId;
    private form: FormGroup;
    private displayForm: boolean = false;
    private title: string;
    private formCreate: boolean;
    constructor(
        private service: MediaCampaignService,
        private fb: FormBuilder,
        private router: Router
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'status': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(250)])),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'shopId': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});

        let user = JSON.parse(localStorage.getItem('user'));
        this.shopId = user.shopId;
        this.loadItems();
    }
    gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
    create(item){
		item.shopId = this.shopId;
        this.service.addMediaCampaign({name: item.name, startDate: item.startDate, endDate: item.endDate}).subscribe(
            res => {
                this.displayForm = false;
                this.loadItems();
                this.showMes('success', 'Thêm thành công');
                this.isLoading = false;
                this.router.navigate(['/add-program', res.id]);
            },
            error => {
                this.displayForm = false;
                this.showMes('danger', 'Có lỗi sảy ra!');
                this.isLoading = false;
            }
        )
        
		// this.form.reset();
		// this.title = "Thêm";
		// this.formCreate = true;
		// this.displayForm = true;
		// this.form.controls['name'].setValue('Tên ví dụ', { onlySelf: true });
		// this.form.controls['id'].setValue(item.id, { onlySelf: true });
		// this.form.controls['startDate'].setValue(this.gDate(1491452021120), { onlySelf: true });
		// this.form.controls['endDate'].setValue(this.gDate(1491452021120), { onlySelf: true });
		// this.form.controls['status'].setValue(item.status, { onlySelf: true });
	}
    loadItems(){
        this.isLoading = true;
        this.service.findRequestByShopId(this.shopId).subscribe(res=>{
            this.items = res;
            this.isLoading = false;
        }, error => {});
	}
    onSubmit(value:any){
		this.isLoading = true;
		if (this.formCreate){
			
			value.shopId = this.shopId;
			this.service.addMediaCampaign(value).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
					this.isLoading = false;
					this.router.navigate(['/add-program', res.id]);
				},
				error => {
					this.displayForm = false;
					this.showMes('danger', 'Có lỗi sảy ra!');
					this.isLoading = false;
				}
			)
		}else{
			value.startDate = this.vDate(value.startDate);
			value.endDate = this.vDate(value.endDate);
			value.status = 1;
			
			this.service.updateMediaCampaign(value, value.id).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
					this.isLoading = false;
				},
				error => {
					this.displayForm = false;
					this.showMes('success', 'Có lỗi sảy ra!');
					this.isLoading = false;
				}
			)
		}
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
