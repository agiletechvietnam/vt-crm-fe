import { Component, OnInit } from '@angular/core';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import { Router, ActivatedRoute } from '@angular/router';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-store-media-campaign-detail',
    templateUrl: './store-media-campaign-detail.component.html',
    styleUrls: ['./store-media-campaign-detail.component.css']
})
export class StoreMediaCampaignDetailComponent implements OnInit {
    private isLoading: boolean = false;
    public subscription: Subscription;
    public program: any = {};

    private doanhthuNhom;
    private doanhthuTong;
    private doanhthuSp;
    private diaban;
    constructor(
        private service: MediaCampaignService,
        private activatedRoute: ActivatedRoute,
    ) { }
    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.program.id = params['id'];
            let that = this;

            this.loadProgram().then(function(){
                that.loadDoanhthuNhom();
                that.loadDoanhthuSp();
                that.loadDoanhthuTong();
                that.loadDiaban();
            });
        });
        
    }

    loadProgram(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.findMediaCampaignById(that.program.id).subscribe(
                res => {
                    that.program = res;
                    that.isLoading = false;
                    resolve();
                }, 
                error => {
                    error > console.log(error);
                    reject();
                }
            );
        })
        
    }

    loadDoanhthuNhom(){
        let query = {
            "sql": "SELECT * FROM CRM_MEDIA_SHOP_AREA_TARGET WHERE SHOP_ID = "+this.program.shopId+" AND CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" AND GOODS_TYPE = 1",
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuNhom = res;
        }, error => {

        });
    }
    
    loadDoanhthuSp(){
        let query = {
            "sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = 0 and CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" and SHOP_ID = "+this.program.shopId
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuSp = res;
        }, error => {

        });
    }
    
    loadDoanhthuTong(){
        let query = {
            "sql": "SELECT * FROM CRM_MEDIA_SHOP_AREA_TARGET WHERE SHOP_ID = "+this.program.shopId+" AND CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" AND GOODS_TYPE = 2",
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuTong = res;
        }, error => {

        });
    }

    loadDiaban(){
        this.service.findAreaByMediaId(this.program.id).subscribe(
			res => {
				this.diaban = res;
			}, 
			error => {
				error> console.log(error);
			}
		)
	}

}
