import { Component, OnInit } from '@angular/core';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-communication-program-detail',
    templateUrl: './communication-program-detail.component.html',
    styleUrls: ['./communication-program-detail.component.css']
})

export class CommunicationProgramDetailComponent implements OnInit {

    private mes: Message[];
    private items: any[] = [];
    public program: any = {};
    private title;
    public subscription: Subscription;
    private displayForm: boolean = false;
    private form: FormGroup;
    private isLoading: boolean = false;
    private goods: any[] =[];
    constructor(
        private service: MediaCampaignService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.program.id = params['id'];
        });
        this.loadProgram();
        this.loadItems();     
    }

    loadProgram(){
        this.isLoading = true;
        this.service.findMediaCampaignById(this.program.id).subscribe(
            res => {
                this.program = res;
                this.isLoading = false;
                this.loadGoodsOfCampaign();
            }, 
            error => {
                error> console.log(error);
            }
        )
        
    }

    loadGoodsOfCampaign(){
		this.isLoading = true;
		this.service.findGoodsOfCampgin(this.program.shopId, this.program.campaignRequestId).subscribe(res=> {
			this.goods = res;
			this.isLoading = false;
			
		}, error=>{

		});
	}

    loadItems(){
		this.isLoading = true;
		this.service.findAreaByMediaId(this.program.id).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			}, 
			error => {
				error> console.log(error);
			}
		);
	}

    Duyet() {
        this.isLoading = true;
        this.service.updateMediaCampaign({ status: 3}, this.program.id).subscribe(
            res => {
                this.showMes('success', 'Duyệt thành công');
                this.loadProgram();
            },
            error => { }
        );
    }

    showForm(){
        console.log('show form');
        this.displayForm = true;
        this.form.reset();
    }

    onSubmit(value){
        this.isLoading = true;
        this.service.updateMediaCampaign({ status: 4, note: value.note  }, this.program.id).subscribe(
            res => {
                this.showMes('success', 'Đã từ chối');
                this.displayForm = false;
                this.loadProgram();
            },
            error => { }
        );
    }

    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}

}
