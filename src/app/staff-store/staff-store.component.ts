import { Component, OnInit } from '@angular/core';
import {StaffService} from "../shared/services/staff.service";
import {StaffSearchModel} from "../shared/models/staff.search";
import {StaffModel} from "../shared/models/staff.model";
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
@Component({
	selector: 'app-staff-store',
	templateUrl: './staff-store.component.html',
	styleUrls: ['./staff-store.component.css']
})
export class StaffStoreComponent implements OnInit {
	private mes: Message[];
	private staffs: any[] = [];
	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private isLoading: boolean =  false;
    private shopId;
    private genders: SelectItem[];
    private workPoints: SelectItem[] = [];
	private roles: any[] = [];
	private totalRecords: number;
    private page:number;
	private search = "";
    constructor(
        private service: StaffService,
        private fb: FormBuilder
    ) { }
    
    ngOnInit() {
        // M F
        this.form = this.fb.group({
            'staffCode': new FormControl(),
			'staffRole': new FormControl(),
            'id': new FormControl(),
            'shopCode': new FormControl(),
            'mobile': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'name': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'email': new FormControl('', Validators.compose([Validators.required , Validators.minLength(5), Validators.maxLength(200), this.NoWhitespaceValidator])),
            // 'staffRole': new FormControl(),
			
        });
        this.genders = [];
        this.genders.push({label:'Nam', value: 'F'});
        this.genders.push({label:'Nữ', value: 'M'});

		this.roles = [
            {label: 'Vai trò', value: null},
            {label: 'Nhân viên siêu thị', value: 'NVST'},
			{label: 'Khóa', value: 'BLOCK'},
        ];

       
        var user = JSON.parse(localStorage.getItem('user'));
        this.shopId = user.shopId;
        this.loadItems();
    }
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}

	loadLazy(event: any) {
        this.isLoading = true;
        this.page  = event.first/event.rows;	
        this.service.findStaffsByShopId(this.search, this.shopId, this.page, event.rows).subscribe(
            res => {
                this.staffs = res.content;
                this.totalRecords = res.totalElements;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    change(){
        this.search = "";
        this.loadItems();
    }

    loadItems(){
        this.isLoading = true;
        this.service.findStaffsByShopId(this.search, this.shopId, 0, 20).subscribe(
            res => {
                this.staffs = res.content;
               	this.totalRecords = res.totalElements;
                this.isLoading = false;
            },
            error => {
                error => console.log(error);
                this.isLoading = false;
            }
        );
    }

    create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
	}
	
	update(item){
		console.log(item);
        this.form.reset();
		this.displayForm = true;
		this.form.controls['staffCode'].setValue(item.staffCode, { onlySelf: true });
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['email'].setValue(item.email, { onlySelf: true });
        this.form.controls['mobile'].setValue(item.mobile, { onlySelf: true });
        this.form.controls['staffRole'].setValue(item.staffRole, { onlySelf: true });
        this.form.controls['shopCode'].setValue(item.shopCode, { onlySelf: true });
        this.form.controls['name'].setValue(item.name, { onlySelf: true });
    }
    onSubmit(value){
        this.service.updateRole(value, value.id).subscribe(res => {
            this.showMes('success', 'Sửa thành công');
            this.displayForm = false;
            for (let i=0; i<this.staffs.length; i++)
                if (this.staffs[i].staffCode == value.staffCode){
					console.log('tim thay');
                    this.staffs[i].email = value.email;
                    this.staffs[i].staffRole = value.staffRole;
                    this.staffs[i].mobile = value.mobile;
					break;
                } 
            //this.loadItems();
        }, error => {

        })
    }
	gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.isLoading = true;
		this.service.delStaff(this.delItem).subscribe(
			res => {
				this.isLoading = false;
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
    
    
        

}
