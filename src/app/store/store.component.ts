import { Component, OnInit } from '@angular/core';
import {StoreModel}  from '../shared/models/store.model';
import {StoreService} from '../shared/services/store.service';
import {GrowlModule} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";
import {Router} from "@angular/router";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
@Component({
	selector: 'app-store',
	templateUrl: './store.component.html',
	styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

	private items: StoreModel[];


	private storeForm: FormGroup;

	private cities: SelectItem[];
    private districts: SelectItem[];
    private wards: SelectItem[];
    private brands: SelectItem[];
    private isLoading: boolean =false;
	private msgs: Message[];
	private display_add: boolean = false;
	private display_del: boolean = false;
	private delItem:any;
	private title: string;

    private svalue: string = '';
	constructor(
        private service: MediaCampaignService,
		private storeService: StoreService,
		private fb: FormBuilder,
		private cityService: CityService,
		private router: Router) { }

	ngOnInit() {
		this.storeForm = this.fb.group({
            'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(20)])),
            'cityId': new FormControl('', Validators.required),
            'districtId': new FormControl('', Validators.required),
            'wardId': new FormControl('', Validators.required),
        });

        this.brands = [];
        this.brands.push({label: 'Tất cả', value: null});
        this.brands.push({label: 'VTMB', value: 'VTMB'});
        this.brands.push({label: 'VTMN', value: 'VTMN'});

		this.cities = [];
        this.cities.push({label:'Tỉnh/TP', value:null});

        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});

        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
		this.loadStores();
	}
	loadStores(){
        let query = {
            "sql" : "select SHOP_CODE, NAME, ID, (select count(*)  from CRM_BDKH_AREA_SHOP where CRM_BDKH_AREA_SHOP.shop_id = cRM_SHOP.id and CRM_BDKH_AREA_SHOP.SHOP_FRIENDLY = 1  ) Num_area_1, (select count(*)  from CRM_BDKH_AREA_SHOP where CRM_BDKH_AREA_SHOP.shop_id = cRM_SHOP.id and CRM_BDKH_AREA_SHOP.SHOP_FRIENDLY = 2 )  Num_area_2 from cRM_SHOP"
        }
        this.isLoading = true;
        this.service.runSql(query).subscribe(res => {
            this.items = res;
            this.isLoading = false;
        }, error => {})

        // this.isLoading = true;
		// this.storeService.findAllStores().subscribe(
		// 	res => {
		// 		this.items = res;
        //         this.isLoading = false;
        //     },
		// 	error => {
		// 		error => console.log(error);
		// 	}
		// );
	}
    clearFilter(){
        this.svalue = '';
        
    }
	showAdd(){
		this.title = "Thêm cửa hàng";
		this.storeForm.reset();
		this.display_add = true;

		if (this.cities.length == 1){
            this.cityService.findCities().subscribe(
                res => {
                    for (let i=0; i<res.length; i++){
                        this.cities.push({label: res[i].name, value: res[i].id});
                    }
                }, 
                error => {
                    error> console.log(error);
                }
            )
        }
	}
	showDetail(c){
		this.title = "Sửa cửa hàng";
		this.storeForm.reset();
		this.display_add = true;
		if (this.cities.length == 1){
            this.cityService.findCities().subscribe(
                res => {
                    for (let i=0; i<res.length; i++){
                        this.cities.push({label: res[i].name, value: res[i].id});
                    }
                }, 
                error => {
                    error> console.log(error);
                }
            )
        }
    }

	CityDropDownChange(){
        if (this.storeForm.value.cityId == null) return;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findDistrictsByCityId(this.storeForm.value.cityId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.districts.push({label: res[i].name, value: res[i].id});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    DistrictDropDownChange(){
        if (this.storeForm.value.districtId == null) return;
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findWardsByDistrictId(this.storeForm.value.districtId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.wards.push({label: res[i].name, value: res[i].id});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    
    showDel(item){
        this.display_del = true;
        this.delItem = item;
    }
    Del(){
        this.storeService.delStore(this.delItem.id, null).subscribe(
            res => {
                this.display_del = false;
                this.loadStores();
                this.msgs = [];
                this.msgs.push({severity:'warn', summary:'Xóa thành công!', detail:''});
                let t = this;
                setTimeout(function(){
                    t.msgs = [];
                }, 1500);
            },
            error => {
                error> console.log(error);
            }
        )
        
    }
    onSubmit(value:any){
        this.storeService.addStore(value).subscribe(
            res => {
                this.display_add = false;
                this.loadStores();
                this.msgs = [];
                this.msgs.push({severity:'success', summary:'Thêm thành công!', detail:''});
                let t = this;
                setTimeout(function(){
                    t.msgs = [];
                }, 1500);

            },
            error => {
                error> console.log(error);
            }
        )

    }

}
