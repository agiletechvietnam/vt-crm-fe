import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {AreaService} from '../shared/services/area.service';
import {Message} from 'primeng/primeng';
@Component({
	selector: 'app-population-group',
	templateUrl: './population-group.component.html',
	styleUrls: ['./population-group.component.css']
})
export class PopulationGroupComponent implements OnInit {
	private form: FormGroup;
	private mes: Message[];
	private items: any[] = [];
	private isLoading: boolean = false;
	private displayForm: boolean = false;
	private title: string;
	private formCreate: boolean;
	private payAreaCode;
	private displayDel: boolean = false;
	private delItem: any;
	constructor(
		private router: Router, 
		private activatedRoute: ActivatedRoute,
		private fb: FormBuilder,
		private service: AreaService
    ) { }
	create(){
		this.form.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
	}
	ngOnInit() {
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.minLength(3), Validators.maxLength(50), this.NoWhitespaceValidator])),
			'population': new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
			'distance': new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9.]+')])),
			'price': new FormControl('', Validators.compose([ Validators.pattern('[0-9]+')])),
			'mainJob': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(50)]))
		});

		this.activatedRoute.params.subscribe(params => {
			this.payAreaCode = params['id'];
			this.loadItems();
        });
	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	
	update(item){
		this.form.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['population'].setValue(item.population, { onlySelf: true });
		this.form.controls['mainJob'].setValue(item.mainJob, { onlySelf: true });
		this.form.controls['distance'].setValue(item.distance, { onlySelf: true });
		this.form.controls['price'].setValue(item.price, { onlySelf: true });
	}	
	loadItems(){
		this.isLoading = true;
        this.service.findPopgroupByAreaCode(this.payAreaCode).subscribe(
            res => {
                this.items = res;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.service.delPopgroup(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.showMes('warn', 'Xóa thành công');
				
			},
			error => {}
		);
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
	onSubmit(value: any){
		this.isLoading = true;
		if (this.formCreate){
			value.payAreaCode = this.payAreaCode;
			this.service.addPopgroup(value).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Thêm thành công');
					this.isLoading = false;
				},
				error => {
					this.showMes('error', 'Bạn đã thêm tổ dân phố này rồi');
					this.isLoading = false;
				}
			)
		}else{

			this.service.updatePopgroup(value, value.id).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.showMes('success', 'Sửa thành công');
					this.isLoading = false;
				},
				error => {
					this.displayForm = false;
					this.showMes('error', 'Bạn đã thêm tổ dân phố này rồi');
					this.isLoading = false;
				}
			)
		}
	}
}
