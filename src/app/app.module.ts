import {NgModule} from "@angular/core";
import {AlertModule, ModalModule, DatepickerModule, TabsModule} from "ng2-bootstrap";
import {RouterModule} from "@angular/router";
import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {AlertComponent} from "./shared/alert/alert.component";
import {PagerComponent} from "./shared/pager/pager.component";
import {CustomDatepickerComponent} from "./shared/datepicker/custom.datepicker.component";
import {MultiSelectComponent} from "./shared/multiselect/multiselect.component";
import {AlertService} from "./shared/alert/alert.service";
import {CustomerComponent} from "./customer/customer.component";
import {HttpService} from "./shared/http.service";
import {CustomerService} from "./shared/services/customer.service";
import {DashboardService} from "./shared/services/dashboard";
import {NotificationService} from "./shared/services/notification.service";
import {CityService} from "./city/city.component.service";
import {StaffService} from "./shared/services/staff.service";
import {CampaignService} from "./shared/services/campaign.service";
import {DepartmentService} from "./department/department.service";
import {StoreService} from "./shared/services/store.service";
import {MediaCampaignService} from "./shared/services/mediacampaign.service";
import {ChannelService} from "./shared/services/channel.service";
import {AppMenuComponent,AppSubMenu} from './menu.component';
import {TargetService} from './shared/services/target.service';
import {AreaService} from './shared/services/area.service';
import {SurveyService} from './shared/services/survey.service';
import {GoodsService} from './shared/services/goods.service';
// import {AccordionModule} from 'primeng/primeng';
// import {AutoCompleteModule} from 'primeng/primeng';
// import {BreadcrumbModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
// import {CarouselModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
// import {ChipsModule} from 'primeng/primeng';
// import {CodeHighlighterModule} from 'primeng/primeng';
// import {ConfirmDialogModule} from 'primeng/primeng';
// import {SharedModule} from 'primeng/primeng';
// import {ContextMenuModule} from 'primeng/primeng';
// import {DataGridModule} from 'primeng/primeng';
// import {DataListModule} from 'primeng/primeng';
// import {DataScrollerModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
// import {DragDropModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
// import {EditorModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
// import {GalleriaModule} from 'primeng/primeng';
// import {GMapModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
// import {InputMaskModule} from 'primeng/primeng';
// import {InputSwitchModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
// import {LightboxModule} from 'primeng/primeng';
// import {ListboxModule} from 'primeng/primeng';
// import {MegaMenuModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
// import {OrderListModule} from 'primeng/primeng';
// import {OverlayPanelModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {PanelMenuModule} from 'primeng/primeng';
// import {PasswordModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {ProgressBarModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';
// import {RatingModule} from 'primeng/primeng';
// import {ScheduleModule} from 'primeng/primeng';
// import {SelectButtonModule} from 'primeng/primeng';
// import {SlideMenuModule} from 'primeng/primeng';
// import {SliderModule} from 'primeng/primeng';
// import {SpinnerModule} from 'primeng/primeng';
// import {SplitButtonModule} from 'primeng/primeng';
// import {StepsModule} from 'primeng/primeng';
import {TabMenuModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';
// import {TerminalModule} from 'primeng/primeng';
// import {TieredMenuModule} from 'primeng/primeng';
// import {ToggleButtonModule} from 'primeng/primeng';
// import {ToolbarModule} from 'primeng/primeng';
// import {TooltipModule} from 'primeng/primeng';
// import {TreeModule} from 'primeng/primeng';
// import {TreeTableModule} from 'primeng/primeng';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerHangComponent } from './customer-hang/customer-hang.component';
import { CustomerAccComponent } from './customer-acc/customer-acc.component';

import { DepartmentComponent } from './department/department.component';
import { StaffComponent } from './staff/staff.component';
import { CityComponent } from './city/city.component';
//pipe
import {Status} from './shared/pipe/status.pipe';
import {Round} from './shared/pipe/round.pipe';
import { StoreComponent } from './store/store.component';
import { TargetComponent } from './target/target.component';
import { CampaignManagerStoreviewComponent } from './campaign-manager-storeview/campaign-manager-storeview.component';
import { AreaManagerComponent } from './area-manager/area-manager.component';
import { ProgramStoreComponent } from './program-store/program-store.component';

import { CampaignComponent } from './campaign/campaign.component';
import { CampaignStoreComponent } from './campaign-store/campaign-store.component';
import { CampaignStoreUpdateComponent } from './campaign-store-update/campaign-store-update.component';
import { CampaignReportComponent } from './campaign-report/campaign-report.component';
import { CampaignProgressComponent } from './campaign-progress/campaign-progress.component';
import { TargetStoreComponent } from './target-store/target-store.component';
import { CampaignCenterComponent } from './campaign-center/campaign-center.component';
import { CustomerOfStaffComponent } from './customer-of-staff/customer-of-staff.component';
import { CustomerOfCampaignComponent } from './customer-of-campaign/customer-of-campaign.component';
import { CampaignStaffComponent } from './campaign-staff/campaign-staff.component';
import { CampaignStaffCustomerComponent } from './campaign-staff-customer/campaign-staff-customer.component';
import { StaffStoreComponent } from './staff-store/staff-store.component';
import { MediaChannelComponent } from './media-channel/media-channel.component';
import { AddShopTargetComponent } from './add-shop-target/add-shop-target.component';
import { CommunicationProgramComponent } from './communication-program/communication-program.component';
import { AddShopCampaignComponent } from './add-shop-campaign/add-shop-campaign.component';
import { LoadingOverlayComponent } from './loading-overlay/loading-overlay.component';
import { AddCampaignComponent } from './add-campaign/add-campaign.component';
import { ViewCampaignComponent } from './view-campaign/view-campaign.component';
import { TSTGuard, NVSTGuard, TTTGuard, NVTTGuard, TTTNVTTGuard, TTTTSTGuard, TTTNVTTTSTGuard } from './_guards/index';
import { CustomerStoreComponent } from './customer-store/customer-store.component';
import { ViewShopTargetComponent } from './view-shop-target/view-shop-target.component';
import { PopulationGroupComponent } from './population-group/population-group.component';
import { PopulationComponent } from './population/population.component';
import { TargetStaffComponent } from './target-staff/target-staff.component';
import { CampaignAssignComponent } from './campaign-assign/campaign-assign.component';
import { CampaignResultComponent } from './campaign-result/campaign-result.component';
import { CampaignStaffResultComponent } from './campaign-staff-result/campaign-staff-result.component';
import { CampaignStoreResultComponent } from './campaign-store-result/campaign-store-result.component';
import { AddProgramComponent } from './add-program/add-program.component';
import { StaffProgramComponent } from './staff-program/staff-program.component';
import { TargetResultComponent } from './target-result/target-result.component';
import { CommunicationProgramDetailComponent } from './communication-program-detail/communication-program-detail.component';
import { SurveyComponent } from './survey/survey.component';
import { QuestionComponent } from './question/question.component';
import { SurveyDetailComponent } from './survey-detail/survey-detail.component';
import { SurveyAddComponent } from './survey-add/survey-add.component';
import { AnswerComponent } from './answer/answer.component';
import { TargetStoreStaffComponent } from './target-store-staff/target-store-staff.component';
import { AddCampaignFileComponent } from './add-campaign-file/add-campaign-file.component';
import { RequestMediaCampaignComponent } from './request-media-campaign/request-media-campaign.component';
import { ReceiveComponent } from './receive/receive.component';
import { RequestDetailComponent } from './request-detail/request-detail.component';
import { FillSurveyComponent } from './fill-survey/fill-survey.component';
import { UpSurveyComponent } from './up-survey/up-survey.component';
import { CampaignStaffCustomerSurveyComponent } from './campaign-staff-customer-survey/campaign-staff-customer-survey.component';
import { ResultSurveyComponent } from './result-survey/result-survey.component';
import { StaffNotificationComponent } from './staff-notification/staff-notification.component';
import { AddMediaChannelComponent } from './add-media-channel/add-media-channel.component';
import { PriceMediaChannelComponent } from './price-media-channel/price-media-channel.component';
import { AddRequestMediaCampaignComponent } from './add-request-media-campaign/add-request-media-campaign.component';
import { CompetitorComponent } from './competitor/competitor.component';
import { CompanyComponent } from './company/company.component';
import { MarketComponent } from './market/market.component';
import { SchoolComponent } from './school/school.component';
import { AdministrativeComponent } from './administrative/administrative.component';
import { TargetTtComponent } from './target-tt/target-tt.component';
import { ReceiveDetailComponent } from './receive-detail/receive-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddRequestMediaCampaignStoreComponent } from './add-request-media-campaign-store/add-request-media-campaign-store.component';
import { ProgramStoreNewComponent } from './program-store-new/program-store-new.component';
import { AddRequestMediaCampaignNewComponent } from './add-request-media-campaign-new/add-request-media-campaign-new.component';
import { CampaignAssignAgainComponent } from './campaign-assign-again/campaign-assign-again.component';
import { PlanMediaCampaignComponent } from './plan-media-campaign/plan-media-campaign.component';
import { AddMediaCampaignFromRequestComponent } from './add-media-campaign-from-request/add-media-campaign-from-request.component';
import { StoreMediaCampaignDetailComponent } from './store-media-campaign-detail/store-media-campaign-detail.component';
import { AreaShopComponent } from './area-shop/area-shop.component';

@NgModule({
    declarations: [
        // tat ca component minh code
        AppComponent,
        AppMenuComponent,
        AppSubMenu,
        AlertComponent,
        PagerComponent,
        CustomDatepickerComponent,
        MultiSelectComponent,
        CustomerComponent,
        CustomerDetailComponent,
        CustomerHangComponent,
        CustomerAccComponent,
        DepartmentComponent,
        StaffComponent,
        CityComponent,
        Status,
        Round,
        StoreComponent,
        TargetComponent,
        CampaignManagerStoreviewComponent,
        AreaManagerComponent,
        ProgramStoreComponent,
        CampaignComponent,
        CampaignStoreComponent,
        CampaignStoreUpdateComponent,
        CampaignReportComponent,
        CampaignProgressComponent,
        TargetStoreComponent,
        CampaignCenterComponent,
        CustomerOfStaffComponent,
        CustomerOfCampaignComponent,
        CampaignStaffComponent,
        CampaignStaffCustomerComponent,
        StaffStoreComponent,
        MediaChannelComponent,
        AddShopTargetComponent,
        CommunicationProgramComponent,
        AddShopCampaignComponent,
        LoadingOverlayComponent,
        AddCampaignComponent,
        ViewCampaignComponent,
        CustomerStoreComponent,
        ViewShopTargetComponent,
        PopulationGroupComponent,
        PopulationComponent,
        TargetStaffComponent,
        CampaignAssignComponent,
        CampaignResultComponent,
        CampaignStaffResultComponent,
        CampaignStoreResultComponent,
        AddProgramComponent,
        StaffProgramComponent,
        TargetResultComponent,
        CommunicationProgramDetailComponent,
        SurveyComponent,
        QuestionComponent,
        SurveyDetailComponent,
        SurveyAddComponent,
        AnswerComponent,
        TargetStoreStaffComponent,
        AddCampaignFileComponent,
        RequestMediaCampaignComponent,
        ReceiveComponent,
        RequestDetailComponent,
        FillSurveyComponent,
        UpSurveyComponent,
        CampaignStaffCustomerSurveyComponent,
        ResultSurveyComponent,
        StaffNotificationComponent,
        AddMediaChannelComponent,
        PriceMediaChannelComponent,
        AddRequestMediaCampaignComponent,
        CompetitorComponent,
        CompanyComponent,
        MarketComponent,
        SchoolComponent,
        AdministrativeComponent,
        TargetTtComponent,
        ReceiveDetailComponent,
        DashboardComponent,
        AddRequestMediaCampaignStoreComponent,
        ProgramStoreNewComponent,
        AddRequestMediaCampaignNewComponent,
        CampaignAssignAgainComponent,
        PlanMediaCampaignComponent,
        AddMediaCampaignFromRequestComponent,
        StoreMediaCampaignDetailComponent,
        AreaShopComponent

        
    ],
    imports: [
        // thu vien ben ngoai
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(rootRouterConfig, {useHash: true}),

        ModalModule.forRoot(),
        AlertModule.forRoot(),
        TabsModule.forRoot(),
        DatepickerModule.forRoot(),

        ModalModule.forRoot(),
        AlertModule.forRoot(),

        // AccordionModule,
        // AutoCompleteModule,
        // BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        // CarouselModule,
        ChartModule,
        CheckboxModule,
        // ChipsModule,
        // CodeHighlighterModule,
        // ConfirmDialogModule,
        // SharedModule,
        // ContextMenuModule,
        // DataGridModule,
        // DataListModule,
        // DataScrollerModule,
        DataTableModule,
        DialogModule,
        // DragDropModule,
        DropdownModule,
        // EditorModule,
        FieldsetModule,
        FileUploadModule,
        // GalleriaModule,
        // GMapModule,
        GrowlModule,
        // InputMaskModule,
        // InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        // LightboxModule,
        // ListboxModule,
        // MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessagesModule,
        MultiSelectModule,
        // OrderListModule,
        // OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        // PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        // RatingModule,
        // ScheduleModule,
        // SelectButtonModule,
        // SlideMenuModule,
        // SliderModule,
        // SpinnerModule,
        // SplitButtonModule,
        // StepsModule,
        TabMenuModule,
        TabViewModule,
        // TerminalModule,
        // TieredMenuModule,
        // ToggleButtonModule,
        // ToolbarModule,
        // TooltipModule,
        // TreeModule,
        // TreeTableModule
    ],
    providers: [
        // khai bao service
        NVSTGuard,
        TTTGuard,
        NVTTGuard,
        TSTGuard,
        TTTNVTTGuard,
        TTTTSTGuard,
        TTTNVTTTSTGuard,
        
        AlertService,
        HttpService,
        CustomerService,
        CityService,
        StaffService,
        CampaignService,
        DepartmentService,
        StoreService,
        TargetService,
        ChannelService,
        MediaCampaignService,
        AreaService,
        SurveyService,
        GoodsService,
        NotificationService,
        DashboardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}
