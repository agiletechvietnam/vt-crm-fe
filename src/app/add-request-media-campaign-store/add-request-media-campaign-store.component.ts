import { Component, OnInit } from '@angular/core';
import { ChannelModel } from "../shared/models/channel.model";
import { ChannelService } from "../shared/services/channel.service";
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { AreaService } from '../shared/services/area.service';
import { GoodsService } from '../shared/services/goods.service';
import { SelectItem } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { HostConstant } from "../shared/host.constant";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-add-request-media-campaign-store',
    templateUrl: './add-request-media-campaign-store.component.html',
    styleUrls: ['./add-request-media-campaign-store.component.css']
})
export class AddRequestMediaCampaignStoreComponent implements OnInit {

    public subscription: Subscription;
    private items: ChannelModel[];
    private mes: Message[];

    private form: FormGroup;
    private formShop: FormGroup;
    private formDoanhthu: FormGroup;
    private formNgansach: FormGroup;
    private formCreate: boolean;
    private displayFormGoods: boolean = false;
    private displayFormDoanhthu: boolean = false;
    private title;
    private channels: any[] = [];

    private areas: SelectItem[] = [];
    private provinces: SelectItem[] = [];
    private shops: SelectItem[] = [];
    private isLoading: boolean = false;

    private selectedAreas;
    private selectedProvinces;
    private selectedShops;
    private channelsInRequest;
    private id: number;
    private selectedChannel: any = {};
    private selectedGoods: any = [];
    private goodsInRequest;
    private shopsInRequest;
    private doanhthu;
    private api = HostConstant.API;
    private host = HostConstant.HOST;
    private user;

    private domains: SelectItem[] = [];
    private goodsGroups: SelectItem[] = [];
    private goods: SelectItem[] = [];
    private displayFormUploadNgansach: boolean = false;
    private displayFormUploadSieuthi: boolean = false;
    private displayUpdateNgansach: boolean = false;
    constructor(
        private fb: FormBuilder,
        private channelService: ChannelService,
        private goodsService: GoodsService,
        private areaService: AreaService,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.form = this.fb.group({
            'selectedDomains': new FormControl(),
            'selectedGoodsGroups': new FormControl(),
            'selectedGoods': new FormControl('', Validators.compose([Validators.required])),
        });

        this.formShop = this.fb.group({
            'selectedShops': new FormControl('', Validators.compose([Validators.required])),
        });

        this.formDoanhthu = this.fb.group({
            'id': new FormControl(''),
            'quantity': new FormControl('', Validators.compose([Validators.required])),
        });
        this.formNgansach = this.fb.group({
            'id': new FormControl(''),
            'budget': new FormControl('', Validators.compose([Validators.required])),
        });
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        this.loadPreChannel();
        this.loadGoodsInRequest();
        this.loadShopInRequest();
        this.loadDoanhthu();


    }
    // channel;
    loadPreChannel() {
        let that = this;
        Promise.all([this.loadChannels(), this.loadChannelsInRequest()]).then(function () {
            for (let i = 0; i < that.channelsInRequest.length; i++)
                for (let j = 1; j < that.channels.length; j++) {
                    if (that.channels[j].value.id == that.channelsInRequest[i].mediaId) {
                        that.channels.splice(j, 1);
                    }
                }
        })
        this.loadAreas();
    }
    loadChannels() {
        let that = this;
        return new Promise(function (resolve, rej) {
            that.channels = [];
            that.channelService.findChannels().subscribe(
                res => {
                    that.channels.push({ label: "Hình thức", value: null });
                    for (let i = 0; i < res.length; i++) {
                        that.channels.push({ label: res[i].name, value: res[i] });
                    }
                    resolve();
                },
                error => {
                    error => console.log(error);
                    rej();
                }
            );
        })

    }
    loadChannelsInRequest() {
        let that = this;
        return new Promise(function (resolve, rej) {
            that.channelService.findChannelsInRequest(that.id).subscribe(
                res => {
                    that.channelsInRequest = res;
                    resolve();
                },
                error => {
                    rej();
                }
            )
        })

    }
    addChannel() {
        if (this.selectedChannel == null) {
            this.showMes('error', 'Bạn cần chọn hình thức');
            return;
        }
        this.isLoading = false;
        for (let i = 0; i < this.channelsInRequest.length; i++)
            if (this.selectedChannel.id == this.channelsInRequest[i].mediaId) {
                this.showMes('error', 'Bạn đã thêm hình thức này rồi');
                return;
            }
        this.channelService.addChannelToRequest({
            campaignRequestId: this.id,
            mediaId: this.selectedChannel.id,
            name: this.selectedChannel.name
        }).subscribe(res => {
            this.loadPreChannel();
            this.showMes('success', 'Thêm thành công');
            this.isLoading = false;
            this.selectedChannel = null;
        }, error => {
            this.showMes('error', 'Thêm thất bại');
            this.isLoading = false;
        })
    }
    deleteChannel(cus) {
        this.isLoading = true;
        this.channelService.deleteChannelInRequest(cus.id).subscribe(res => {
            this.loadPreChannel();
            this.showMes('success', 'Xóa thành công');
            this.isLoading = false;
        }, error => {
            this.showMes('error', 'Xóa thất bại');
            this.isLoading = false;
        })
    }
    deleteGoods(cus) {
        this.isLoading = true;
        this.goodsService.deleteGoodToCampaign(cus.id).subscribe(res => {
            this.loadGoodsInRequest();
            this.showMes('success', 'Xóa thành công');
            this.isLoading = false;
        }, error => {
            this.showMes('error', 'Xóa thất bại');
            this.isLoading = false;
        })
    }
    //end channel;

    //good
    //load ngành hàng
    loadProductType() {
        this.isLoading = true;
        this.goodsService.findProductType().subscribe(res => {
            this.isLoading = false;
            this.domains.push({ label: "Chọn ngành hàng", value: null });
            for (let i = 0; i < res.length; i++) {
                this.domains.push({ label: res[i].name, value: res[i] });
            }
        }, error => {

        });
    }

    //change product type thay doi nhom group
    ProductTypeChange() {
        // if (this.form.value.selectedDomains.length == 0) return;

        if (this.form.value.selectedDomains == null) return;
        this.isLoading = true;

        this.goodsService.findGoodsgroupByProductType(this.form.value.selectedDomains.code).subscribe(res => {
            this.form.value.selectedGoodsGroups = [];
            this.form.value.selectedGoods = [];
            this.goodsGroups = [];
            this.goodsGroups.push({ label: "Chọn nhóm hàng", value: null });
            for (let i = 0; i < res.length; i++) {
                this.goodsGroups.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });
    }

    // change goodsGroups 
    GoodGroupChange() {
        // if (this.form.value.selectedGoodsGroups.length == 0) return;


        if (this.form.value.selectedGoodsGroups == null) return;
        this.isLoading = true;
        this.goodsService.findGoodByGroupId(this.form.value.selectedGoodsGroups.id).subscribe(res => {
            this.form.value.selectedGoods = [];
            this.goods = [];
            this.goods.push({ label: "Chọn mặt hàng", value: null });
            for (let i = 0; i < res.length; i++) {
                this.goods.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });
    }


    //chon shop
    initSelect(e, res) {

        for (let i = 0; i < res.length; i++)
            this.areas.push({ label: res[i].name, value: res[i] });
    }
    loadAreas() {
        this.areas.push({ label: "Chọn vùng", value: null });
        this.provinces.push({ label: "Tỉnh", value: null });
        this.shops.push({ label: "Siêu thị", value: null });
        this.areaService.findAreas().subscribe(res => {
            this.initSelect(this.areas, res);
        }, error => { });
    }
    AreaChange() {
        if (this.selectedAreas == null) {
            this.selectedProvinces = null;
            this.selectedShops = null;
            return;
        }
        this.isLoading = true;


        this.areaService.findCityByCenterCodeIn(this.selectedAreas.code).subscribe(res => {
            this.selectedProvinces = null;
            this.selectedShops = null;
            this.provinces = [];
            this.provinces.push({ label: "Tỉnh", value: null });
            for (let i = 0; i < res.length; i++) {
                this.provinces.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });
    }
    ProvinceChange() {
        if (this.selectedProvinces == null) return;
        this.isLoading = true;


        this.areaService.findShopByProvinceIn(this.selectedProvinces.payAreaCode).subscribe(res => {
            this.selectedShops = [];
            this.shops = [];
            this.shops.push({ label: "Siêu thị", value: null });
            for (let i = 0; i < res.length; i++) {
                this.shops.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });

    }
    //end chon shop

    // good
    loadGoodsInRequest() {
        this.goodsService.findGoodsInRequest(this.id).subscribe(res => {
            this.goodsInRequest = res;
        }, error => {

        });
    }
    addGoods() {
        this.title = "Thêm";
        this.displayFormGoods = true;
        this.form.reset();
        this.loadProductType();
        this.goods = [];
        this.goods.push({ label: "Chọn mặt hàng", value: null });
        this.goodsGroups = [];
        this.goodsGroups.push({ label: "Chọn nhóm hàng", value: null });
    }
    onSubmit(value) {
        this.isLoading = true;

        let data = {
            campaignRequestId: this.id,
            goodsName: value.selectedGoods.name,
            goodsIds: value.selectedGoods.id,
            goodsCode: value.selectedGoods.goodsCode
        }
        this.goodsService.addGoodToCampaign(data).subscribe(res => {
            this.showMes('success', 'Thêm mặt hàng thành công');
            this.loadGoodsInRequest();
            this.isLoading = false;
            this.displayFormGoods = false;
            this.selectedGoods = [];
        }, error => {
            this.isLoading = false;
            this.showMes('error', 'Thêm mặt hàng thất bại');
        });
    }
    // end good
    //shop
    addShop(value) {
        this.isLoading = true;

        let data = {
            campaignRequestId: this.id,
            shopId: value.selectedShops.id,
            budget: 0
        }
        this.channelService.addShopToRequest(data).subscribe(res => {
            this.showMes('success', 'Thêm mặt hàng thành công');
            this.loadShopInRequest();

            this.isLoading = false;
        }, error => {
            this.isLoading = false;
        });
    }
    loadShopInRequest() {
        this.channelService.findShopInRequest(this.id).subscribe(res => {
            this.shopsInRequest = res;
        }, error => { });
    }
    //end shop
    loadDoanhthu() {
        let that = this;
        return new Promise(function (resolve, reject) {
            that.channelService.findDoanhthu(that.id).subscribe(res => {
                that.doanhthu = res;
                resolve();
            }, error => {
                reject();
            });
        })

    }
    update(item) {
        this.displayUpdateNgansach = true;
        this.formNgansach.reset();
        this.formNgansach.controls['id'].setValue(item.id, { onlySelf: true });
        this.formNgansach.controls['budget'].setValue(item.targetNumber, { onlySelf: true });

    }
    updateDoanhthu(item) {
        this.displayFormDoanhthu = true;
        this.formDoanhthu.reset();
        this.formDoanhthu.controls['id'].setValue(item.id, { onlySelf: true });
        this.formDoanhthu.controls['targetNumber'].setValue(item.targetNumber, { onlySelf: true });

    }
    onSubmitDoanhthu(value) {
        this.isLoading = true;
        this.channelService.updateDoanhthu(value).subscribe(res => {
            this.showMes('success', 'Cập nhật doanh thu thành công');
            this.loadDoanhthu();
            this.displayFormDoanhthu = false;
            this.isLoading = false;
        }, error => {
            this.isLoading = false;
            this.showMes('error', 'Cập nhật doanh thu thất bại');
        });

    }
    onTabChange(event) {
        if (event.index == 3) {
            this.isLoading = true;
            let that = this;
            this.loadDoanhthu().then(function () {
                that.isLoading = false;
            });
        }
    }
    onBeforeUpload(event) {
        this.isLoading = true;
    }
    onUpload(event) {
        this.showMes('success', 'Up load file thành công');

        this.loadDoanhthu();
        this.isLoading = false;
        this.displayFormUploadSieuthi = false;
    }
    exportFile() {
        localStorage.setItem('api', this.api + 'bdkhmediashoptarget/get-by-campaign-request-id?campaignRequestId=' + this.id);
        window.open(this.host + '/baocao/export_doanh_thu.html');
    }
    exportFileNganSach() {
        localStorage.setItem('api', this.api + 'requestcampaign/get-media-campaign-shop-by-id?id=' + this.id);
        window.open(this.host + '/baocao/export_ngan_sach.html');
    }
    onUploadNgansach(event) {
        this.loadShopInRequest();
        this.isLoading = false;
        this.displayFormUploadNgansach = false;
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
    // loadItems(){
    // 	this.service.findChannels().subscribe(
    // 		res => {
    // 			this.items = res;
    // 		},
    // 		error => {
    // 			error => console.log(error);
    // 		}
    // 	);
    // }
}
