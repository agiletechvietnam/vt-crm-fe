/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddRequestMediaCampaignStoreComponent } from './add-request-media-campaign-store.component';

describe('AddRequestMediaCampaignStoreComponent', () => {
  let component: AddRequestMediaCampaignStoreComponent;
  let fixture: ComponentFixture<AddRequestMediaCampaignStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRequestMediaCampaignStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRequestMediaCampaignStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
