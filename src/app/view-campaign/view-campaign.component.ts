import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { Router, ActivatedRoute } from '@angular/router';
import { CampaignModel } from "../shared/models/campaign.model";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { Subscription } from 'rxjs';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {StoreService} from '../shared/services/store.service';
import {CustomerService} from "../shared/services/customer.service";
@Component({
	selector: 'app-view-campaign',
	templateUrl: './view-campaign.component.html',
	styleUrls: ['./view-campaign.component.css']
})
export class ViewCampaignComponent implements OnInit {
	public subscription: Subscription;
	private items:any[];
    private mes: Message[];
	private shops: SelectItem[] = [];
	private form: FormGroup;
	private formTuchoi: FormGroup;
	private formDuyet: FormGroup;
	private genders: SelectItem[];
	private isLoading: boolean = false;
	private cshops: any[] = [];
	private id: Number = 0;
	private status: Number;
	private customers: any[] = [];
	private displayForm: boolean = false;
	private displayFormDuyet: boolean = false;
	private goods:any[];
	private user;

	public displayCustomer:boolean = false;
	private displayFormTD: boolean = false;
	private customer: any = {};
	private transactions: any[] = [];
    private warrantys: any[] = [];
	private transaction_details: any[] = [];

	constructor(
        private service: CampaignService,
		private storeService: StoreService,
		private activatedRoute: ActivatedRoute,
		private customerService: CustomerService,
        private fb: FormBuilder) { }
	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.genders = [];
		this.genders.push({label:'Giới tính', value: null});
		this.genders.push({label:'Nam', value: 1});
		this.genders.push({label:'Nữ', value: 2});
		this.genders.push({label:'Khác', value: 3});
		this.form = this.fb.group({
			'id': new FormControl(),
			'surveyId': new FormControl(),
			'status': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'code': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'gender': new FormControl(),
			'startDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'endDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'fromTransDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'toTransDate': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])),
			'fromAge': new FormControl(),
			'toAge': new FormControl(),
			'fromTime': new FormControl(),
			'toTime': new FormControl(),
			'fromAmount': new FormControl(),
			'toAmount': new FormControl(),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
			'shops': new FormControl(),
      		'sqlQueryInsert': new FormControl('', Validators.required)
		});
		this.formTuchoi = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(1000)])),
		});
		this.formDuyet = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)])),
		});
		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
			this.isLoading = true;
			let that = this;
			if (this.user.staffRole == 'TTT' || this.user.staffRole == 'NVTT')
				Promise.all([this.loadCampaign(), this.loadShops()]).then(function(){
					that.isLoading = false;
				})
			else
				Promise.all([this.loadCampaign()]).then(function(){
					that.isLoading = false;
				})
			
		});
	}
	gDate(d: number){
		if (d == null ) return null;
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	
	loadCampaign(){
		let that = this;
		return new Promise(function(resolve, reject){
			that.service.findCampaignById(that.id).subscribe(
				res => {
					that.id = res.id;
					that.status = res.status;
					that.form.controls['id'].setValue(res.id, {onlySelf: true});
					that.form.controls['status'].setValue(res.status, {onlySelf: true});
					that.form.controls['code'].setValue(res.code, {onlySelf: true});
					that.form.controls['name'].setValue(res.name, {onlySelf: true});
					that.form.controls['gender'].setValue(res.gender, {onlySelf: true});
					that.form.controls['startDate'].setValue(res.startDate, {onlySelf: true});
					that.form.controls['endDate'].setValue(res.endDate, {onlySelf: true});
					that.form.controls['fromTransDate'].setValue(res.fromTransDate, {onlySelf: true});
					that.form.controls['toTransDate'].setValue(res.toTransDate, {onlySelf: true});
					that.form.controls['fromAge'].setValue(res.fromAge, {onlySelf: true});
					that.form.controls['toAge'].setValue(res.toAge, {onlySelf: true});
					that.form.controls['fromTime'].setValue(res.fromTime, {onlySelf: true});
					that.form.controls['toTime'].setValue(res.toTime, {onlySelf: true});
					that.form.controls['fromAmount'].setValue(res.fromAmount, {onlySelf: true});
					that.form.controls['toAmount'].setValue(res.toAmount, {onlySelf: true});
					that.form.controls['description'].setValue(res.description, {onlySelf: true});
					that.form.controls['shops'].setValue(res.shops, {onlySelf: true});
					that.form.controls['sqlQueryInsert'].setValue(res.sqlQueryInsert, {onlySelf: true});
					that.form.controls['note'].setValue(res.note, {onlySelf: true});
					that.form.controls['surveyId'].setValue(res.surveyId, {onlySelf: true});
					if (res.goods.length != 0)
						that.goods = res.goods;
					else that.goods = null;
					resolve();
				},
				error => {}
			);
		});
		
	}
	loadCustomers(){
		let that = this;
		this.isLoading = true;
		return new Promise(function(resolve, reject){
			that.service.findCustomersByCampaignId(that.id).subscribe(
				res => {
					that.customers = res;
					that.isLoading = false;
					resolve();
				},
				error => {}
			)
		});
		
	}
	loadShops(){
		let that = this;
		return new Promise(function(resolve, reject){
			that.service.findShopCampaignByCampaignId(that.id).subscribe(
				res => {
					that.cshops = res;
					resolve();
				},
				error => {}
			)
		});
	}

	Tuchoi(){
		this.displayForm = true;
		this.displayFormDuyet = false;
	}
	Duyet(){
		this.displayForm = false;
		this.displayFormDuyet = true;	
	}
	onSubmitTuchoi(value){
		this.isLoading = true;
		this.service.updateCampaign({status: 4, note: value.note, createdId: this.user.id}, this.id).subscribe(
			res => {
				this.isLoading = false;
				this.displayForm = false;
				this.showMes('danger','Đã từ chối');
				this.loadCampaign();
			},
			error => {}
		)
	}
	onSubmitDuyet(value){
		this.isLoading = true;
		this.service.verifyCampaign({status: 3, note: value.note, verifiedId: this.user.id }, this.id).subscribe(
			res => {
				this.isLoading = false;
				this.displayFormDuyet = false;
				this.showMes('success','Duyệt thành công');
				this.loadCampaign();
			},
			error => {}
		)
	}
	loadCustomer(phone){
		this.customer= {};
		
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
	viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[2]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }
    view(id){
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[3]);
        c.style.width = "700px";
		c.style.height = "500px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
	

}
