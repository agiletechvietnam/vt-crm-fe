import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import { CampaignModel } from "../shared/models/campaign.model";
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-campaign-store',
  templateUrl: './campaign-store.component.html',
  styleUrls: ['./campaign-store.component.css']
})
export class CampaignStoreComponent implements OnInit {
    //private items: CampaignModel[];
    private items: any[] = [];
    private user;
    private isLoading: boolean =  false;
    private mes: Message[];
    
    constructor(private service: CampaignService) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.loadItems();
    }
    loadItems(){
		this.isLoading = true;
		this.service.findCampaignShopByShopId(this.user.shopId).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
	}
    showDialog() {
        //this.display_add = true;
    }
    viewDetail(c){
        
    }
    reject(c){
       // this.display_reject = true;
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}

}
