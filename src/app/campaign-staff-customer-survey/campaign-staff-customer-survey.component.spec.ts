/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CampaignStaffCustomerSurveyComponent } from './campaign-staff-customer-survey.component';

describe('CampaignStaffCustomerSurveyComponent', () => {
  let component: CampaignStaffCustomerSurveyComponent;
  let fixture: ComponentFixture<CampaignStaffCustomerSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignStaffCustomerSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignStaffCustomerSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
