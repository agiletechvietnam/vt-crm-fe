import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { AreaService } from '../shared/services/area.service';
import { StoreService } from '../shared/services/store.service';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CampaignService } from "../shared/services/campaign.service";

@Component({
    selector: 'app-campaign-staff-customer-survey',
    templateUrl: './campaign-staff-customer-survey.component.html',
    styleUrls: ['./campaign-staff-customer-survey.component.css']
})
export class CampaignStaffCustomerSurveyComponent implements OnInit {

    private formnot: FormGroup;
	private displayFormnot: boolean = false;
	private items: any[] = [];
	public subscription: Subscription;
    private user;
    private campaignId: number;
    private mes: Message[];
    private staffId;
	private lydos: any[] = [];
	private isLoading:boolean = false;
	constructor(
		private fb: FormBuilder,
		private router: Router,
        private activatedRoute: ActivatedRoute,
		private service: CampaignService,
	) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.formnot = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([])),
			'careStatus': new FormControl('', Validators.compose([Validators.required])),
		});
		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['campaignid'];
			this.loadItems();
        });
		this.lydos.push({label: "Chọn nguyên nhân", value: 8});
		this.lydos.push({label: "Tắt máy", value: 8});
		this.lydos.push({label: "Bận không nghe máy", value: 9});
		this.lydos.push({label: "Từ chối chăm sóc", value: 10});
		this.lydos.push({label: "Khác", value: 7});
		
	} 
	loadItems(){
        this.isLoading = true;
        this.service.findResultCustomersCampaignByStaffId(this.user.id, this.campaignId).subscribe(
			res => {
                this.items =res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
	updatenot(item){
		this.formnot.reset();
		this.displayFormnot = true;
		this.formnot.controls['id'].setValue(item.id, { onlySelf: true });
		this.formnot.controls['careStatus'].setValue(8, { onlySelf: true });
		this.formnot.controls['note'].setValue(item.note, { onlySelf: true });
		
	}
	onSubmitnot(value){
		this.isLoading = true;
		this.service.updateCampaignCustomer(value, value.id).subscribe(res => {
			this.isLoading = false;
			this.loadItems();
		}, error => {});
		this.displayFormnot = false;
	}

}