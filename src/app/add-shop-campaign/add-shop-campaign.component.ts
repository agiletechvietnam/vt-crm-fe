import { Component, OnInit } from '@angular/core';
import { MultiSelectModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import {SelectItem} from 'primeng/primeng';
import {StoreService} from '../shared/services/store.service';
import {TargetModel} from  '../shared/models/target.model';
import { Router, ActivatedRoute } from '@angular/router';
import {TargetService} from  '../shared/services/target.service';

@Component({
	selector: 'app-add-shop-campaign',
	templateUrl: './add-shop-campaign.component.html',
	styleUrls: ['./add-shop-campaign.component.css']
})
export class AddShopCampaignComponent implements OnInit {
	public subscription: Subscription;
	private items: any[] = [];
	private shops: SelectItem[] = [];
	private selectedShops: any[] = [];
	private target = new TargetModel();
	constructor(
		private storeService: StoreService,
		private activatedRoute: ActivatedRoute,
		private targetService: TargetService
	) { }

	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.targetService.findTargetById(params['id']).subscribe(
				res => {
					this.target = res;
				},
				error => {}
			)
        });

		this.storeService.findAllStores().subscribe(res => {
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].name, value: res[i]});
			}
		}, error => {

		});
	}
	addShop(){
		// xoa cac shop da selecte
		for (let i=0; i<this.shops.length; i++){
			for (let j=0; j<this.selectedShops.length; j++)
				try{
					if (this.shops[i].value.id == this.selectedShops[j].id){
						this.shops.splice(i, 1);
						
					}
				}catch(e){

				}
		}
		for (var j=0; j<this.selectedShops.length; j++){

			this.items.unshift({shop_name: this.selectedShops[j].name.toString(), target_number: 0, id: this.selectedShops[j].id, ob: this.selectedShops[j]});
		}
		this.selectedShops = [];
		//console.log(this.selectedShops.);

	}
	delete(item){
		for (var i=0; i<this.items.length; i++)
			if (this.items[i].id == item.id){
				
				this.shops.push({label: this.items[i].shop_name, value: this.items[i].ob});
				this.items.splice(i, 1);

				break;
			}
	}
	save(){

	}

}
