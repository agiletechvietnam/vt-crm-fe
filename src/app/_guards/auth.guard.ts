import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {HostConstant} from "../shared/host.constant";
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router) { }
 
    canActivate() {
        if (localStorage.getItem('user') != undefined) {
            return true;
        }
        window.location.href = HostConstant.HOST+'/login.html';
        return false;
    }
}