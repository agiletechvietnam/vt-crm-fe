import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {HostConstant} from "../shared/host.constant";
@Injectable()
export class TSTGuard implements CanActivate {
 
    constructor(private router: Router) { }
 
    canActivate() {
        if (localStorage.getItem('user') != undefined) {
            let user = JSON.parse(localStorage.getItem('user'));
            if (user.staffRole == 'TST')
                return true;
        }
        window.location.href = HostConstant.HOST;
        return false;
    }
}