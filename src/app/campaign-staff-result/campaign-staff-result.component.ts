import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { CampaignService } from "../shared/services/campaign.service";
import { Router, ActivatedRoute } from '@angular/router';
import {CustomerService} from "../shared/services/customer.service";
@Component({
    selector: 'app-campaign-staff-result',
    templateUrl: './campaign-staff-result.component.html',
    styleUrls: ['./campaign-staff-result.component.css']
})
export class CampaignStaffResultComponent implements OnInit {
    private items: any[] = [];
    private isLoading: boolean = false;
    public subscription: Subscription;
    private user;
    private campaignId: number;
    private mes: Message[];
    private staffId;
    public hasSurvey:boolean = false;
    private displayCustomer: boolean = false;
    private customer: any = {};
    private transactions: any[] = [];
    private warrantys: any[] = [];
	private transaction_details: any[] = [];
    private displayFormTD: boolean = false;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: CampaignService,
        private customerService: CustomerService
    ) { }
 
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['campaignid'];
            this.staffId = params['staffid'];
             this.loadItems();
        });
       
    }

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }
    viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}
    loadCustomer(phone){
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    loadItems(){
        this.isLoading = true;
        this.service.findResultCustomersCampaignByStaffId(this.staffId, this.campaignId).subscribe(
			res => {
                this.items =res;
                if (this.items[0].surveyId != null) this.hasSurvey = true;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
    }
    view(id){
        // var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[1]);
        // c.style.width = "700px";
		// c.style.height = "700px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}   
