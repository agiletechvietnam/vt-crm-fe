import { Component, OnInit } from '@angular/core';
import {ChannelModel} from "../shared/models/channel.model";
import {ChannelService} from "../shared/services/channel.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
@Component({
	selector: 'app-media-channel',
	templateUrl: './media-channel.component.html',
	styleUrls: ['./media-channel.component.css']
})
export class MediaChannelComponent implements OnInit {
	private items: ChannelModel[];
	private mes: Message[];
 
	private form: FormGroup;
	private formCreate: boolean;
	private title: string;
	private displayForm: boolean = false;
	private displayDel: boolean = false;
	private delItem: any;
	private user;
	constructor(
		private service: ChannelService,
		private fb: FormBuilder,
		
	) { }

	ngOnInit() {
		this.form = this.fb.group({
			'id': new FormControl(),
			'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(200), this.NoWhitespaceValidator])),
			'price': new FormControl('', Validators.compose([Validators.required])),
			'description': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(1000), this.NoWhitespaceValidator])),
		});
		this.user = JSON.parse(localStorage.getItem('user'));
		console.log(this.user);
		this.loadItems();

	}
	public NoWhitespaceValidator(control: FormControl) {
		let isWhitespace = (control.value || '').trim().length === 0;
		let isValid = !isWhitespace;
		return isValid ? null : { 'whitespace': true }
	}
	loadItems(){
		this.service.findChannels().subscribe(
			res => {
				this.items = res;
			},
			error => {
				error => console.log(error);
			}
		);
	}
	create(){
		this.form.reset();
		this.title = "Thêm kênh";
		this.formCreate = true;
		this.displayForm = true;
	}
	update(item){
		this.form.reset();
		this.title = "Sửa kênh";
		this.formCreate = false;
		this.displayForm = true;

		this.form.controls['name'].setValue(item.name, { onlySelf: true });
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['price'].setValue(item.price, { onlySelf: true });
		this.form.controls['description'].setValue(item.description, { onlySelf: true });
		

	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	acceptDel(){
		this.service.delChannel(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadItems();
				this.mes = [];
				this.mes.push({severity:'warn', summary:'Xóa thành công!', detail:''});
				let t = this;
				setTimeout(function(){
					t.mes = [];
				}, 1500);
				
			},
			error => {}
		);
	}
	onSubmit(value:any){
		if (this.formCreate){
			this.service.addChannel(value).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.mes = [];
					this.mes.push({severity:'success', summary:'Thêm thành công!', detail:''});
					let t = this;
					setTimeout(function(){
						t.mes = [];
					}, 1500);
				},
				error => {}
			)
		}else{
			this.service.updateChannel(value, value.id).subscribe(
				res => {
					this.displayForm = false;
					this.loadItems();
					this.mes = [];
					this.mes.push({severity:'success', summary:'Sửa thành công!', detail:''});
					let t = this;
					setTimeout(function(){
							t.mes = [];
						}, 1500);
					},
				error => {}
			)
		}
	}

}
