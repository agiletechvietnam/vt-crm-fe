import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-hang',
  templateUrl: './customer-hang.component.html',
  styleUrls: ['./customer-hang.component.css']
})
export class CustomerHangComponent implements OnInit {
  private cuss = [];
  constructor() {
    this.cuss = [
      { stt: 1, tenhang: 'Vàng', chitieu: '10000 - 50000'},
      { stt: 2, tenhang: 'Bạc', chitieu: '1000-10000'},
      { stt: 3, tenhang: 'Đồng', chitieu: '100-1000'}

    ];
  }

  ngOnInit() {

  }
  display_add: boolean = false;
  display_edit: boolean = false;
  display_del: boolean = false;
  showAdd() {
    this.display_add = true;
  }
  showEdit() {
    this.display_edit = true;
  }

  del(c) {
    this.display_del = true;
  }
}
