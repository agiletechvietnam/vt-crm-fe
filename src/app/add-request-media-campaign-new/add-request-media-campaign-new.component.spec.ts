/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddRequestMediaCampaignNewComponent } from './add-request-media-campaign-new.component';

describe('AddRequestMediaCampaignNewComponent', () => {
  let component: AddRequestMediaCampaignNewComponent;
  let fixture: ComponentFixture<AddRequestMediaCampaignNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRequestMediaCampaignNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRequestMediaCampaignNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
