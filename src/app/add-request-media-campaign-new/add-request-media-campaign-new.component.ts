import {Component, OnInit, ViewChild} from "@angular/core";
import { ChannelModel } from "../shared/models/channel.model";
import { ChannelService } from "../shared/services/channel.service";
import { MediaCampaignService } from "../shared/services/mediacampaign.service";
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { GrowlModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { AreaService } from '../shared/services/area.service';
import { GoodsService } from '../shared/services/goods.service';
import { SelectItem } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { HostConstant } from "../shared/host.constant";
import { Router, ActivatedRoute } from '@angular/router';
import { FilenameConstant } from "../shared/filename.constant";
import {FileUpload} from 'primeng/primeng';
@Component({
    selector: 'app-add-request-media-campaign-new',
    templateUrl: './add-request-media-campaign-new.component.html',
    styleUrls: ['./add-request-media-campaign-new.component.css']
})
export class AddRequestMediaCampaignNewComponent implements OnInit {
    @ViewChild('upfile') upfile: FileUpload;
    public subscription: Subscription;
    private items: ChannelModel[];
    private mes: Message[];

    private form: FormGroup;
    private formShop: FormGroup;
    private formDoanhthu: FormGroup;
    private formNgansach: FormGroup;
    private formArea: FormGroup;
    private formCreate: boolean;
    private displayFormGoods: boolean = false;
    private displayFormDoanhthu: boolean = false;
    private title;
    private channels: any[] = [];
    private areas: any[] = [];
    private provinces: SelectItem[] = [];
    private shops: SelectItem[] = [];
    private isLoading: boolean = false;
    private selectedAreas;
    private selectedProvinces;
    private selectedShops;
    private channelsInRequest: any[] = [];
    private id: number;
    private selectedChannel: any = {};
    private selectedGoods: any = [];
    private goodsInRequest;
    private shopsInRequest;
    private goodGroupsInRequest;
    private doanhthu;
    private api = HostConstant.API;
    private host = HostConstant.HOST;
    private user;
    private domains: SelectItem[] = [];
    private goodsGroups: SelectItem[] = [];
    private goods: SelectItem[] = [];
    private displayFormUploadNgansach: boolean = false;
    private displayFormUploadSieuthi: boolean = false;
    private displayUpdateNgansach: boolean = false;
    private request: any = {};
    private filename = FilenameConstant;
    private shopId;
    public program: any = {};
    private displayForm: boolean = false;
    private delItem: any;
    private displayDel: boolean = false;
    private diaban;
    private info = {
        TOTAL_COST: 0,
        DELTA_BUDGET: 0,
        EXPECT_PROFIT: 0
    };
    private doanhthuTong;
	private doanhthuNhom;
	private doanhthuSanpham;
    private TYPES = {
		TONG: 2, NHOM: 1, SANPHAM: 0
	};
    constructor(
        private fb: FormBuilder,
        private channelService: ChannelService,
        private mediaService: MediaCampaignService,
        private goodsService: GoodsService,
        private areaService: AreaService,
        private activatedRoute: ActivatedRoute,
         private service: MediaCampaignService,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.shopId = this.user.shopId;
        this.formArea = this.fb.group({
			'id': new FormControl(),
			'areaShopId': new FormControl('', Validators.compose([Validators.required])),
			'startDate': new FormControl('', Validators.compose([Validators.required])),
			'endDate': new FormControl('', Validators.compose([Validators.required])),
			'amount': new FormControl(),
			'description': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});
        this.form = this.fb.group({
			'selectedDomains': new FormControl('', Validators.compose([Validators.required])),
			'selectedGoodsGroups': new FormControl('', Validators.compose([Validators.required])),
			'selectedGoods':  new FormControl(''),
		});

        this.formShop = this.fb.group({
            'selectedAreas': new FormControl(),
            'selectedProvinces': new FormControl(),
            'selectedShops': new FormControl(),
        });

        this.formDoanhthu = this.fb.group({
            'id': new FormControl(''),
            'targetNumber': new FormControl('', Validators.compose([Validators.required])),
        });
        this.formNgansach = this.fb.group({
            'id': new FormControl(''),
            'budget': new FormControl('', Validators.compose([Validators.required])),
        });
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        });
        let that = this;
        this.loadProgram().then(function(){
            that.loadPreChannel();
            that.loadDiaban();
        });
        
        this.loadGoodsInRequest();
        this.loadAllDoanhThu();
        this.loadInfo();
        
        
    }
    loadProgram(){
 
		let sql = {
			"sql":"select * from CRM_BDKH_MEDIA_CAMPAIGN where CAMPAIGN_REQUEST_ID = "+this.id
		};
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.runSql(sql).subscribe(res => {
                that.program = res[0];
                that.isLoading = false;
                resolve();
            }, error => {
                error > console.log(error);
                reject();
            });
        })
	

        
    }
    loadPreChannel() {
        let that = this;
        Promise.all([this.loadChannels(), this.loadChannelsInRequest()]).then(function () {
            for (let i = 0; i < that.channelsInRequest.length; i++)
                for (let j = 1; j < that.channels.length; j++) {
                    if (that.channels[j].value.id == that.channelsInRequest[i].mediaId) {
                        that.channels.splice(j, 1);
                    }
                }
        })
    }
    loadChannels() {
        let that = this;
        return new Promise(function (resolve, rej) {
            that.channels = [];
            that.channelService.findChannels().subscribe(
                res => {
                    that.channels.push({ label: "Kênh", value: null });
                    for (let i = 0; i < res.length; i++) {
                        that.channels.push({ label: res[i].name, value: res[i] });
                    }
                    resolve();
                },
                error => {
                    error => console.log(error);
                    rej();
                }
            );
        })

    }
    loadChannelsInRequest() {
        let that = this;
        return new Promise(function (resolve, rej) {
            that.mediaService.findChannelsOfCampgin(that.shopId, that.id).subscribe(
                res => {
                    that.channelsInRequest = res;
                    for (let i in that.channelsInRequest){
                        that.channelsInRequest[i].amount = 0;
                        that.channelsInRequest[i].quantity = 0;
                    }
                        
                    resolve();
                },
                error => {
                    rej();
                }
            )
        })

    }
    addChannel() {
        if (this.selectedChannel == null) {
            this.showMes('error', 'Bạn cần chọn kênh');
            return;
        }
        this.isLoading = false;
        for (let i = 0; i < this.channelsInRequest.length; i++)
            if (this.selectedChannel.id == this.channelsInRequest[i].mediaId) {
                this.showMes('error', 'Bạn đã thêm kênh này rồi');
                return;
            }
        this.channelService.addChannelToRequest({
            campaignRequestId: this.id,
            mediaId: this.selectedChannel.id,
            name: this.selectedChannel.name
        }).subscribe(res => {
            this.loadPreChannel();
            this.showMes('success', 'Thêm thành công');
            this.isLoading = false;
            this.selectedChannel = null;
        }, error => {
            this.showMes('error', 'Thêm thất bại');
            this.isLoading = false;
        })
    }
    deleteChannel(cus) {
        this.isLoading = true;
        this.channelService.deleteChannelInRequestFromShop(this.id, cus.mediaId).subscribe(res => {
            this.loadPreChannel();
            this.showMes('success', 'Xóa thành công');
            this.isLoading = false;
        }, error => {
            this.showMes('error', 'Xóa thất bại');
            this.isLoading = false;
        })
    }
    deleteGoods(cus) {
        this.isLoading = true;
        this.goodsService.deleteGoodToCampaign(cus.id).subscribe(res => {
            this.loadGoodsInRequest();
            this.showMes('success', 'Xóa thành công');
            this.isLoading = false;
        }, error => {
            this.showMes('error', 'Xóa thất bại');
            this.isLoading = false;
        })
    }

    loadProductType() {

        this.isLoading = true;
        this.goodsService.findProductType().subscribe(res => {
            this.isLoading = false;
            this.domains = [];
            this.domains.push({ label: "Chọn ngành hàng", value: null });
            
            for (let i = 0; i < res.length; i++) {
                this.domains.push({ label: res[i].name, value: res[i] });
            }
        }, error => {

        });
    }

    //change product type thay doi nhom group
    ProductTypeChange() {
        // if (this.form.value.selectedDomains.length == 0) return;

        if (this.form.value.selectedDomains == null) return;
        this.isLoading = true;

        this.goodsService.findGoodsgroupByProductType(this.form.value.selectedDomains.code).subscribe(res => {
            this.form.value.selectedGoodsGroups = [];
            this.form.value.selectedGoods = [];
            this.goodsGroups = [];
            this.goodsGroups.push({ label: "Chọn nhóm hàng", value: null });
            this.form.controls['selectedGoodsGroups'].setValue('', { onlySelf: true });
            this.goods = [];
			this.goods.push({label: "Chọn mặt hàng", value: null});
            for (let i = 0; i < res.length; i++) {
                this.goodsGroups.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });
    }

    // change goodsGroups 
    GoodGroupChange() {
        // if (this.form.value.selectedGoodsGroups.length == 0) return;


        if (this.form.value.selectedGoodsGroups == null) return;
        this.isLoading = true;

        this.goodsService.findGoodByGroupId(this.form.value.selectedGoodsGroups.id).subscribe(res => {
            this.form.value.selectedGoods = [];
            this.goods = [];
            this.goods.push({ label: "Chọn mặt hàng", value: null });
            for (let i = 0; i < res.length; i++) {
                this.goods.push({ label: res[i].name, value: res[i] });
            }
            this.isLoading = false;
        }, error => {

        });
    }

    // good
    loadGoodsInRequest() {
        this.goodsInRequest = [];
		this.goodGroupsInRequest = [];
		this.goodsService.findGoodsInRequest(this.id).subscribe(res=> {
			for(let i in res){
				if (res[i].goodsType == 0)
					this.goodsInRequest.push(res[i]);
				else 
					this.goodGroupsInRequest.push(res[i]);
			}
			
		}, error => {

		});
    }

    addGoods(){
        this.title = "Thêm";
		this.displayFormGoods = true;
		if (this.domains.length == 0){
			this.loadProductType();
			this.goods = [];
			this.goods.push({label: "Chọn mặt hàng", value: null});
			this.goodsGroups = [];
			this.goodsGroups.push({label: "Chọn nhóm hàng", value: null});
		}	
	}
	onSubmit(value){

		let data = {
			campaignRequestId: this.id,
			goodsName: '',
			goodsIds: '',
			goodsCode: '',
			goodsType: 0
		};
		if (value.selectedGoods.goodsCode != null){
			for (let i=0; i < this.goodsInRequest.length && this.goodsInRequest != null; i++){
				if (this.goodsInRequest[i].goodsCode == value.selectedGoods.goodsCode){
					this.showMes('error', 'Bạn đã thêm mặt hàng này rồi');
					return;
				}
			}
			data.goodsName = value.selectedGoods.name;
			data.goodsIds =  value.selectedGoods.id,
			data.goodsCode = value.selectedGoods.goodsCode,
			data.goodsType = 0
		}else{
			for (let i=0; i < this.goodGroupsInRequest.length && this.goodGroupsInRequest != null; i++){
				if (this.goodGroupsInRequest[i].goodsIds == value.selectedGoodsGroups.id){
					this.showMes('error', 'Bạn đã thêm nhóm hàng này rồi');
					return;
				}
			}
			data.goodsName = value.selectedGoodsGroups.name;
			data.goodsIds =  value.selectedGoodsGroups.id,
			data.goodsCode = value.selectedGoodsGroups.goodsCode,
			data.goodsType = 1
		}
			
		this.isLoading = true;
		
		this.goodsService.addGoodToCampaign(data).subscribe(res=> {
			this.showMes('success', 'Thêm thành công');
			this.loadGoodsInRequest();
			this.isLoading = false;
			this.displayFormGoods = false;
			this.selectedGoods = [];
		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Thêm thất bại');
		});
	}
    genSql(type){
        if (type == this.TYPES.SANPHAM)
            return {
                "sql" : "select * from CRM_BDKH_MEDIA_SHOP_TARGET where CAMPAIGN_REQUEST_ID = "+this.id+" and SHOP_ID = "+this.shopId+" and GOODS_TYPE = "+type+"",
            };
        else
            return {
                "sql" : "select * from CRM_MEDIA_SHOP_AREA_TARGET where CAMPAIGN_REQUEST_ID = "+this.id+" and SHOP_ID = "+this.shopId+" and GOODS_TYPE = "+type+"",
            };
        
	}
    loadAllDoanhThu(){
		this.loadDoanhthu(this.TYPES.TONG);
		this.loadDoanhthu(this.TYPES.SANPHAM);
		this.loadDoanhthu(this.TYPES.NHOM);
	}
    loadDoanhthu(type){
		let that = this;
		return new Promise(function(resolve, reject){
			that.service.runSql(that.genSql(type)).subscribe(res => {
				if (type == that.TYPES.TONG) that.doanhthuTong = res;
				else if (type == that.TYPES.NHOM) that.doanhthuNhom = res;
				else if (type == that.TYPES.SANPHAM) that.doanhthuSanpham = res;
				resolve();
			}, error => {
				reject();
			})
		})
	}
    update(item) {
        this.displayUpdateNgansach = true;
        this.formNgansach.reset();
        this.formNgansach.controls['id'].setValue(item.ID, { onlySelf: true });
        this.formNgansach.controls['budget'].setValue(item.BUDGET, { onlySelf: true });
    }
    updateDoanhthu(item) {
        this.displayFormDoanhthu = true;
        this.formDoanhthu.reset();
        this.formDoanhthu.controls['id'].setValue(item.ID, { onlySelf: true });
        this.formDoanhthu.controls['targetNumber'].setValue(item.TARGET_NUMBER, { onlySelf: true });

    }
    onSubmitDoanhthu(value) {
        this.isLoading = true;
        this.channelService.updateDoanhthu(value).subscribe(res => {
            this.showMes('success', 'Cập nhật doanh thu thành công');
            this.loadAllDoanhThu();
            this.displayFormDoanhthu = false;
            this.isLoading = false;
        }, error => {
            this.isLoading = false;
            this.showMes('error', 'Cập nhật doanh thu thất bại');
        });
    }
    onSubmitNgansach(value) {
        this.isLoading = true;
        this.channelService.updateMediaCampaign(value, value.id).subscribe(res => {
            this.showMes('success', 'Cập nhật ngân sách thành công');
            //this.loadShopInRequest();
            this.displayUpdateNgansach = false;
            this.isLoading = false;
        }, error => {
            this.isLoading = false;
            this.showMes('error', 'Cập nhật ngân sách thành công');
        });

    }
    onTabChange(event) {
        if (event.index == 2) {
            this.loadDiaban();
        }
        if (event.index == 3) {
            this.loadAllDoanhThu();

        }

    }
    loadInfo(){
        let sql = {
            "sql" : "SELECT * FROM CRM_BDKH_MEDIA_CAMPAIGN WHERE CAMPAIGN_REQUEST_ID = "+this.id+" AND SHOP_ID = "+this.shopId,
        };
        let that = this;
        return new Promise(function(resolve, reject){
            
            that.service.runSql(sql).subscribe(res => {
                that.info = res[0];
                resolve();
            }, error => {
                
            })
        });
        
    }
    onBeforeUpload(event) {
        this.isLoading = true;
    }
    beforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
    onUpload(event) {
        this.showMes('success', 'Up load file thành công');

        this.loadAllDoanhThu();
        this.isLoading = false;
        this.displayFormUploadSieuthi = false;
        this.loadInfo();
    }
    exportFile() {

        let sql = {
			// "sql":"select * from CRM_MEDIA_SHOP_AREA_TARGET where GOODS_TYPE <> 0 and SHOP_ID = "+this.shopId+" and CAMPAIGN_REQUEST_ID = "+this.id,

            "sql": "select shop_id, shop_code,pay_area_code,pay_area_name, GOODS_CODE, GOODS_NAME,TARGET_NUMBER, GOODS_TYPE from CRM_MEDIA_SHOP_AREA_TARGET where campaign_request_id = "+this.id+" and goods_type <> 0 union select shop_id, shop_code, 'Sieuthi' as Pay_area_code,'Sieuthi' as pay_area_name, GOODS_CODE, GOODS_NAME,TARGET_NUMBER, GOODS_TYPE from CRM_BDKH_MEDIA_SHOP_TARGET where campaign_request_id = "+this.id+" and goods_type = 0"
		}
		
		localStorage.setItem('api', this.api+'domain/get-sql-result');
		localStorage.setItem('query', JSON.stringify(sql));
		window.open(this.host+'/baocao/export_doanh_thu.html');
    }
    exportFileNganSach() {
        localStorage.setItem('api', this.api + 'requestcampaign/get-media-campaign-shop-by-id?id=' + this.id);
        window.open(this.host + '/baocao/export_ngan_sach.html');
    }
    onSelect(event, name){
        if (event.files[0].name != name){
			this.upfile.clear();
            this.showMes('error', 'File cần có tên là '+ name);

        }
    }
    onUploadNgansach(event) {
        // this.loadShopInRequest();
        this.isLoading = false;
        this.displayFormUploadNgansach = false;
    }

    loadSelect(){
		if (this.areas.length == 0){
			this.isLoading = true;
			this.areas.push({label:'Địa bàn', value:null});
			this.areaService.findAreasByShopId(this.shopId).subscribe(res => {	
				for(let i=0; i<res.length; i++){
					this.areas.push({label: res[i].address, value: res[i].id, ob: res[i]});
				}
				this.isLoading = false;
			}, error => {});
		}
	}
    suane(){
		let total = 0;
		for (let i=0; i<this.channelsInRequest.length; i++){
			this.channelsInRequest[i].amount = this.channelsInRequest[i].quantity * this.channelsInRequest[i].price;
			total += this.channelsInRequest[i].amount;
		}
        this.formArea.controls['amount'].setValue(total, { onlySelf: false });
		
	}
    create(){
        this.loadSelect();
		this.formArea.reset();
		this.title = "Thêm";
		this.formCreate = true;
		this.displayForm = true;
		if (this.channelsInRequest.length == 0){
			this.loadChannelsInRequest();
		}
		for (let i =0; i<this.channelsInRequest.length; i++)
			this.channelsInRequest[i].quantity = 0;
	}
    onSubmitArea(value:any){
   
        
		if (this.formCreate){
			if (this.formArea.controls['startDate'].value > this.formArea.controls['endDate'].value){
				this.showMes('error', 'Ngày bắt đầu chiến dịch phải nhỏ hơn ngày kết thúc');
				return;
			}
			if (this.formArea.controls['startDate'].value < this.program.START_DATE){
				this.showMes('error', 'Ngày bắt đầu của địa bàn phải lớn hơn hoặc bằng ngày bắt đầu của chiến dịch');
				return;
			}
			if (this.formArea.controls['endDate'].value > this.program.END_DATE){
				this.showMes('error', 'Ngày kết thúc của địa bàn phải nhỏ hơn hoặc bằng ngày kết thúc của chiến dịch');
				return;
			}
		}
		
		 
		 
		let data = {
			id: this.program.ID,
			campaignDetail:{
                areaShopId : value.areaShopId,
                startDate: value.startDate,
                endDate: value.endDate,
                description : value.description
        	},
			prices: [],
		};

		for (let i=0;i<this.channelsInRequest.length; i++){
			if (this.channelsInRequest[i].quantity != 0 && this.channelsInRequest[i].quantity != '' && this.channelsInRequest[i].quantity != null){
				data.prices.push({
					mediaId: this.channelsInRequest[i].mediaId,
					price: this.channelsInRequest[i].price,
					quantity : this.channelsInRequest[i].quantity,
					amount : this.channelsInRequest[i].amount
				});
			}
		}
	

		if (data.prices.length == 0) {
			this.showMes('error', 'Bạn điền số lượng cho ít nhất 1 hình thức truyền thông');
			return;
		}
		if (this.formCreate == true){
			
			this.isLoading = true;
			this.service.addMediaCampaignDetail(data).subscribe(res => {
				this.isLoading = false;
				this.displayForm = false;
				this.loadProgram();
                this.loadDiaban();
				this.showMes('success', 'Thêm thành công');
                this.loadInfo();
			}, error => {
				this.isLoading = false;
				this.showMes('error', 'Thêm thất bại');
			});
		}else{
			this.isLoading = true;
			delete data.campaignDetail.endDate;
			delete data.campaignDetail.startDate;
			// console.log('sua');
			this.service.updateMediaCampaignDetail(data).subscribe(res => {
				this.loadProgram();
				this.loadDiaban();
				this.displayForm = false;
                this.loadInfo();
			}, error => {

			});
		}
        
	}
    gDate(d: number){
		let date = new Date(d);
		let m = date.getMonth() + 1;
		return date.getDate()+"/"+m +"/"+date.getFullYear();
	}
	vDate(d){
		let date = new Date(d);
		return date;
	}
    loadDiaban(){
		this.service.findAreaByMediaId(this.program.ID).subscribe(
			res => {
				this.diaban = res;
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    updateArea(item){
		this.formArea.reset();
		this.title = "Sửa";
		this.formCreate = false;
		this.displayForm = true;
		this.formArea.controls['id'].setValue(item.id, { onlySelf: false });
		this.formArea.controls['amount'].setValue(item.amount, { onlySelf: false });
		this.formArea.controls['description'].setValue(item.description, { onlySelf: false });
		this.formArea.controls['areaShopId'].setValue(item.areaShopId, { onlySelf: false });
		this.formArea.controls['startDate'].setValue(this.gDate(item.startDate), { onlySelf: false });
		this.formArea.controls['endDate'].setValue(this.gDate(item.endDate), { onlySelf: false });

		this.channelsInRequest = [{mediaId: item.mediaId, name: item.mediaName, quantity: item.quantity, amount: item.amount, price: item.price}];
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
	}
	guiDuyet(){
		// if (this.program.TOTAL_COST > this.program.BUDGET || ((this.program.TOTAL_COST/this.program.BUDGET) * 100) <80){
		// 	this.showMes('error', 'Tổng tiền phải lớn hơn 80% ngân sách và nhỏ hơn ngân sách');
		// 	 return;
		// }
        let that = this;
        this.loadInfo().then(function(){

            if (that.info.TOTAL_COST > that.info.DELTA_BUDGET || ((that.info.TOTAL_COST/that.info.DELTA_BUDGET) * 100) <80){
            	that.showMes('error', 'Tổng tiền phải lớn hơn 80% ngân sách và nhỏ hơn ngân sách');
            	 
            }else{
                that.isLoading = true;
                that.service.updateMediaCampaign({status: 2}, that.program.ID).subscribe(
                    res => {
                        that.displayForm = false;
                        that.loadProgram();
                        that.isLoading = false;
                        that.showMes('success', 'Gửi duyệt thành công');
                    },
                    error => {}
                )
            }
        });
		
	}
	acceptDel(){
		this.service.delAreaInMedia(this.delItem).subscribe(
			res => {
				this.delItem = null;
				this.displayDel = false;
				this.loadProgram();
                this.loadDiaban();
				this.showMes('error', 'Xóa thành công');
			},
			error => {}
		);
	}
	hoanhthanh(){
        this.isLoading = true;
        this.service.genShopArea(this.id, this.shopId).subscribe(res=>{
            this.showMes('success', 'Đã hoàn thành');
            this.isLoading = false;
        }, error => {});
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }

}