import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { CampaignService } from "../shared/services/campaign.service";
import { StaffService } from "../shared/services/staff.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';

@Component({
    selector: 'app-campaign-assign-again',
    templateUrl: './campaign-assign-again.component.html',
    styleUrls: ['./campaign-assign-again.component.css']
})
export class CampaignAssignAgainComponent implements OnInit {

    private items: any[] = [];
   
    private staffs: any[] = [];
    private user;
    private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    private campaignId: number;
    private total: number;
    private dagiao: number;
    private form: FormGroup;
    private displayForm:boolean = false;
    private campaignShopId;
    private status = 1;
    private click: boolean = false;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: CampaignService,
        private staffService: StaffService,
        private fb: FormBuilder,
    ) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['id'];
            this.total = params['total'];
            this.campaignShopId = params['campaignshopId'];
            this.dagiao = this.total;
            this.check();
            this.loadItems();

        });
        this.form = this.fb.group({
			'id': new FormControl(),
			'customerNumber': new FormControl('', Validators.compose([Validators.required])),
		});

    } 
    check(){
        this.isLoading = true;
        this.service.findCampaignShopById(this.campaignShopId).subscribe(res=> {
            this.status = res.status;
            if (this.status == 2){
                 
            }
        }, error => {});
    }
    loadItems() {
        this.isLoading = true;
        this.service.findResultStaffsCampaignByShopId(this.user.shopId, this.campaignId).subscribe(
			res => {
 
                for (var i in res){
                    this.items.push({ id: res[i].staffId, staffCode: res[i].staffCode, name: res[i].staffName, customerNumber: res[i].targetNumber });
                }
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);

        

    }
    loadDaphancong(){
        this.isLoading = true;
        this.service.findStaffsCampaignByShopId(this.user.shopId, this.campaignId).subscribe(
        	res => {
                this.items =res;
        		this.isLoading = false;
        	},
        	error => {
        		this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
        	}
        );
    }
    update(item){
        this.displayForm = true;
        this.form.controls['id'].setValue(item.id, { onlySelf: true });
        this.form.controls['customerNumber'].setValue(item.customerNumber, { onlySelf: true });
    }
    onSubmit(value){
        for (let i=0; i<this.items.length; i++){
            if (this.items[i].id == value.id){

                this.items[i].customerNumber = value.customerNumber;
                break;
            }
        }
        this.dagiao = 0;
        for (let i=0; i< this.items.length; i++){
            console.log(i+' '+this.items[i].customerNumber);
            this.dagiao +=parseInt(this.items[i].customerNumber);
        }
        this.displayForm = false;
    }
    assignAuto() {
        this.isLoading = true;
        this.service.assignAuto({ campaignId: this.campaignId, shopId: this.user.shopId, shopCode: this.user.shopCode, shopName: this.user.shopName }).subscribe(res => {
            this.showMes('success', 'Phân công thành công');
            this.isLoading = false;
        }, erorr => {

        })
    }
    assignManualAgain(){
        if (this.total != this.dagiao) {
            this.showMes('error', 'Số khách hàng đã giao phải bằng tổng số khách hàng');
            return;
        }
        if (this.click == true){
            return
        }else this.click = true;
        
        this.isLoading = true;
        let data = {
            campaignId: this.campaignId,
            shopId: this.user.shopId,
            shopCode: this.user.shopCode,
            staffs: this.items,
        }
        this.service.assignManualAgain(data).subscribe(res => {
            console.log(res);
            if (res == true){
                this.router.navigate(['/campaign-assign-again', this.campaignId, this.total, this.campaignShopId]);
                this.showMes('success', 'Phân công lại thành công');
            }else{
                this.router.navigate(['/campaign-assign-again', this.campaignId, this.total, this.campaignShopId]);
                this.showMes('error', 'Đã có khách hàng được chăm sóc nên không thể phân công lại');
            }
            this.isLoading = false;
            this.click = false;
            
        }, erorr => {
            this.isLoading = false;
            this.showMes('error', 'Có lỗi xảy ra');
            this.click = false;
        })

    }

    phancong(event){
        
        this.dagiao = 0;
        for (let i=0; i< this.items.length; i++){
            console.log(i+' '+this.items[i].customerNumber);
            this.dagiao +=parseInt(this.items[i].customerNumber);
        }
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
