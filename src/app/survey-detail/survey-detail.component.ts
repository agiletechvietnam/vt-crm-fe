import { Component, OnInit } from '@angular/core';
import {SurveyService} from  '../shared/services/survey.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
	selector: 'app-survey-detail',
	templateUrl: './survey-detail.component.html',
	styleUrls: ['./survey-detail.component.css']
})
export class SurveyDetailComponent implements OnInit {
	public subscription: Subscription;
	private questions: any[] = [];
	private selectedQuestions: any[] = [];
	private selectedQuestionId;
	private isLoading = true;
	private mes: Message[];
	private surveyId;
	private user;
	constructor(
		private service: SurveyService,
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this.surveyId = params['id'];
			let self = this;
			Promise.all([this.loadSelected(), this.loadItems()]).then(function(){
				for (let i =0; i<self.selectedQuestions.length; i++)
					for (let j =0; j<self.questions.length; j++){
						console.log('xoa ne'+self.selectedQuestions[i].id+' '+self.questions[j].value);
						if (self.selectedQuestions[i].id == self.questions[j].value){							
							self.questions.splice( j, 1 );
						}
					}
			})

        });
	
	}
	loadItems(){
		let that = this;
		return new Promise(function(solove, reject){
			that.isLoading = true;
			that.questions.push({label: "Chọn câu hỏi", value: null});
			that.service.findQuestions().subscribe(res => {
				
				for(let i=0; i<res.length; i++){
					that.questions.push({label: res[i].questionText, value: res[i].id});
				}
				that.isLoading = false;
				solove();
			}, error => {
				that.isLoading = false;
			});
		});
		

	}
	loadSelected(){
		let that = this;
		return new Promise(function(solove, reject){
			that.isLoading = true;
			that.service.findQuestionsBySurveyId(that.surveyId).subscribe(res => {
				that.selectedQuestions = res;
				that.isLoading = false;
				solove();
			}, error => {

			});
		});
	}
	add(){
		if (this.selectedQuestionId == null) {
			this.showMes('warn', 'Bạn cần chọn câu hỏi');
			return;
		}
		let i = 0;
		while(i < this.selectedQuestions.length && this.selectedQuestions[i].id != this.selectedQuestionId){
			i++;
		}
		
		if (i >= this.selectedQuestions.length){
			this.isLoading = true;
			this.service.addQuestionToSurvey({surveyId: this.surveyId, questionId: this.selectedQuestionId}).subscribe(res=> {
				this.isLoading = false;
				this.showMes('success', 'Thêm thành công');
				this.loadSelected();
			}, error => {
				this.isLoading = false;
				this.showMes('success', 'Có lỗi sảy ra');
			})
		
		}else{

			this.showMes('warn', 'Bạn đã thêm câu hỏi này rồi');
		}
	}
	delete(item){
		this.isLoading = true;
		this.service.delQuestionInSurvey(item.surveyQuestionId).subscribe(res=>{
			this.isLoading = false;
			this.showMes('warn', 'Xóa thành công');
			this.loadSelected();
		}, error => {
			this.isLoading = false;
			this.showMes('success', 'Có lỗi sảy ra');
		})
	}
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}