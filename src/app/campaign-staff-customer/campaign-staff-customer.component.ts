import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {AreaService} from '../shared/services/area.service';
import {StoreService} from '../shared/services/store.service';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CampaignService } from "../shared/services/campaign.service";
import {CustomerService} from "../shared/services/customer.service";

@Component({
	selector: 'app-campaign-staff-customer',
	templateUrl: './campaign-staff-customer.component.html',
	styleUrls: ['./campaign-staff-customer.component.css']
})

export class CampaignStaffCustomerComponent implements OnInit {
	private form: FormGroup;
	private formnot: FormGroup;
	private displayForm: boolean = false;
	private items: any[] = [];
	public subscription: Subscription;
    private user;
    private campaignId: number;
    private mes: Message[];
    private staffId;
	private isLoading:boolean = false;
	private lydos: any[] = [];
	private displayFormnot: boolean = false;
	private displayCustomer: boolean = false;
	private customer: any = {};
	private transactions: any[] = [];
    private warrantys: any[] = [];
	private transaction_details: any[] = [];
	private hinhthucs: any[] =[];
	private displayFormTD: boolean = false;
	constructor(
		private fb: FormBuilder,
		private router: Router,
        private activatedRoute: ActivatedRoute,
		private service: CampaignService,
		private customerService: CustomerService
	) { }

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.form = this.fb.group({
			'id': new FormControl(),
			'careContent': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(1000)])),
			'careResult': new FormControl('', Validators.compose([Validators.required,  Validators.minLength(5), Validators.maxLength(1000)])),
			'cusCareMethodId': new FormControl('', Validators.compose([Validators.required]))
		});
		this.formnot = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([])),
			'careStatus': new FormControl('', Validators.compose([Validators.required])),
		});
		
		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['campaignid'];
			this.loadItems();
        });

		this.hinhthucs = [
			{label: "Chọn hình thức", value: null},
			{label: "Gọi điện", value: 1},
			{label: "Nhắn tin", value: 2},
			{label: "Trực tiếp", value: 3},
			{label: "Qua facebook", value: 4},
			{label: "Khác", value: 5}
		];
		this.lydos.push({label: "Chọn nguyên nhân", value: null});
		this.lydos.push({label: "Tắt máy", value: 8});
		this.lydos.push({label: "Bận không nghe máy", value: 9});
		this.lydos.push({label: "Từ chối chăm sóc", value: 10});
		this.lydos.push({label: "Khác", value: 7});
	}
	loadCustomer(phone){
		this.customerService.findCustomerByPhone(phone).subscribe(
            res => {
				this.customer = res[0];
				let that = this;
                this.isLoading = true;
				Promise.all([that.loadTransactions(res[0].cusMobile), that.loadWarrantys(res[0].cusMobile)]).then(function(){
                    that.isLoading = false;
                })
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
	loadItems(){
        this.isLoading = true;
        this.service.findResultCustomersCampaignByStaffId(this.user.id, this.campaignId).subscribe(
			res => {
                this.items =res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
	update(item){
		
		
		this.form.reset();
		this.displayForm = true;
		this.form.controls['id'].setValue(item.id, { onlySelf: true });
		this.form.controls['careContent'].setValue(item.careContent, { onlySelf: true });
		this.form.controls['careResult'].setValue(item.careResult, { onlySelf: true });

		this.viewCustomer(item.cusMobile);
	}
	updatenot(item){

		this.formnot.reset();
		this.displayFormnot = true;
		this.formnot.controls['id'].setValue(item.id, { onlySelf: true });
		this.formnot.controls['careStatus'].setValue(item.careStatus, { onlySelf: true });
		this.formnot.controls['note'].setValue(item.note, { onlySelf: true });
		this.viewCustomer(item.cusMobile);
	}
	onSubmit(value){
		this.isLoading = true;
		value.careStatus = 6;
		this.service.updateCampaignCustomer(value, value.id).subscribe(res => {
			this.isLoading = false;
			this.loadItems();
		}, error => {});
		this.displayForm = false;
	}
	onSubmitnot(value){
		
		this.isLoading = true;
		this.service.updateCampaignCustomer(value, value.id).subscribe(res => {
			this.isLoading = false;
			this.loadItems();
		}, error => {});
		this.displayFormnot = false;
	}
	viewCustomer(phone){
		var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[0]);
        c.style.width = "1000px";
		c.style.height = "500px";
		this.displayCustomer = true;
		this.loadCustomer(phone);
	}

	loadTransactions(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findTransactions(phone).subscribe(res => {
                that.transactions = res;
                resolve();
            }, error => {})
        });
        
    }
    loadWarrantys(phone){
        let that = this;
        return new Promise(function(resolve, reject){
            that.customerService.findWarrantys(phone).subscribe(res => {
                that.warrantys = res;
                resolve();
            }, error => {})
        })       
    }

	// loadTransactions(phone){
    //     this.isLoading = true;
    //     this.customerService.findTransactions(phone).subscribe(res => {
    //         this.transactions = res;
    //         this.isLoading = false;
    //     }, error => {})
    // }
	// loadWarrantys(phone){
    //     this.isLoading = true;
    //     this.customerService.findWarrantys(phone).subscribe(res => {
    //         this.warrantys = res;
    //         this.isLoading = false;
    //     }, error => {})

       
    // }
	view(id){
        var c = <HTMLElement>(document.getElementsByClassName('ui-dialog-content')[3]);
        c.style.width = "700px";
		c.style.height = "500px";
        this.displayFormTD = true;
        this.transaction_details = [];
        this.isLoading = true;
        this.customerService.findTransactionDetail(id).subscribe(
            res => {
                 this.transaction_details = res;
                 this.isLoading = false;
            },
            error => {}
        );
    }
    
}