import { Component, OnInit } from '@angular/core';
import { GrowlModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Message } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from "../shared/services/survey.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';

@Component({
    selector: 'app-fill-survey',
    templateUrl: './fill-survey.component.html',
    styleUrls: ['./fill-survey.component.css']
})
export class FillSurveyComponent implements OnInit {
    private isLoading: boolean = false;
    private mes: Message[];
    public subscription: Subscription;
    private surveyId;
    private survey: any = {};
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: SurveyService
    ) { }

    ngOnInit() {
        
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.surveyId = params['id'];
            this.loadItems();
        });
    }
    
    loadItems() {
        this.isLoading = true;
        this.service.fillSurvey(this.surveyId).subscribe(
            res => {
                this.survey = res;
                for (let i=0; i<this.survey.questions.length; i++){
                    this.survey.questions[i].realAnswers = [];
                }
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    Done(){
        console.log(this.survey);
    }
    showMes(type, m) {
        this.mes = [];
        this.mes.push({ severity: type, summary: m, detail: '' });
        let t = this;
        setTimeout(function () {
            t.mes = [];
        }, 2500);
    }
}
