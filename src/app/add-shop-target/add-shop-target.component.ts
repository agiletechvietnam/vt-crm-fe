import {Component, OnInit, ViewChild} from "@angular/core";
import { MultiSelectModule } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import {SelectItem} from 'primeng/primeng';
import {StoreService} from '../shared/services/store.service';
import {TargetModel} from  '../shared/models/target.model';
import { Router, ActivatedRoute } from '@angular/router';
import {TargetService} from  '../shared/services/target.service';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { HostConstant } from "../shared/host.constant";
import { FilenameConstant } from "../shared/filename.constant";
import {FileUpload} from 'primeng/primeng';
@Component({
	selector: 'app-add-shop-target',
	templateUrl: './add-shop-target.component.html',
	styleUrls: ['./add-shop-target.component.css']
})
export class AddShopTargetComponent implements OnInit {
	 @ViewChild('upfile') upfile: FileUpload;


	public subscription: Subscription;
	private items: any[] = [];
	private shops: SelectItem[] = [];
	private target = new TargetModel();
	private form: FormGroup;
	private targetForm: FormGroup;
	private mes: Message[];

	private displayUpdate: boolean = false;
	private displayDel: boolean = false;
	private shopName: string;
	private delItem: any;
	private isLoading: boolean = false;
	private displayLydo: boolean = false;
	private lydo: string;
	private idLydo: number;
	private user;
	private api = HostConstant.API;
	private dagiao: boolean = false;
	constructor(
		private storeService: StoreService,
		private activatedRoute: ActivatedRoute,
		private targetService: TargetService,
		private fb: FormBuilder
	) { }
	
	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('user'));
		this.isLoading = true;
		this.form = this.fb.group({
			'shop': new FormControl('', Validators.required),
			'targetNumber': new FormControl('', Validators.compose([Validators.required]))
		});

		this.targetForm = this.fb.group({
			'id': new FormControl('', Validators.required),
			'targetNumber': new FormControl('', Validators.compose([Validators.required]))
		});

		this.subscription = this.activatedRoute.params.subscribe(params => {
            this.targetService.findTargetById(params['id']).subscribe(
				res => {
					this.target = res;

					this.loadShop();
				},
				error => {}
			);

			
        });

		this.storeService.findAllStores().subscribe(res => {
			this.shops.push({label: "Chọn siêu thị", value: null});
			for (let i=0; i<res.length; i++){
				this.shops.push({label: res[i].name, value: res[i]});
			}
		}, error => {
 
		});
		
	}

	loadShop(){
		this.isLoading = true;
		this.targetService.findShopTargetByTarget(this.target.id).subscribe(res => {
			this.items = res;
			this.isLoading = false;
		}, error => {

		}); 
	}
	addShop(value:any){

		this.isLoading = true;
		for(let i = 0; i<this.items.length; i++){
			
			if (this.items[i].shopId == value.shop.id){
				this.showMes('warn', 'Bạn đã thêm cửa hàng này rồi');
				this.isLoading = false;
				return;
			}
		}
		this.targetService.addShopTarget({
			shopId: value.shop.id,
			shopName: value.shop.name,
			shopCode: value.shop.shopCode,
			targetNumber: value.targetNumber, 
			bdkhTargetId: this.target.id
		}).subscribe(res => {
			this.isLoading = false;
			this.items.unshift(res);
			this.showMes('success', 'Thêm thành công');
			this.loadShop();
		}, error => {
			this.showMes('danger', 'Thêm thất bại');
		});
		
	}
	update(item){
		this.targetForm.reset();
		this.displayUpdate = true;
		this.shopName = item.shopName;
		this.targetForm.controls['id'].setValue(item.id, {onlySelf: true});
		this.targetForm.controls['targetNumber'].setValue(item.targetNumber, {onlySelf: true});

	}
	showDuyet(item){
		this.displayLydo = true;
		this.lydo = item.note;
		this.idLydo = item.id;
		
	}
	Giao(){
		this.isLoading = true;
		this.targetService.updateTarget({status: 2},  this.target.id).subscribe(res => {
			this.isLoading = false;
			this.showMes('success', 'Đã giao');
			this.dagiao = true;
			this.target.status = 2;
		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Chưa giao được');
		});
	}

	duyet(status){
		this.isLoading = true;
		this.targetService.updateShopTarget({id: this.idLydo, status: status}).subscribe(res => {
			this.isLoading = false;
			this.loadShop();
			this.showMes('success', 'Đã duyệt thành công');
			this.displayLydo = false;

		}, error => {
			this.isLoading = false;
			this.showMes('error', 'Chưa duyệt được');
			this.displayLydo = false;
		});
	}
	updateSubmit(value: any){
		this.isLoading = true;
		this.targetService.updateShopTarget(value).subscribe(res => {
			this.showMes('success', 'Sửa thành công');
			this.isLoading = false;
			this.displayUpdate = false;
			this.loadShop();
		}, error => {
			this.isLoading = false;
			this.displayUpdate = false;
		});
	}
	delete(item){
		this.delItem = item;
		this.displayDel = true;
		// for (var i=0; i<this.items.length; i++)
		// 	if (this.items[i].id == item.id){
				
		// 		this.shops.push({label: this.items[i].shop_name, value: this.items[i].ob});
		// 		this.items.splice(i, 1);

		// 		break;
		// 	}
	}
	acceptDel(){
		this.isLoading = true;
		this.targetService.deleteShopTarget(this.delItem.id).subscribe(res => {
			this.isLoading = false;
			for(let i = 0; i<this.items.length; i++){
				if (this.items[i].id == this.delItem.id){
					this.items.splice(i, 1);
					break;
				}
			}
			this.displayDel = false;
			this.showMes('warn', 'Xóa thành công');
		}, error => {
			this.showMes('danger', 'Xóa không thành công');
		});
	}
	onBeforeUpload(event){
        this.isLoading = true;
    }
	onBeforeSend(event){
		event.xhr.setRequestHeader("Authorization", "Bearer "+ this.user.jwtToken);
	}
	onUpload(event) {
		this.isLoading = false;
		this.loadShop();
	}
	onSelect(event){
        if (event.files[0].name != FilenameConstant.target){  
            this.upfile.clear();
            this.showMes('error', 'File cần có tên là '+ FilenameConstant.target);
        }
    }
	showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
