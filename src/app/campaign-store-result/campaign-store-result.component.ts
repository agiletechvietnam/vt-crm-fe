import { Component, OnInit } from '@angular/core';
import { CampaignService } from "../shared/services/campaign.service";
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {HostConstant} from '../shared/host.constant';
@Component({
    selector: 'app-campaign-store-result',
    templateUrl: './campaign-store-result.component.html',
    styleUrls: ['./campaign-store-result.component.css']
})
export class CampaignStoreResultComponent implements OnInit {
    private items: any[] = [];
    private user;
    private isLoading: boolean =  false;
    private mes: Message[];
    public subscription: Subscription;
    public campaignId;
    constructor(
        private service: CampaignService,
        private router: Router, 
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.campaignId = params['id'];
            this.loadItems();
        });
    }
    baocao(){
        this.isLoading = true;


        this.service.findCampaignById(this.campaignId).subscribe(res => {
            
            let query1 = {
                "sql":"SELECT * FROM CRM_RPT_NANGSUAT_SIEUTHI WHERE CAMPAIGN_ID = "+this.campaignId+" and SHOP_ID = 0 and RPT_TYPE='Tonghop'"
            }
            let query2 = {
                "sql":"select * from CRM_RPT_NANGSUAT_SIEUTHI where CAMPAIGN_ID= "+this.campaignId+" and RPT_TYPE='Survey' and shop_id = 0 order by STT asc, STT_SUB asc"
            }
            let query3= {
                "sql":"SELECT * FROM CRM_RPT_RASOAT_CHITIET where CAMPAIGN_ID = "+this.campaignId
            }

            let query4 = {
                "sql":"SELECT SHOP_CODE,STAFF_CODE, STAFF_NAME,TARGET_NUMBER,RESULT_NUMBER,ROUND(RESULT_NUMBER*100/TARGET_NUMBER) as HoanThanh,SUCCESS_NO, ROUND(SUCCESS_NO*100/TARGET_NUMBER) as ThanhCong,FAIL_NO,ROUND(FAIL_NO*100/TARGET_NUMBER) as ThatBai, TARGET_NUMBER-RESULT_NUMBER as UNCARE_NO,100 - ROUND(RESULT_NUMBER*100/TARGET_NUMBER) as ChuaThucHien, ALARM_1,RESULT_1,ALARM_2,RESULT_2,ALARM_3,RESULT_3 from CRM_CAMPAIGN_STAFF where campaign_id = "+this.campaignId+" and target_number > 0 order by SHOP_CODE"
            };
            localStorage.setItem('report_url', HostConstant.API + 'domain/get-sql-result');
            localStorage.setItem('report_query1', JSON.stringify(query1));
            localStorage.setItem('report_query2', JSON.stringify(query2));
            localStorage.setItem('report_query3', JSON.stringify(query3));
            localStorage.setItem('report_query4', JSON.stringify(query4));
            localStorage.setItem('report_campaign', JSON.stringify(res));
            window.open(HostConstant.HOST+'/baocao/nang_suat_cac_sieu_thi_1.html');
            this.isLoading = false;

        }, error => {});
        
    }
    baocaochamsoc(){
        this.isLoading = true;
        let that = this;
        that.findCampaign().then(function(value){
            let resultCare = HostConstant.API+'survey/get-response-by-campaign-id?campaignId='+that.campaignId;
            localStorage.setItem('report_campaign', JSON.stringify(value));
            let survey = HostConstant.API+'survey/get-by-survey-id?surveyId=';
            localStorage.setItem('resultCare',resultCare);
            localStorage.setItem('survey', survey);

            window.open(HostConstant.HOST+'/baocao/survey_tt.html');
            that.isLoading = false;
        });
    }
    findCampaign(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.findCampaignById(that.campaignId).subscribe(res => {
                resolve(res);
            }, error => {});
        });
    }

    loadItems(){
		this.isLoading = true;
		this.service.findShopCampaignByCampaignId(this.campaignId).subscribe(
			res => {
				this.items = res;
				this.isLoading = false;
			},
			error => {
				this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
			}
		);
	}
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
