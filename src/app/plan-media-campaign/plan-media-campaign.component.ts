import { Component, OnInit } from '@angular/core';
import {MediaCampaignService} from "../shared/services/mediacampaign.service";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-plan-media-campaign',
    templateUrl: './plan-media-campaign.component.html',
    styleUrls: ['./plan-media-campaign.component.css']
})
export class PlanMediaCampaignComponent implements OnInit {
    private mes: Message[];
    private isLoading: boolean = false;
    public subscription: Subscription;
    public program: any = {};

    private doanhthuNhom;
    private doanhthuSp;
    private diaban;
    
    private form: FormGroup;

    private displayForm: boolean = false;

    constructor(
        private service: MediaCampaignService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder
    ) { }
    ngOnInit() {
        this.form = this.fb.group({
			'id': new FormControl(),
			'note': new FormControl('', Validators.compose([Validators.minLength(5), Validators.maxLength(1000)]))
		});

        this.subscription = this.activatedRoute.params.subscribe(params => {
            this.program.id = params['id'];
            let that = this;

            this.loadProgram().then(function(){
                that.loadDoanhthuNhom();
                that.loadDoanhthuSp();
                that.loadDiaban();
            });
        });
        
    }

    loadProgram(){
        let that = this;
        return new Promise(function(resolve, reject){
            that.service.findMediaCampaignById(that.program.id).subscribe(
                res => {
                    that.program = res;
                    that.isLoading = false;
                    resolve();
                }, 
                error => {
                    error > console.log(error);
                    reject();
                }
            );
        })
        
    }

    loadDoanhthuNhom(){
        let query = {
            "sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = 1 and CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" and SHOP_ID = "+this.program.shopId
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuNhom = res;
        }, error => {

        });
    }
    
    loadDoanhthuSp(){
        let query = {
            "sql":"select * from CRM_BDKH_MEDIA_SHOP_TARGET where  GOODS_TYPE = 0 and CAMPAIGN_REQUEST_ID = "+this.program.campaignRequestId+" and SHOP_ID = "+this.program.shopId
        };
        this.service.runSql(query).subscribe(res => {
            this.doanhthuSp = res;
        }, error => {

        });
    }
    
    loadDiaban(){
		this.service.findAreaByMediaId(this.program.id).subscribe(
			res => {
				this.diaban = res;
			}, 
			error => {
				error> console.log(error);
			}
		)
	}
    Duyet() {
        this.isLoading = true;
        this.service.updateMediaCampaign({ status: 3}, this.program.id).subscribe(
            res => {
                this.showMes('success', 'Duyệt thành công');
                this.loadProgram();
            },
            error => { }
        );
    }

    showForm(){
        console.log('show form');
        this.displayForm = true;
        this.form.reset();
    }

    onSubmit(value){
        this.isLoading = true;
        this.service.updateMediaCampaign({ status: 4, note: value.note  }, this.program.id).subscribe(
            res => {
                this.showMes('success', 'Đã từ chối');
                this.displayForm = false;
                this.loadProgram();
            },
            error => { }
        );
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}
