import {Component, OnInit, ViewChild} from "@angular/core";
import {CustomerSearchModel} from "../shared/models/customer.search";
import {CustomerService} from "../shared/services/customer.service";
import {CustomerModel} from "../shared/models/customer.model";
import {ModalDirective} from "ng2-bootstrap";
import {DialogModule} from 'primeng/primeng';
import {TabMenuModule,MenuItem} from 'primeng/primeng';
import {SelectItem} from 'primeng/primeng';
import {CityService} from "../city/city.component.service";
import {CitySearchModel} from "../city/city.search";
import {Router} from "@angular/router";
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import {GrowlModule} from 'primeng/primeng';
import {Message} from 'primeng/primeng';
@Component({
    selector: "customer-store",
    templateUrl: './customer-store.component.html',
    styleUrls: ['./customer-store.component.css']
})
export class CustomerStoreComponent implements OnInit {
    private customerSearch = new CustomerSearchModel();
    private customers: CustomerModel[];
    private citySearch = new CitySearchModel();
    private mes: Message[];
    //
    private customerForm: FormGroup;
    private submitted: boolean;
    private genders: SelectItem[];
    private cities: SelectItem[];
    private districts: SelectItem[];
    private wards: SelectItem[];
    //
    private delItem:any;
    private display_add: boolean = false;
    private display_del: boolean = false;
    private isLoading: boolean = false;
    private shopId: number;
    private search = "";
    private totalRecords: number;
    private page: number;
    @ViewChild('createCustomer') public createCustomer: ModalDirective;
    constructor(
        private customerService: CustomerService, 
        private fb: FormBuilder,
        private cityService: CityService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.customerForm = this.fb.group({
            'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])),
            'gender':  new FormControl('', Validators.required),
            'phone': new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
            'idNo': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
            // 'identityPassport': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
            //'birthday': new FormControl('', Validators.required),
            'job': new FormControl(''),
            'income': new FormControl('', Validators.compose([Validators.pattern('[0-9]+')])),
            'cityId': new FormControl('', Validators.required),
            'districtId': new FormControl('', Validators.required),
            'payAreaCode': new FormControl('', Validators.required)
        });
   
        this.genders = [];
        this.genders.push({label:'Giới tính', value: null});
        this.genders.push({label:'Nam', value: 1});
        this.genders.push({label:'Nữ', value: 2});
        this.genders.push({label:'Khác', value: 3});
        
        this.cities = [];
        this.cities.push({label:'Tỉnh/TP', value:null});

        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});

        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.shopId = JSON.parse(localStorage.getItem('user')).shopId;
        this.loadCustomers();
         
    }
    loadLazy(event: any) {
        this.isLoading = true;
        this.page  = event.first/event.rows;	
        this.customerService.findCustomersByShopId(this.search, this.shopId, this.page, event.rows).subscribe(
            res => {
                this.customers = res.content;
                this.totalRecords = res.totalElements;
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    change(){
        this.search = "";
        this.loadCustomers();
    }
    loadCustomers(){
        this.isLoading = true;
        this.customerService.findCustomersByShopId(this.search, this.shopId, 0, 20).subscribe(
            res => {
                this.customers = res.content;
                this.totalRecords = res.totalElements;            
                this.isLoading = false;
            },
            error => {
                this.showMes('error', 'Không thể tải dữ liệu!');
                this.isLoading = false;
            }
        );
    }
    showAdd() {
        this.customerForm.reset();
        this.display_add = true;
     
           //get all city fill selectbox
        if (this.cities.length == 1){
            this.cityService.findCities().subscribe(
                res => {
                    for (let i=0; i<res.length; i++){
                        this.cities.push({label: res[i].name, value: res[i].payAreaCode});
                    }
                }, 
                error => {
                    error> console.log(error);
                }
            )
        }
    }
    CityDropDownChange(){
        if (this.customerForm.value.cityId == null) return;
        this.districts = [];
        this.districts.push({label:'Quận/Huyện', value:null});
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findDistrictsByCityId(this.customerForm.value.cityId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.districts.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    DistrictDropDownChange(){
        if (this.customerForm.value.districtId == null) return;
        this.wards = [];
        this.wards.push({label:'Phường/Xã', value:null});
        this.cityService.findWardsByDistrictId(this.customerForm.value.districtId).subscribe(
            res => {
                for (let i=0; i<res.length; i++){
                    this.wards.push({label: res[i].name, value: res[i].payAreaCode});
                }
            },
            error =>{
                error> console.log(error);
            }
        )
    }
    showDetail(c){}
    showDel(item){
        this.display_del = true;
        this.delItem = item;
    }
    Del(){
        this.customerService.delCustomer(this.delItem.id, null).subscribe(
            res => {
                this.display_del = false;
                this.loadCustomers();
                this.showMes('warn', 'Xóa thành công!');
                
            },
            error => {
                error> console.log(error);
            }
        )
        
    }
    onSubmit(value:any){
        this.customerService.addCustomer(value).subscribe(
            res => {
                this.display_add = false;
                this.loadCustomers();
                this.showMes('warn', 'Thêm thành công!');
            },
            error => {
                error> console.log(error);
            }
        )
    }
    showMes(type, m){
		this.mes = [];
		this.mes.push({severity: type, summary: m, detail:''});
		let t = this;
		setTimeout(function(){
			t.mes = [];
		}, 2500);
	}
}