import { HmtimesheetPage } from './app.po';

describe('hmtimesheet App', function() {
  let page: HmtimesheetPage;

  beforeEach(() => {
    page = new HmtimesheetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
